/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module;

#include <concepts>
#include <cstring>
#include <format>
#include <memory>
#include <type_traits>
#include <vector>


export module ninbot.pos:mujoco;

import :clifford;
import :euclidean;
import :coordsys;
import :spatial;


//________________________________ CONCEPTS ________________________________



namespace nin::mujoco {



template <typename T> concept ref_integral       = std::integral<std::remove_reference_t<T>>;
template <typename T> concept ref_floating_point = std::floating_point<std::remove_reference_t<T>>;
template <typename T> concept ref_is_pointer     = std::is_pointer_v<std::remove_reference_t<T>>;

template <typename MJN>
concept mjtNumC = std::floating_point<MJN>;

template <typename MJM>
concept mjModelC =
    requires (MJM * m) {
        {m->nbody} -> ref_integral;
        {m->ngeom} -> ref_integral;
        {m->nsite} -> ref_integral;
        {m->ncam}  -> ref_integral;

        {m->body_parentid} -> ref_is_pointer; {*m->body_parentid} -> ref_integral;
        {m->body_pos}      -> ref_is_pointer; {*m->body_pos}      -> ref_floating_point;
        {m->body_quat}     -> ref_is_pointer; {*m->body_quat}     -> ref_floating_point;

        {m->geom_bodyid}   -> ref_is_pointer; {*m->geom_bodyid}   -> ref_integral;
        {m->geom_pos}      -> ref_is_pointer; {*m->geom_pos}      -> ref_floating_point;
        {m->geom_quat}     -> ref_is_pointer; {*m->geom_quat}     -> ref_floating_point;

        {m->site_bodyid}   -> ref_is_pointer; {*m->site_bodyid}   -> ref_integral;
        {m->site_pos}      -> ref_is_pointer; {*m->site_pos}      -> ref_floating_point;
        {m->site_quat}     -> ref_is_pointer; {*m->site_quat}     -> ref_floating_point;

        {m->cam_bodyid}    -> ref_is_pointer; {*m->cam_bodyid}    -> ref_integral;
        {m->cam_pos}       -> ref_is_pointer; {*m->cam_pos}       -> ref_floating_point;
        {m->cam_quat}      -> ref_is_pointer; {*m->cam_quat}      -> ref_floating_point;
    };

template <typename MJD>
concept mjDataC =
    requires (MJD const*const d) {
        {d->xpos} -> ref_is_pointer; {*d->xpos} -> ref_floating_point;
        {d->xmat} -> ref_is_pointer; {*d->xmat} -> ref_floating_point;
    };

} // namespace nin::mujoco


//______________________ Forward declaration of global space functions ______________________

int mj_name2id (nin::mujoco::mjModelC auto const*, int, const char*);
extern "C" { int mju_str2Type (char const*); }
extern "C" { const char* mju_type2Str (int); }

//________________________________ POSITION - BACKEND - MUJOCO ________________________________

namespace nin {

export
class position_coordsys_backend_mujoco : public coordsys_backend<position_coordsys_class>
{
    // Mujoco docs: 'x' means it is stored in global coordinates
    double const* xpos_ptr;
    double const* xmat_ptr;

public:
    position_coordsys_backend_mujoco( mujoco::mjModelC auto const* m,
                                      mujoco::mjDataC  auto const* d,
                                      int type,
                                      std::string const& name );

    rigid_body_tf offset() const override;
    std::shared_ptr<coordsys_backend<position_coordsys_class>> clone() const override;

};

/*** Definitions - Constructor ***/

// DOCS: m and d must be available through the life of the backend.

inline
position_coordsys_backend_mujoco:: position_coordsys_backend_mujoco(
        mujoco::mjModelC auto const* m,
        mujoco::mjDataC  auto const* d,
        int type,
        std::string const& name )
{
    static int body_type   = mju_str2Type("body");
    static int xbody_type  = mju_str2Type("xbody");
    static int geom_type   = mju_str2Type("geom");
    static int site_type   = mju_str2Type("site");
    static int camera_type = mju_str2Type("camera");

    if (type == body_type || type == xbody_type)
    {
        xpos_ptr = d->xpos;
        xmat_ptr = d->xmat;
    }
    else if (type == geom_type)
    {
        xpos_ptr = d->geom_xpos;
        xmat_ptr = d->geom_xmat;
    }
    else if (type == site_type)
    {
        xpos_ptr = d->site_xpos;
        xmat_ptr = d->site_xmat;
    }
    else if (type == camera_type)
    {
        xpos_ptr = d->cam_xpos;
        xmat_ptr = d->cam_xmat;
    }
    else
        throw std::domain_error(std::format("Unsupported object type '{}' '{}'",
                                                                     type, mju_type2Str(type)));

    int id = mj_name2id(m, type, name.c_str());
    if (id == -1)
        throw std::logic_error(std::format("Unknown object name '{}' of type '{}' in MuJoCo model",
                                                                name,         mju_type2Str(type)));
    xpos_ptr += 3*id;
    xmat_ptr += 9*id;
}

#if 0
Used in lights
static
pose
coordsys_backend_mujoco:: make_pose_quat(mjtNum const* xpos, mjtNum const* xdir)
{
    pose retval;
    if (position)
        retval.position = { units::metres(xpos[0]),
                            units::metres(xpos[1]),
                            units::metres(xpos[2]) };
    if (quat)
        retval.orientation = quaternion{ xdir[0],
                                         xdir[1],
                                         xdir[2],
                                         xdir[3] };
    return retval;
}
#endif

/*** Definitions - Overrides ***/

[[nodiscard]]
inline
rigid_body_tf
position_coordsys_backend_mujoco:: offset() const
{
    nin::translation tr = { units::metres(xpos_ptr[0]),
                            units::metres(xpos_ptr[1]),
                            units::metres(xpos_ptr[2]) };

    nin::orientation_quantity ori (xmat_ptr, matrix_ordering::ROW_MAJOR);

    return {tr, {ori.to_quaternion()}};
}

[[nodiscard]]
inline
std::shared_ptr<coordsys_backend<position_coordsys_class>>
position_coordsys_backend_mujoco:: clone() const
{
    return std::make_shared<position_coordsys_backend_mujoco>(*this);
}

//________________________________ COORDSYS_MUJOCO ________________________________

export
using position_coordsys_mujoco = coordsys_generic<position_coordsys_backend_mujoco>;

//________________________________ END ________________________________


} // namespace nin

