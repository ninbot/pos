/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module;

#include <concepts>
#include <span>
#include <memory>
#include <complex>

#include "type_ops.hh"


export module ninbot.pos:spatial;

import :coordsys;
import :euclidean;
import :clifford;

using std::size_t;

namespace nin {


//______________________ --------------------SPATIAL----------------------- ______________________


//_______________________ POSITION CS CLASS _______________________

export
struct position_coordsys_class
{
    using quantity_type = position_quantity;
    using tf_data_type  = rigid_body_tf;

    [[nodiscard]] constexpr static tf_data_type invert_tf  ( tf_data_type  const& tf_data );
    [[nodiscard]] constexpr static tf_data_type compose_tf ( tf_data_type  const& after,
                                                             tf_data_type  const& first );
    [[nodiscard]] constexpr static quantity_type transform_coords ( tf_data_type  const& tf_data,
                                                                    quantity_type const& qty );
};

static_assert(coordsys_class<position_coordsys_class>);

export using position_value    = coord_value <position_coordsys_class>;
export using position_point    = coord_value <position_coordsys_class>; // Same as position_value
export using          point    = coord_value <position_coordsys_class>; // Same as position_value
export template <size_t N = std::dynamic_extent>
       using position_cloud    = coord_cloud <position_coordsys_class, N>;
export using position_coordsys = coordsys    <position_coordsys_class>;
export using position_coord_tf = coord_tf    <position_coordsys_class>;


export
nin::length distance( point const& lhs, point const& rhs );


/*** Definitions ***/

[[nodiscard]]
inline constexpr
position_coordsys_class::tf_data_type
position_coordsys_class:: invert_tf( tf_data_type const& tf_data )
{
    return nin::inv(tf_data);
}

[[nodiscard]]
inline constexpr
position_coordsys_class::tf_data_type
position_coordsys_class:: compose_tf( tf_data_type const& after,
                                      tf_data_type const& first )
{
    return nin::compose(after, first);
}

[[nodiscard]]
inline constexpr
position_coordsys_class::quantity_type
position_coordsys_class:: transform_coords( tf_data_type  const& tf_data,
                                       quantity_type const& qty )
{
    return tf_data(qty);
}


inline
nin::length
distance( point const& lhs, point const& rhs )
{
    if (lhs.inCS() == rhs.inCS())
    {
        return nin::metres( std::hypot( lhs.x().SI() - rhs.x().SI(),
                                        lhs.y().SI() - rhs.y().SI(),
                                        lhs.z().SI() - rhs.z().SI() ));
    }
    else
    {
        point rtf = {lhs.inCS(), rhs};
        return nin::metres( std::hypot( lhs.x().SI() - rtf.x().SI(),
                                        lhs.y().SI() - rtf.y().SI(),
                                        lhs.z().SI() - rtf.z().SI() ));
    }
}


//________________________________ POSITION TREE NODE ________________________________

export
class position_coordsys_treenode
  : public coordsys_treenode< coordsys_tree_backend<position_coordsys_class> >
{
public:
    using coordsys_treenode< coordsys_tree_backend<position_coordsys_class> >::coordsys_treenode;

    explicit position_coordsys_treenode( position_coordsys const& parent_CS,
                                         pose_quantity     const& origin_in_parent = {} );
    explicit position_coordsys_treenode( WCS_t,
                                         pose_quantity const& origin_in_WCS = {} );

    explicit position_coordsys_treenode( position_coordsys const& parent_CS,
                                         pose              const& origin_in_parent );
    explicit position_coordsys_treenode( WCS_t,
                                         pose const& origin_in_WCS );

    void set_origin_in_parent(pose_quantity const& origin_in_parent);
    void set_origin_in_parent(pose          const& origin_in_parent);

    // TODO
    //coordsys_tree_backend<position_coordsys_class>      * operator -> ()       { return dynamic_cast< BE      * >(this->backend.get()); }
    //coordsys_tree_backend<position_coordsys_class> const* operator -> () const { return dynamic_cast< BE const* >(this->backend.get()); }
};

//_______________________ ORIENTATION CS CLASS _______________________

export
struct orientation_coordsys_class
{
    using quantity_type = nin::orientation_quantity;
    using tf_data_type  = nin::rotation;

    [[nodiscard]] constexpr static tf_data_type invert_tf  ( tf_data_type  const& tf_data );
    [[nodiscard]] constexpr static tf_data_type compose_tf ( tf_data_type  const& after,
                                                             tf_data_type  const& first );
    [[nodiscard]] constexpr static quantity_type transform_coords ( tf_data_type  const& tf_data,
                                                                    quantity_type const& qty );
};

static_assert(coordsys_class<orientation_coordsys_class>);

export using orientation_value     = coord_value    <orientation_coordsys_class>;
export using orientation           = coord_value    <orientation_coordsys_class>;
export template <size_t N = std::dynamic_extent>
       using orientation_cloud     = coord_cloud    <orientation_coordsys_class, N>;
export using orientation_coordsys  = coordsys       <orientation_coordsys_class>;
export using orientation_coord_tf  = coord_tf       <orientation_coordsys_class>;
export using orientation_coordsys_treenode
                            = coordsys_treenode<coordsys_tree_backend<orientation_coordsys_class>>;

// TODO: (planar)Orientation tree node that accepts origin_in_parent, just like (planar)position
// Also apply to poses, maybe?

/*** Definitions ***/

[[nodiscard]]
inline constexpr
orientation_coordsys_class::tf_data_type
orientation_coordsys_class:: invert_tf( tf_data_type const& tf_data )
{
    return inv(tf_data);
}

[[nodiscard]]
inline constexpr
orientation_coordsys_class::tf_data_type
orientation_coordsys_class:: compose_tf( tf_data_type const& after,
                                    tf_data_type const& first )
{
    return compose(after, first, 0);
}

[[nodiscard]]
inline constexpr
orientation_coordsys_class::quantity_type
orientation_coordsys_class:: transform_coords( tf_data_type  const& tf_data,
                                          quantity_type const& qty )
{
    return tf_data(qty);
}

//_______________________ ORIENTATION - BACKEND - TAKE FROM POSITION TF _______________________

export
class orientation_coordsys_position_backend : public coordsys_backend<orientation_coordsys_class>
{
public:
    nin::position_coordsys const parent;

    orientation_coordsys_position_backend(position_coordsys const& parent_posCS);

    [[nodiscard]] rotation offset() const override;
    [[nodiscard]] std::shared_ptr<coordsys_backend<orientation_coordsys_class>>
                                                                            clone() const override;
};

export
using orientation_coordsys_from_position_coordsys =
                                        coordsys_generic<orientation_coordsys_position_backend>;


/*** Definitions - Constructor ***/

inline
orientation_coordsys_position_backend::
                orientation_coordsys_position_backend(position_coordsys const& parent_posCS)
  : parent{ parent_posCS.linked_copy() }
{ }

/*** Definitions - Virtual overrides ***/

[[nodiscard]]
inline
rotation
orientation_coordsys_position_backend:: offset() const
{
    return parent.offset().rotation;
}

[[nodiscard]]
inline
std::shared_ptr<coordsys_backend<orientation_coordsys_class>>
orientation_coordsys_position_backend:: clone() const
{
    return std::make_shared<orientation_coordsys_position_backend>(parent);
}

//________________________________ POSE - COORDSYS ________________________________

/* DOCS: Pose has no associated coordinate system because pose_value is an
 * aggregate of position_value and orientation_value, that is, position
 * and orientation are placed in different coordinate systems.
 * Thus, mapCS and coordinate transformation function are special cases.
 */

export
struct pose_coordsys
{
    nin::position_coordsys    position;
    nin::orientation_coordsys orientation;

    pose_coordsys();

    pose_coordsys(position_coordsys const& posCS);
    pose_coordsys(WCS_t);

    pose_coordsys(position_coordsys const& posCS, orientation_coordsys const& oriCS);
    pose_coordsys(WCS_t,                          orientation_coordsys const& oriCS);
    pose_coordsys(position_coordsys const& posCS, WCS_t                            );
    pose_coordsys(WCS_t,                          WCS_t                            );

    bool operator == (pose_coordsys const& rhs) const;

    pose_value origin() const;
};

//________________________________ POSE - VALUE ________________________________

export
class pose_value : public pose_quantity
{
    pose_coordsys inCS_data;

public:
    pose_value();
    pose_value(pose_value const& copy);
    pose_value(pose_value     && move);
    pose_value & operator = (pose_value const& copy);
    pose_value & operator = (pose_value     && move);

    pose_value(pose_quantity const& qty_arg);
    pose_value(pose_coordsys const& in_coordsys, pose_quantity const& qty_arg = {});
    pose_value(WCS_t,                            pose_quantity const& qty_arg = {});

    nin::point       position_value() const;
    nin::orientation orientation_value() const;
    pose_coordsys const& inCS() const;

    pose_value map_to(pose_coordsys const& to_coordsys) const;
    pose_value map_to(WCS_t) const;
    void fetch(pose_value const& value);

    pose_quantity const& qty() const;
    pose_quantity      & qty();
};

//________________________________ POSE - CLOUD ________________________________

export
template <size_t N = std::dynamic_extent>
class pose_cloud : public type_ops::sequence_container_t< pose_quantity, N >
{
    pose_coordsys inCS_data;

public:
    using container_type = type_ops::sequence_container_t< pose_quantity, N >;

    pose_cloud();
    pose_cloud(pose_cloud const& copy);
    pose_cloud(pose_cloud     && move);
    pose_cloud & operator = (pose_cloud const& copy);
    pose_cloud & operator = (pose_cloud     && move);

    pose_cloud(container_type const& quantities);
    pose_cloud(pose_coordsys const& in_coordsys, container_type const& qties);
    pose_cloud(WCS_t,                            container_type const& qties);

    nin::position_cloud<N>    position_values() const;
    nin::orientation_cloud<N> orientation_values() const;

    pose_coordsys const& inCS() const;
    pose_cloud map_to(pose_coordsys const& to_coordsys) const;
    pose_cloud map_to(WCS_t) const;
    void fetch(pose_cloud const& cloud);
};

//________________________________ POSE - TRANSFORMATION FUNCTION ________________________________

export
class pose_coord_tf
{
    position_coord_tf    position_tf;
    orientation_coord_tf orientation_tf;

public:
    pose_coord_tf();
    pose_coord_tf(pose_coord_tf const& copy);
    pose_coord_tf & operator = (pose_coord_tf const& copy);
    // TODO: Move constructor, move assignment operator

    explicit
    pose_coord_tf( position_coord_tf    const& pos_TF,
                   orientation_coord_tf const& ori_TF );


    pose_coordsys from_CS() const;
    pose_coordsys to_CS() const;

    pose_coord_tf friend compose( pose_coord_tf const& left_evaluates_after,
                                  pose_coord_tf const& right_evaluates_first );

    [[nodiscard]] pose_quantity operator () (pose_quantity const& q_in) const;
    [[nodiscard]] pose_value    operator () (pose_value    const& q_in) const;
    [[nodiscard]] pose_value    operator () () const;
    template <size_t N>
    [[nodiscard]] pose_cloud<N> operator () (pose_cloud<N> const& q_in) const;

};

//________________________________ POSE - COORDSYS - Definitions ________________________________

/*** Constructors ***/

inline
pose_coordsys:: pose_coordsys()
    = default;

// DOCS: Note that this is the only case when orientation_coordsys_from_position_coordsys
//       is created by default. It will trigger this constructor with position_coordsys{}
//       but not with the WCS tag.
inline
pose_coordsys:: pose_coordsys( position_coordsys const& posCS )
  : position    { posCS.linked_copy() }
  , orientation { orientation_coordsys_from_position_coordsys(position) }
{ }

inline
pose_coordsys:: pose_coordsys( WCS_t )
  : pose_coordsys()
{ }

inline
pose_coordsys:: pose_coordsys( position_coordsys    const& posCS,
                               orientation_coordsys const& oriCS )
  : position    { posCS.linked_copy() }
  , orientation { oriCS.linked_copy() }
{ }

inline
pose_coordsys:: pose_coordsys( WCS_t,
                               orientation_coordsys const& oriCS )
  : pose_coordsys{ position_coordsys{WCS}, oriCS }
{ }

inline
pose_coordsys:: pose_coordsys( position_coordsys const& posCS,
                               WCS_t )
  : pose_coordsys{ posCS, orientation_coordsys{WCS} }
{ }

inline
pose_coordsys:: pose_coordsys( WCS_t,
                               WCS_t )
  : pose_coordsys()
{ }

/*** Other ***/

inline
bool
pose_coordsys:: operator == (pose_coordsys const& rhs) const
    = default;

inline
pose_value
pose_coordsys:: origin () const
{
    return pose_value{*this};
}


//________________________________ POSE - VALUE - Definitions ________________________________

// DOCS: When WCS tag is used, pos and ori are set to WCS.
//       But when pose_coordsys{WCS}, ori is set to default value defined in class definition.
//       Comparison of first case is ok, but comparison of second case yields false
//       when pose_coordsys{WCS} != pose_coordsys{WCS}.

/*** Definitions - Semiregular traits ***/

inline
pose_value:: pose_value()
    = default;

inline
pose_value:: pose_value(pose_value const& copy)
{
    *this = copy;
}

inline
pose_value:: pose_value(pose_value && move)
    = default;

inline
pose_value &
pose_value:: operator = (pose_value const& copy)
{
    static_cast<pose_quantity &>(*this) = copy;
    inCS_data.position    = copy.position_value().inCS().linked_copy();
    inCS_data.orientation = copy.orientation_value().inCS().linked_copy();
    return *this;
}

inline
pose_value &
pose_value:: operator = (pose_value && move)
    = default;

/*** Definitions - Other constructors ***/

inline
pose_value:: pose_value(pose_quantity const& qty_arg)
  : pose_quantity{ qty_arg }
{ }

inline
pose_value:: pose_value(pose_coordsys const& in_coordsys, pose_quantity const& qty_arg)
  : pose_quantity{ qty_arg }
  , inCS_data{ in_coordsys.position.linked_copy(),
               in_coordsys.orientation.linked_copy() }
{ }

inline
pose_value:: pose_value(WCS_t, pose_quantity const& qty_arg)
  : pose_value{ pose_coordsys{WCS, WCS}, qty_arg }
{ }

/*** Definitions - Projections ***/

inline
nin::point
pose_value:: position_value() const
{
    return nin::point{ inCS_data.position, pose_quantity::position };
}

inline
nin::orientation
pose_value:: orientation_value() const
{
    return nin::orientation{ inCS_data.orientation, pose_quantity::orientation };
}

inline
pose_coordsys const&
pose_value:: inCS() const
{
    return inCS_data;
}

inline
pose_quantity const&
pose_value:: qty() const
{
    return *this;
}

inline
pose_quantity &
pose_value:: qty()
{
    return *this;
}


/*** Definitions - Coordinate system transformations ***/

inline
pose_value
pose_value:: map_to(pose_coordsys const& to_coordsys) const
{
    pose_coord_tf tf { mapCS(inCS_data.position,    to_coordsys.position),
                       mapCS(inCS_data.orientation, to_coordsys.orientation) };
    return tf(*this);
}

inline
pose_value
pose_value:: map_to(WCS_t) const
{
    return map_to(pose_coordsys{WCS, WCS});
}

inline
void
pose_value:: fetch(pose_value const& value)
{
    static_cast<pose_quantity &>(*this) = value.map_to(inCS());
}

//________________________________ POSE - CLOUD - Definitions ________________________________

// DOCS: When WCS tag is used, pos and ori are set to WCS.
//       But when pose_coordsys{WCS}, ori is set to default value defined in class definition.
//       Comparison of first case is ok, but comparison of second case yields false
//       when pose_coordsys{WCS} != pose_coordsys{WCS}.

/*** Definitions - Semiregular traits ***/

template <size_t N>
pose_cloud<N>:: pose_cloud()
    = default;

template <size_t N>
pose_cloud<N>:: pose_cloud(pose_cloud const& copy)
{
    *this = copy;
}

template <size_t N>
pose_cloud<N>:: pose_cloud(pose_cloud && move)
    = default;

template <size_t N>
pose_cloud<N> &
pose_cloud<N>:: operator = (pose_cloud const& copy)
{
    static_cast<container_type &>(*this) = copy;
    inCS_data.position    = copy.inCS_data.position.linked_copy();
    inCS_data.orientation = copy.inCS_data.orientation.linked_copy();
    return *this;
}

template <size_t N>
pose_cloud<N> &
pose_cloud<N>:: operator = (pose_cloud && move)
    = default;

/*** Definitions - Other constructors ***/

template <size_t N>
pose_cloud<N>:: pose_cloud(container_type const& quantities)
  : pose_quantity{ quantities }
{ }

template <size_t N>
pose_cloud<N>:: pose_cloud( pose_coordsys const& in_coordsys,
                            container_type const& quantities )
  : container_type{ quantities }
  , inCS_data{ in_coordsys.position.linked_copy(),
               in_coordsys.orientation.linked_copy() }
{ }

template <size_t N>
pose_cloud<N>:: pose_cloud(WCS_t, container_type const& quantities)
  : pose_cloud{ pose_coordsys{WCS, WCS}, quantities }
{ }

/*** Definitions - Projections ***/

template <size_t N>
nin::position_cloud<N>
pose_cloud<N>:: position_values() const
{
    typename position_cloud<N>::container_type quantities;
    if constexpr (N == std::dynamic_extent)
        quantities.resize(this->size());
    std::ranges::copy(static_cast<container_type const&>(*this), {}, &pose_quantity::position);
    return { inCS_data.position, quantities };
}

template <size_t N>
nin::orientation_cloud<N>
pose_cloud<N>:: orientation_values() const
{
    typename orientation_cloud<N>::container_type quantities;
    if constexpr (N == std::dynamic_extent)
        quantities.resize(this->size());
    std::ranges::copy(static_cast<container_type const&>(*this), {}, &pose_quantity::orientation);
    return { inCS_data.orientation, quantities };
}

template <size_t N>
pose_coordsys const&
pose_cloud<N>:: inCS() const
{
    return inCS_data;
}

/*** Definitions - Coordinate system transformations ***/

template <size_t N>
pose_cloud<N>
pose_cloud<N>:: map_to(pose_coordsys const& to_coordsys) const
{
    pose_coord_tf tf { mapCS(inCS_data.position,    to_coordsys.position),
                       mapCS(inCS_data.orientation, to_coordsys.orientation) };
    return tf(*this);
}

template <size_t N>
pose_cloud<N>
pose_cloud<N>:: map_to(WCS_t) const
{
    return map_to(pose_coordsys{WCS, WCS});
}

template <size_t N>
void
pose_cloud<N>:: fetch(pose_cloud const& cloud)
{
    static_cast<container_type &>(*this) = cloud.map_to(inCS());
}

//______________________ POSE - TRANSFORMATION FUNCTION - Definitions ______________________

/*** Constructors ***/
// No-op transformation from WCS to WCS
inline
pose_coord_tf:: pose_coord_tf()
    = default;

inline
pose_coord_tf:: pose_coord_tf(pose_coord_tf const& copy)
{
    *this = copy;
}

inline
pose_coord_tf &
pose_coord_tf:: operator = (pose_coord_tf const& copy)
{
    position_tf    = copy.position_tf;
    orientation_tf = copy.orientation_tf;

    return *this;
}

inline
pose_coord_tf:: pose_coord_tf( position_coord_tf    const& pos_TF,
                               orientation_coord_tf const& ori_TF )
  : position_tf   { pos_TF }
  , orientation_tf{ ori_TF }
{ }

/*** Projections ***/

inline
pose_coordsys
pose_coord_tf:: from_CS() const
{
    return { position_tf   .from_CS().linked_copy(),
             orientation_tf.from_CS().linked_copy() };
}

inline
pose_coordsys
pose_coord_tf:: to_CS() const
{
    return { position_tf   .to_CS().linked_copy(),
             orientation_tf.to_CS().linked_copy() };
}

/*** Friends: compose ***/

[[nodiscard]]
inline
pose_coord_tf
compose( pose_coord_tf const& left_evaluates_after,
         pose_coord_tf const& right_evaluates_first )
{
    if (right_evaluates_first.to_CS() != left_evaluates_after.from_CS())
        throw std::runtime_error("Arguments are not composable: "
                "Chained coordinate systems do not match");

    return pose_coord_tf{ compose( left_evaluates_after .position_tf,
                                   right_evaluates_first.position_tf ),
                          compose( left_evaluates_after .orientation_tf,
                                   right_evaluates_first.orientation_tf ) };
}

/*** Operator () ***/

[[nodiscard]]
inline
pose_quantity
pose_coord_tf:: operator () (pose_quantity const& q_in) const
{
    return { position_tf    (q_in.position),
             orientation_tf (q_in.orientation) };
}

[[nodiscard]]
inline
pose_value
pose_coord_tf:: operator () (pose_value const& q_in) const
{
    [[unlikely]]
    if ( q_in.inCS().position    != position_tf.from_CS() ||
         q_in.inCS().orientation != orientation_tf.from_CS() )
    {
        throw std::domain_error("Coordinate system mismatch");
    }

    return pose_value{ to_CS(), operator()(static_cast<pose_quantity const&>(q_in)) };
}

[[nodiscard]]
inline
pose_value
pose_coord_tf:: operator () () const
{
    return pose_value{ to_CS(), operator()( pose_quantity{} ) };
}

template <size_t N>
[[nodiscard]]
pose_cloud<N>
pose_coord_tf:: operator () (pose_cloud<N> const& q_in) const
{
    [[unlikely]]
    if ( q_in.inCS().position    != position_tf.from_CS() ||
         q_in.inCS().orientation != orientation_tf.from_CS() )
    {
        throw std::domain_error("Coordinate system mismatch");
    }

    pose_cloud<N> retval {to_CS(), {}};

    if constexpr (N == std::dynamic_extent)
        retval.resize(q_in.size());

    std::ranges::transform(q_in, retval.begin(),
            [&](pose_quantity const& q){return operator () (q);} );

    return retval;
}

//________________________________ POSE - MAPCS ________________________________

/* DOCS: Pose has no associated coordinate system because pose_value is an
 * aggregate of position_value and orientation_value, that is, position
 * and orientation are placed in different coordinate systems.
 * Thus, mapCS and coordinate transformation function are special cases.
 */
export [[nodiscard]] pose_coord_tf mapCS(pose_coordsys const& from, pose_coordsys const& to);
export [[nodiscard]] pose_coord_tf mapCS(pose_coordsys const& from, WCS_t                  );
export [[nodiscard]] pose_coord_tf mapCS(WCS_t,                     pose_coordsys const& to);

/*** Definitions ***/

[[nodiscard]]
inline
pose_coord_tf
mapCS(pose_coordsys const& from, pose_coordsys const& to)
{
    return pose_coord_tf{ mapCS(from.position, to.position),
                          mapCS(from.orientation, to.orientation) };
}

[[nodiscard]]
inline
pose_coord_tf
mapCS(pose_coordsys const& from, WCS_t)
{
    return mapCS(from, pose_coordsys{});
}

[[nodiscard]]
inline
pose_coord_tf
mapCS(WCS_t, pose_coordsys const& to)
{
    return mapCS(pose_coordsys{}, to);
}

//_____________________________ POSITION TREE NODE - Definitions _____________________________

inline
position_coordsys_treenode:: position_coordsys_treenode( position_coordsys const& parent_CS,
                                                         pose_quantity     const& origin_in_parent )
  : coordsys_treenode( {origin_in_parent.position, rotation({}, origin_in_parent.orientation) },
                       parent_CS )
{ }
inline
position_coordsys_treenode:: position_coordsys_treenode( WCS_t, pose_quantity const& origin_in_WCS )
  : position_coordsys_treenode(position_coordsys{}, origin_in_WCS )
{ }

inline
position_coordsys_treenode:: position_coordsys_treenode( position_coordsys const& parent_CS,
                                                         pose              const& origin )
  : position_coordsys_treenode( parent_CS, static_cast<pose_quantity>(origin.map_to(parent_CS)) )
{ }
inline
position_coordsys_treenode:: position_coordsys_treenode( WCS_t, pose const& origin )
  : position_coordsys_treenode( position_coordsys{}, origin )
{ }

inline
void
position_coordsys_treenode:: set_origin_in_parent(pose_quantity const& origin_in_parent)
{
    (*this)->tf_to_parent() = rigid_body_tf { origin_in_parent.position,
                                              {{}, origin_in_parent.orientation} };
}

inline
void
position_coordsys_treenode:: set_origin_in_parent(pose const& origin_in_parent)
{
    set_origin_in_parent( origin_in_parent.map_to(
        dynamic_cast< coordsys_tree_backend<position_coordsys_class> * >
                    (this->backend.get())->parent())    .qty());
}


//______________________ --------------------PLANAR----------------------- ______________________

//_______________________ PLANAR POSITION CS CLASS _______________________

export
struct planar_position_coordsys_class
{
    using quantity_type = planar_position_quantity;
    using tf_data_type  = planar_rigid_body_tf;

    [[nodiscard]] constexpr static tf_data_type invert_tf  ( tf_data_type  const& tf_data );
    [[nodiscard]] constexpr static tf_data_type compose_tf ( tf_data_type  const& after,
                                                             tf_data_type  const& first );
    [[nodiscard]] constexpr static quantity_type transform_coords ( tf_data_type  const& tf_data,
                                                                    quantity_type const& qty );
};

static_assert(coordsys_class<planar_position_coordsys_class>);

export using planar_position_value    = coord_value <planar_position_coordsys_class>;
export using planar_position_point    = coord_value <planar_position_coordsys_class>;
export using          planar_point    = coord_value <planar_position_coordsys_class>;
export template <size_t N = std::dynamic_extent>
       using planar_position_cloud    = coord_cloud <planar_position_coordsys_class, N>;
export using planar_position_coordsys = coordsys    <planar_position_coordsys_class>;
export using planar_position_coord_tf = coord_tf    <planar_position_coordsys_class>;


/*** Definitions ***/

[[nodiscard]]
inline constexpr
planar_position_coordsys_class::tf_data_type
planar_position_coordsys_class:: invert_tf( tf_data_type const& tf_data )
{
    return nin::inv(tf_data);
}

[[nodiscard]]
inline constexpr
planar_position_coordsys_class::tf_data_type
planar_position_coordsys_class:: compose_tf( tf_data_type const& after,
                                        tf_data_type const& first )
{
    return nin::compose(after, first);
}

[[nodiscard]]
inline constexpr
planar_position_coordsys_class::quantity_type
planar_position_coordsys_class:: transform_coords( tf_data_type  const& tf_data,
                                              quantity_type const& qty )
{
    return tf_data(qty);
}

//________________________________ PLANAR POSITION TREE NODE ________________________________

export
class planar_position_coordsys_treenode
  : public coordsys_treenode< coordsys_tree_backend<planar_position_coordsys_class> >
{
public:
    using coordsys_treenode< coordsys_tree_backend<planar_position_coordsys_class> >
                                                                        ::coordsys_treenode;

    explicit planar_position_coordsys_treenode( planar_position_coordsys const& parent_CS,
                                                planar_pose_quantity const& origin_in_parent = {} );
    explicit planar_position_coordsys_treenode( WCS_t,
                                                planar_pose_quantity const& origin_in_WCS = {} );

    explicit planar_position_coordsys_treenode( planar_position_coordsys const& parent_CS,
                                                planar_pose              const& origin );
    explicit planar_position_coordsys_treenode( WCS_t,
                                                planar_pose const& origin );

    void set_origin_in_parent(planar_pose_quantity const& origin_in_parent);
    void set_origin_in_parent(planar_pose          const& origin_in_parent);

    // TODO
    //coordsys_tree_backend<planar_position_coordsys_class>      * operator -> ()       { return dynamic_cast< BE      * >(this->backend.get()); }
    //coordsys_tree_backend<planar_position_coordsys_class> const* operator -> () const { return dynamic_cast< BE const* >(this->backend.get()); }
};

//_______________________ PLANAR ORIENTATION CS CLASS _______________________

export
struct planar_orientation_coordsys_class
{
    using quantity_type = nin::planar_orientation_quantity;
    using tf_data_type  = nin::planar_rotation;

    [[nodiscard]] constexpr static tf_data_type invert_tf  ( tf_data_type  const& tf_data );
    [[nodiscard]] constexpr static tf_data_type compose_tf ( tf_data_type  const& after,
                                                             tf_data_type  const& first );
    [[nodiscard]] constexpr static quantity_type transform_coords ( tf_data_type  const& tf_data,
                                                                    quantity_type const& qty );
};

static_assert(coordsys_class<planar_orientation_coordsys_class>);

export using planar_orientation_value     = coord_value    <planar_orientation_coordsys_class>;
export using planar_orientation           = coord_value    <planar_orientation_coordsys_class>;
export template <size_t N = std::dynamic_extent>
       using planar_orientation_cloud     = coord_cloud    <planar_orientation_coordsys_class, N>;
export using planar_orientation_coordsys  = coordsys       <planar_orientation_coordsys_class>;
export using planar_orientation_coord_tf  = coord_tf       <planar_orientation_coordsys_class>;
export using planar_orientation_coordsys_treenode
                    = coordsys_treenode <coordsys_tree_backend<planar_orientation_coordsys_class>>;


export
nin::length distance( planar_point const& lhs, planar_point const& rhs );


/*** Definitions ***/

[[nodiscard]]
inline constexpr
planar_orientation_coordsys_class::tf_data_type
planar_orientation_coordsys_class:: invert_tf( tf_data_type const& tf_data )
{
    return inv(tf_data);
}

[[nodiscard]]
inline constexpr
planar_orientation_coordsys_class::tf_data_type
planar_orientation_coordsys_class:: compose_tf( tf_data_type const& after,
                                           tf_data_type const& first )
{
    return compose(after, first);
}

[[nodiscard]]
inline constexpr
planar_orientation_coordsys_class::quantity_type
planar_orientation_coordsys_class:: transform_coords( tf_data_type  const& tf_data,
                                                 quantity_type const& value )
{
    return tf_data(value);
}


inline
nin::length
distance( planar_point const& lhs, planar_point const& rhs )
{
    if (lhs.inCS() == rhs.inCS())
    {
        return nin::metres( std::hypot( lhs.x().SI() - rhs.x().SI(),
                                        lhs.y().SI() - rhs.y().SI() ));
    }
    else
    {
        planar_point rtf = {lhs.inCS(), rhs};
        return nin::metres( std::hypot( lhs.x().SI() - rtf.x().SI(),
                                        lhs.y().SI() - rtf.y().SI() ));
    }
}


//__________________ PLANAR ORIENTATION - BACKEND - TAKE FROM POSITION TF __________________

export
class planar_orientation_coordsys_position_backend :
                                        public coordsys_backend<planar_orientation_coordsys_class>
{
public:
    nin::planar_position_coordsys const parent;

    planar_orientation_coordsys_position_backend(planar_position_coordsys const& parent_posCS);

    [[nodiscard]] planar_rotation offset() const override;
    [[nodiscard]] std::shared_ptr<coordsys_backend<planar_orientation_coordsys_class>>
                                                                        clone() const override;
};

export
using planar_orientation_coordsys_from_position_coordsys =
                                    coordsys_generic<planar_orientation_coordsys_position_backend>;


/*** Definitions - Constructor ***/

inline
planar_orientation_coordsys_position_backend::
        planar_orientation_coordsys_position_backend(planar_position_coordsys const& parent_posCS)
  : parent{ parent_posCS.linked_copy() }
{ }

/*** Definitions - Virtual overrides ***/

[[nodiscard]]
inline
planar_rotation
planar_orientation_coordsys_position_backend:: offset() const
{
    return parent.offset().rotation;
}

[[nodiscard]]
inline
std::shared_ptr<coordsys_backend<planar_orientation_coordsys_class>>
planar_orientation_coordsys_position_backend:: clone() const
{
    return std::make_shared<planar_orientation_coordsys_position_backend>(parent);
}

//________________________________ PLANAR POSE - COORDSYS ________________________________

/* DOCS: Pose has no associated coordinate system because pose_value is an
 * aggregate of position_value and orientation_value, that is, position
 * and orientation are placed in different coordinate systems.
 * Thus, mapCS and coordinate transformation function are special cases.
 */

export
struct planar_pose_coordsys
{
    nin::planar_position_coordsys    position;
    nin::planar_orientation_coordsys orientation;

    planar_pose_coordsys();

    planar_pose_coordsys(planar_position_coordsys const& posCS);
    planar_pose_coordsys(WCS_t);

    planar_pose_coordsys(planar_position_coordsys    const& posCS,
                         planar_orientation_coordsys const& oriCS);
    planar_pose_coordsys(WCS_t, planar_orientation_coordsys const& oriCS);
    planar_pose_coordsys(planar_position_coordsys const& posCS, WCS_t);
    planar_pose_coordsys(WCS_t, WCS_t);

    // TODO: Test operator == in (planar)pose with WCS.
    bool operator == (planar_pose_coordsys const& rhs) const;

    planar_pose_value origin() const;
};

//________________________________ PLANAR POSE - VALUE ________________________________

export
class planar_pose_value : public planar_pose_quantity
{
    planar_pose_coordsys inCS_data;

public:
    planar_pose_value();
    planar_pose_value(planar_pose_value const& copy);
    planar_pose_value(planar_pose_value     && move);
    planar_pose_value & operator = (planar_pose_value const& copy);
    planar_pose_value & operator = (planar_pose_value     && move);

    planar_pose_value(planar_pose_quantity const& qty_arg);
    planar_pose_value(planar_pose_coordsys const& in_coordsys,
                      planar_pose_quantity const& qty_arg = {});
    planar_pose_value(WCS_t, planar_pose_quantity const& qty_arg = {});

    nin::planar_point       position_value() const;
    nin::planar_orientation orientation_value() const;
    planar_pose_coordsys const& inCS() const;

    planar_pose_value map_to(planar_pose_coordsys const& to_coordsys) const;
    planar_pose_value map_to(WCS_t) const;
    void fetch(planar_pose_value const& value);

    planar_pose_quantity const& qty() const;
    planar_pose_quantity      & qty();
};

//________________________________ PLANAR POSE - CLOUD ________________________________

export
template <size_t N = std::dynamic_extent>
class planar_pose_cloud : public type_ops::sequence_container_t< planar_pose_quantity, N >
{
    planar_pose_coordsys inCS_data;

public:
    using container_type = type_ops::sequence_container_t< planar_pose_quantity, N >;

    planar_pose_cloud();
    planar_pose_cloud(planar_pose_cloud const& copy);
    planar_pose_cloud(planar_pose_cloud     && move);
    planar_pose_cloud & operator = (planar_pose_cloud const& copy);
    planar_pose_cloud & operator = (planar_pose_cloud     && move);

    planar_pose_cloud(container_type const& quantities);
    planar_pose_cloud(planar_pose_coordsys const& in_coordsys, container_type const& qties);
    planar_pose_cloud(WCS_t,                                   container_type const& qties);

    nin::position_cloud<N>    position_values() const;
    nin::orientation_cloud<N> orientation_values() const;

    planar_pose_coordsys const& inCS() const;
    planar_pose_cloud map_to(planar_pose_coordsys const& to_coordsys) const;
    planar_pose_cloud map_to(WCS_t) const;
    void fetch(planar_pose_cloud const& cloud);
};

//___________________________ PLANAR POSE - TRANSFORMATION FUNCTION ___________________________

export
class planar_pose_coord_tf
{
    planar_position_coord_tf    position_tf;
    planar_orientation_coord_tf orientation_tf;

public:
    planar_pose_coord_tf();
    planar_pose_coord_tf(planar_pose_coord_tf const& copy);
    planar_pose_coord_tf & operator = (planar_pose_coord_tf const& copy);
    // TODO: Move constructor, move assignment operator

    explicit
    planar_pose_coord_tf( planar_position_coord_tf    const& pos_TF,
                          planar_orientation_coord_tf const& ori_TF );


    planar_pose_coordsys from_CS() const;
    planar_pose_coordsys to_CS() const;

    planar_pose_coord_tf friend compose( planar_pose_coord_tf const& left_evaluates_after,
                                         planar_pose_coord_tf const& right_evaluates_first );

    [[nodiscard]] planar_pose_quantity operator () (planar_pose_quantity const& q_in) const;
    [[nodiscard]] planar_pose_value    operator () (planar_pose_value    const& q_in) const;
    [[nodiscard]] planar_pose_value    operator () () const;
    template <size_t N>
    [[nodiscard]] planar_pose_cloud<N> operator () (planar_pose_cloud<N> const& q_in) const;

};

//___________________________ PLANAR POSE - COORDSYS - Definitions ___________________________

/*** Constructors ***/

inline
planar_pose_coordsys:: planar_pose_coordsys()
    = default;

// DOCS: Note that this is the only case when orientation_coordsys_from_position_coordsys
//       is created by default. It will trigger this constructor with position_coordsys{}
//       but not with the WCS tag.
inline
planar_pose_coordsys:: planar_pose_coordsys( planar_position_coordsys const& posCS )
  : position    { posCS.linked_copy() }
  , orientation { planar_orientation_coordsys_from_position_coordsys(position) }
{ }

inline
planar_pose_coordsys:: planar_pose_coordsys( WCS_t )
  : planar_pose_coordsys()
{ }

inline
planar_pose_coordsys:: planar_pose_coordsys( planar_position_coordsys    const& posCS,
                                             planar_orientation_coordsys const& oriCS )
  : position    { posCS.linked_copy() }
  , orientation { oriCS.linked_copy() }
{ }

inline
planar_pose_coordsys:: planar_pose_coordsys( WCS_t,
                                             planar_orientation_coordsys const& oriCS )
  : planar_pose_coordsys{ planar_position_coordsys{WCS}, oriCS }
{ }

inline
planar_pose_coordsys:: planar_pose_coordsys( planar_position_coordsys const& posCS,
                                             WCS_t )
  : planar_pose_coordsys{ posCS, planar_orientation_coordsys{WCS} }
{ }

inline
planar_pose_coordsys:: planar_pose_coordsys( WCS_t,
                                             WCS_t )
  : planar_pose_coordsys()
{ }

/*** Other ***/

inline
bool
planar_pose_coordsys:: operator == (planar_pose_coordsys const& rhs) const
    = default;

inline
planar_pose_value
planar_pose_coordsys:: origin () const
{
    return planar_pose_value{*this,{}};
}


//_____________________________ PLANAR POSE - VALUE - Definitions _____________________________

// DOCS: When WCS tag is used, pos and ori are set to WCS.
//       But when planar_pose_coordsys{WCS}, ori is set to default value defined in class
//       definition. Comparison of first case is ok, but comparison of second case yields false
//       when planar_pose_coordsys{WCS} != planar_pose_coordsys{WCS}.

/*** Definitions - Semiregular traits ***/

inline
planar_pose_value:: planar_pose_value()
    = default;

inline
planar_pose_value:: planar_pose_value(planar_pose_value const& copy)
{
    *this = copy;
}

inline
planar_pose_value:: planar_pose_value(planar_pose_value && move)
    = default;

inline
planar_pose_value &
planar_pose_value:: operator = (planar_pose_value const& copy)
{
    static_cast<planar_pose_quantity &>(*this) = copy;
    inCS_data.position    = copy.position_value().inCS().linked_copy();
    inCS_data.orientation = copy.orientation_value().inCS().linked_copy();
    return *this;
}

inline
planar_pose_value &
planar_pose_value:: operator = (planar_pose_value && move)
    = default;

/*** Definitions - Other constructors ***/

inline
planar_pose_value:: planar_pose_value(planar_pose_quantity const& qty_arg)
  : planar_pose_quantity{ qty_arg }
{ }

inline
planar_pose_value:: planar_pose_value( planar_pose_coordsys const& in_coordsys,
                                       planar_pose_quantity const& qty_arg )
  : planar_pose_quantity{ qty_arg }
  , inCS_data{ in_coordsys.position.linked_copy(),
               in_coordsys.orientation.linked_copy() }
{ }

inline
planar_pose_value:: planar_pose_value(WCS_t, planar_pose_quantity const& qty_arg)
  : planar_pose_value{ planar_pose_coordsys{WCS, WCS}, qty_arg }
{ }

/*** Definitions - Projections ***/

inline
nin::planar_point
planar_pose_value:: position_value() const
{
    return nin::planar_point{ inCS_data.position, planar_pose_quantity::position };
}

inline
nin::planar_orientation
planar_pose_value:: orientation_value() const
{
    return nin::planar_orientation{ inCS_data.orientation, planar_pose_quantity::orientation };
}

inline
planar_pose_coordsys const&
planar_pose_value:: inCS() const
{
    return inCS_data;
}

inline
planar_pose_quantity const&
planar_pose_value:: qty() const
{
    return *this;
}

inline
planar_pose_quantity &
planar_pose_value:: qty()
{
    return *this;
}


/*** Definitions - Coordinate system transformations ***/

inline
planar_pose_value
planar_pose_value:: map_to(planar_pose_coordsys const& to_coordsys) const
{
    planar_pose_coord_tf tf { mapCS(inCS_data.position,    to_coordsys.position),
                              mapCS(inCS_data.orientation, to_coordsys.orientation) };
    return tf(*this);
}

inline
planar_pose_value
planar_pose_value:: map_to(WCS_t) const
{
    return map_to(planar_pose_coordsys{WCS, WCS});
}

inline
void
planar_pose_value:: fetch(planar_pose_value const& value)
{
    static_cast<planar_pose_quantity &>(*this) = value.map_to(inCS());
}

//___________________________ PLANAR POSE - CLOUD - Definitions ___________________________

// DOCS: When WCS tag is used, pos and ori are set to WCS.
//       But when pose_coordsys{WCS}, ori is set to default value defined in class definition.
//       Comparison of first case is ok, but comparison of second case yields false
//       when pose_coordsys{WCS} != pose_coordsys{WCS}.

/*** Definitions - Semiregular traits ***/

template <size_t N>
planar_pose_cloud<N>:: planar_pose_cloud()
    = default;

template <size_t N>
planar_pose_cloud<N>:: planar_pose_cloud(planar_pose_cloud const& copy)
{
    *this = copy;
}

template <size_t N>
planar_pose_cloud<N>:: planar_pose_cloud(planar_pose_cloud && move)
    = default;

template <size_t N>
planar_pose_cloud<N> &
planar_pose_cloud<N>:: operator = (planar_pose_cloud const& copy)
{
    static_cast<container_type &>(*this) = copy;
    inCS_data.position    = copy.inCS_data.position.linked_copy();
    inCS_data.orientation = copy.inCS_data.orientation.linked_copy();
    return *this;
}

template <size_t N>
planar_pose_cloud<N> &
planar_pose_cloud<N>:: operator = (planar_pose_cloud && move)
    = default;

/*** Definitions - Other constructors ***/

template <size_t N>
planar_pose_cloud<N>:: planar_pose_cloud(container_type const& quantities)
  : planar_pose_quantity{ quantities }
{ }

template <size_t N>
planar_pose_cloud<N>:: planar_pose_cloud( planar_pose_coordsys const& in_coordsys,
                                          container_type const& quantities )
  : container_type{ quantities }
  , inCS_data{ in_coordsys.position.linked_copy(),
               in_coordsys.orientation.linked_copy() }
{ }

template <size_t N>
planar_pose_cloud<N>:: planar_pose_cloud(WCS_t, container_type const& quantities)
  : planar_pose_cloud{ planar_pose_coordsys{WCS, WCS}, quantities }
{ }

/*** Definitions - Projections ***/

template <size_t N>
nin::position_cloud<N>
planar_pose_cloud<N>:: position_values() const
{
    typename position_cloud<N>::container_type quantities;
    if constexpr (N == std::dynamic_extent)
        quantities.resize(this->size());
    std::ranges::copy(static_cast<container_type const&>(*this), {},
                                            &planar_pose_quantity::position);
    return { inCS_data.position, quantities };
}

template <size_t N>
nin::orientation_cloud<N>
planar_pose_cloud<N>:: orientation_values() const
{
    typename orientation_cloud<N>::container_type quantities;
    if constexpr (N == std::dynamic_extent)
        quantities.resize(this->size());
    std::ranges::copy(static_cast<container_type const&>(*this), {},
                                            &planar_pose_quantity::orientation);
    return { inCS_data.orientation, quantities };
}

template <size_t N>
planar_pose_coordsys const&
planar_pose_cloud<N>:: inCS() const
{
    return inCS_data;
}

/*** Definitions - Coordinate system transformations ***/

template <size_t N>
planar_pose_cloud<N>
planar_pose_cloud<N>:: map_to(planar_pose_coordsys const& to_coordsys) const
{
    planar_pose_coord_tf tf { mapCS(inCS_data.position,    to_coordsys.position),
                       mapCS(inCS_data.orientation, to_coordsys.orientation) };
    return tf(*this);
}

template <size_t N>
planar_pose_cloud<N>
planar_pose_cloud<N>:: map_to(WCS_t) const
{
    return map_to(planar_pose_coordsys{WCS, WCS});
}

template <size_t N>
void
planar_pose_cloud<N>:: fetch(planar_pose_cloud const& cloud)
{
    static_cast<planar_pose_quantity &>(*this) = cloud.map_to(inCS());
}

//______________________ PLANAR POSE - TRANSFORMATION FUNCTION - Definitions ______________________

/*** Constructors ***/
// No-op transformation from WCS to WCS
inline
planar_pose_coord_tf:: planar_pose_coord_tf()
    = default;

inline
planar_pose_coord_tf:: planar_pose_coord_tf(planar_pose_coord_tf const& copy)
{
    *this = copy;
}

inline
planar_pose_coord_tf &
planar_pose_coord_tf:: operator = (planar_pose_coord_tf const& copy)
{
    position_tf    = copy.position_tf;
    orientation_tf = copy.orientation_tf;

    return *this;
}

inline
planar_pose_coord_tf:: planar_pose_coord_tf( planar_position_coord_tf    const& pos_TF,
                                             planar_orientation_coord_tf const& ori_TF )
  : position_tf   { pos_TF }
  , orientation_tf{ ori_TF }
{ }

/*** Projections ***/

inline
planar_pose_coordsys
planar_pose_coord_tf:: from_CS() const
{
    return { position_tf   .from_CS().linked_copy(),
             orientation_tf.from_CS().linked_copy() };
}

inline
planar_pose_coordsys
planar_pose_coord_tf:: to_CS() const
{
    return { position_tf   .to_CS().linked_copy(),
             orientation_tf.to_CS().linked_copy() };
}

/*** Friends: compose ***/

[[nodiscard]]
inline
planar_pose_coord_tf
compose( planar_pose_coord_tf const& left_evaluates_after,
         planar_pose_coord_tf const& right_evaluates_first )
{
    if (right_evaluates_first.to_CS() != left_evaluates_after.from_CS())
        throw std::runtime_error("Arguments are not composable: "
                                 "Chained coordinate systems do not match");

    return planar_pose_coord_tf{ compose( left_evaluates_after .position_tf,
                                   right_evaluates_first.position_tf ),
                          compose( left_evaluates_after .orientation_tf,
                                   right_evaluates_first.orientation_tf ) };
}

/*** Operator () ***/

[[nodiscard]]
inline
planar_pose_quantity
planar_pose_coord_tf:: operator () (planar_pose_quantity const& q_in) const
{
    return { position_tf    (q_in.position),
             orientation_tf (q_in.orientation) };
}

[[nodiscard]]
inline
planar_pose_value
planar_pose_coord_tf:: operator () (planar_pose_value const& q_in) const
{
    [[unlikely]]
    if ( q_in.inCS().position    != position_tf.from_CS() ||
         q_in.inCS().orientation != orientation_tf.from_CS() )
    {
        throw std::domain_error("Coordinate system mismatch");
    }

    return planar_pose_value{ to_CS(), operator()(static_cast<planar_pose_quantity const&>(q_in)) };
}

[[nodiscard]]
inline
planar_pose_value
planar_pose_coord_tf:: operator () () const
{
    return planar_pose_value{ to_CS(), operator()( planar_pose_quantity{} ) };
}

template <size_t N>
[[nodiscard]]
planar_pose_cloud<N>
planar_pose_coord_tf:: operator () (planar_pose_cloud<N> const& q_in) const
{
    [[unlikely]]
    if ( q_in.inCS().position    != position_tf.from_CS() ||
         q_in.inCS().orientation != orientation_tf.from_CS() )
    {
        throw std::domain_error("Coordinate system mismatch");
    }

    planar_pose_cloud<N> retval {to_CS(), {}};

    if constexpr (N == std::dynamic_extent)
        retval.resize(q_in.size());

    std::ranges::transform(q_in, retval.begin(),
            [&](planar_pose_quantity const& q){return operator () (q);} );

    return retval;
}

//________________________________ PLANAR POSE - MAPCS ________________________________

/* DOCS: Pose has no associated coordinate system because pose_value is an
 * aggregate of position_value and orientation_value, that is, position
 * and orientation are placed in different coordinate systems.
 * Thus, mapCS and coordinate transformation function are special cases.
 */
export [[nodiscard]] planar_pose_coord_tf mapCS( planar_pose_coordsys const& from,
                                                 planar_pose_coordsys const& to );
export [[nodiscard]] planar_pose_coord_tf mapCS( planar_pose_coordsys const& from,
                                                 WCS_t );
export [[nodiscard]] planar_pose_coord_tf mapCS( WCS_t,
                                                 planar_pose_coordsys const& to );

/*** Definitions ***/

[[nodiscard]]
inline
planar_pose_coord_tf
mapCS(planar_pose_coordsys const& from, planar_pose_coordsys const& to)
{
    return planar_pose_coord_tf{ mapCS(from.position, to.position),
                                 mapCS(from.orientation, to.orientation) };
}

[[nodiscard]]
inline
planar_pose_coord_tf
mapCS(planar_pose_coordsys const& from, WCS_t)
{
    return mapCS(from, planar_pose_coordsys{});
}

[[nodiscard]]
inline
planar_pose_coord_tf
mapCS(WCS_t, planar_pose_coordsys const& to)
{
    return mapCS(planar_pose_coordsys{}, to);
}

//__________________________ PLANAR POSITION TREE NODE - Definitions __________________________

inline
planar_position_coordsys_treenode:: planar_position_coordsys_treenode(
                                                    planar_position_coordsys const& parent_CS,
                                                    planar_pose_quantity const& origin_in_parent )
  : coordsys_treenode( {origin_in_parent.position, origin_in_parent.orientation.to_angle()},
                       parent_CS )
{ }
inline
planar_position_coordsys_treenode:: planar_position_coordsys_treenode(WCS_t,
                                                       planar_pose_quantity const& origin_in_WCS)
  : planar_position_coordsys_treenode (planar_position_coordsys{}, origin_in_WCS)
{ }

inline
planar_position_coordsys_treenode:: planar_position_coordsys_treenode(
                                                    planar_position_coordsys const& parent_CS,
                                                    planar_pose const& origin )
  : planar_position_coordsys_treenode( parent_CS,
                static_cast<planar_pose_quantity>(origin.map_to(parent_CS)) )
{ }
inline
planar_position_coordsys_treenode:: planar_position_coordsys_treenode(WCS_t,
                                                       planar_pose const& origin)
  : planar_position_coordsys_treenode (planar_position_coordsys{}, origin)
{ }

inline
void
planar_position_coordsys_treenode:: set_origin_in_parent(
                                                    planar_pose_quantity const& origin_in_parent)
{
    (*this)->tf_to_parent() = planar_rigid_body_tf { origin_in_parent.position,
                                                     {{}, origin_in_parent.orientation} };
}

inline
void
planar_position_coordsys_treenode:: set_origin_in_parent(planar_pose const& origin_in_parent)
{
    set_origin_in_parent( origin_in_parent.map_to(
        dynamic_cast< coordsys_tree_backend<planar_position_coordsys_class> * >
                    (this->backend.get())->parent())    .qty());
}


//______________________ --------------------BRIDGES-------------------- ______________________

//_______________________ COORDINATE BRIDGES _______________________

/*** Position <===> PlanarPosition ***/

export
struct bridge_point_and_planar_point : coordsys<position_coordsys_class>,
                                       coordsys<planar_position_coordsys_class>
{
    using coordsys_class_1 = position_coordsys_class;
    using coordsys_class_2 = planar_position_coordsys_class;

    [[nodiscard]]
    static
    planar_position_quantity
    convert(position_quantity q)
    {
        return {q.x(), q.y()};
    }

    [[nodiscard]]
    static
    position_quantity
    convert(planar_position_quantity q)
    {
        return {q.x(), q.y(), units::metres(0)};
    }
};

//_______________________ FOOTER _______________________

} // namespace nin

