/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
module;

#include "type_ops.hh"

#include <exception>
#include <memory>

export module ninbot.pos:coordsys;
import :clifford;


// TODO: constexpr, noexcept, [[nodiscard]], const

//___________________________ PROLOGUE ____________________________
namespace nin
{

/*** WCS - World Coordinate System ***/

// DOCS: WCS - World Coordinate System tag
//       Vocabulary follows ISO 9787:2013
//       Used as a placeholder in arguments of type coordsys<CC>
//       The WCS tag is used as a replacement of coordinate system type on function arguments.

export
struct WCS_t { } inline WCS;

//________________________ COORDINATE SYSTEM CLASS _______________________

/*
 * DOCS:
 * CC::tf_data_type holds the data necessary to transform from from_CS to to_CS
 * coordsys_class defines the coordinate values and coordinate transformation functions
 * of a coordinate space.
 */
export
template <typename CC>
concept coordsys_class =

        requires
        {
            typename CC::quantity_type;
            typename CC::tf_data_type;
        }

     && std::semiregular<typename CC::quantity_type>
     && std::semiregular<typename CC::tf_data_type>
     && !std::same_as<typename CC::quantity_type, typename CC::tf_data_type>

     && requires (CC::tf_data_type const tf_data)
        {
            { CC::invert_tf(tf_data) } // FIXME: noexcept
                                  -> std::same_as<typename CC::tf_data_type>;
        }

     && requires (CC::tf_data_type const  leftside_evaluates_after,
                  CC::tf_data_type const rightside_evaluates_first)
        {
            { CC::compose_tf( leftside_evaluates_after, rightside_evaluates_first ) } // FIXME: noexcept
                                  -> std::same_as<typename CC::tf_data_type>;
        }

     && requires (CC::tf_data_type  const tf_data,
                  CC::quantity_type const coords)
        {
            { CC::transform_coords( tf_data, coords ) } // FIXME: noexcept
                                  -> std::convertible_to<typename CC::quantity_type>;
        };


export
template <coordsys_class CC> class coordsys;

//________________________________ DATA AGGREGATES - QUANTITY ____________________________

export
template <coordsys_class CC>
using coord_qty = CC::quantity_type;

//________________________________ DATA AGGREGATES - VALUE ____________________________

export
template <coordsys_class CC>
class coord_value : public coord_qty<CC>
{
    coordsys<CC> inCS_data;

public:
    using coordsys_class = CC;

    coord_value();
    coord_value(coord_value const& copy);
    coord_value(coord_value     && move);
    coord_value & operator = (coord_value const& copy);
    coord_value & operator = (coord_value     && move);

    bool operator == (coord_value const& rhs) const = delete;

    explicit coord_value(coord_qty<CC> const& qty_arg);
    coord_value(coordsys<CC> const& in_coordsys, coord_qty<CC> const& qty_arg = {});
    coord_value(WCS_t,                           coord_qty<CC> const& qty_arg = {});
    coord_value(coordsys<CC> const& in_coordsys, coord_value<CC> const& value);
    coord_value(WCS_t,                           coord_value<CC> const& value);

    coordsys<CC> const& inCS() const;

    coord_value map_to(coordsys<CC> const& to_coordsys) const;
    coord_value map_to(WCS_t) const;

    coord_qty<CC> const& qty() const;
    coord_qty<CC>      & qty();
};

//________________________________ DATA AGGREGATES - CLOUD ____________________________

export
template <coordsys_class CC, size_t N = std::dynamic_extent>
class coord_cloud : public type_ops::sequence_container_t< coord_qty<CC>, N >
{
    coordsys<CC> inCS_data;

public:
    using coordsys_class = CC;
    using container_type = type_ops::sequence_container_t< coord_qty<CC>, N >;

    coord_cloud();
    coord_cloud(coord_cloud const& copy);
    coord_cloud(coord_cloud     && move);
    coord_cloud & operator = (coord_cloud const& copy);
    coord_cloud & operator = (coord_cloud     && move);

    explicit coord_cloud(container_type const& qties);
    coord_cloud(coordsys<CC> const& in_coordsys, container_type const& qties = {});
    coord_cloud(WCS_t,                          container_type const& qties = {});
    // TODO: Check: are these needed, or is the container type deduced even with init_list?
    coord_cloud(coordsys<CC> const& in_coordsys, std::initializer_list<coord_qty<CC>> qties);
    coord_cloud(WCS_t,                          std::initializer_list<coord_qty<CC>> qties);

    coordsys<CC> const& inCS() const;

    coord_cloud map_to(coordsys<CC> const& to_coordsys) const;
    coord_cloud map_to(WCS_t) const;

    void fetch(coord_cloud const& cloud);
};


//________________________________ DATA AGGREGATES - REGION ____________________________

// TODO

//_______________________ COORDINATE BRIDGE CONCEPTS _______________________

/*** coordsys_bridge_traits ***/

export
template< typename B >
concept coordsys_bridge_traits =
        requires
        {
            typename B::coordsys_class_1;
            typename B::coordsys_class_2;
        }

        // DOCS: Bridge must be between different coordinate spaces.
     && !std::same_as< typename B::coordsys_class_1, typename B::coordsys_class_2 >

     && coordsys_class<typename B::coordsys_class_1>
     && coordsys_class<typename B::coordsys_class_2>

     && std::convertible_to<B, coordsys<typename B::coordsys_class_1>>
     && std::convertible_to<B, coordsys<typename B::coordsys_class_2>>

     // DOCS: A bridge has tight constraints
     && ( sizeof(B) == sizeof(coordsys<typename B::coordsys_class_1>) +
                       sizeof(coordsys<typename B::coordsys_class_2>) )

     && requires ( coord_qty<typename B::coordsys_class_1> const q1,
                   coord_qty<typename B::coordsys_class_2> const q2 )
        {
            // DOCS: convert is expected to be static with no side effects.
            //       All information needed by convert should be contained in the
            //       coord_tf_data of the coordinate system class.
            {B::convert(q1)} -> std::convertible_to< coord_qty<typename B::coordsys_class_2> >;
            {B::convert(q2)} -> std::convertible_to< coord_qty<typename B::coordsys_class_1> >;
        };


/*** coordsys_bridge_dual_traits ***/

template <coordsys_bridge_traits B, coordsys_class CC>
struct coordsys_bridge_dual_traits
{
    using coordsys_class = B::coordsys_class_2;
};

template <coordsys_bridge_traits B, coordsys_class CC>
requires std::same_as<typename B::coordsys_class_2, CC>
struct coordsys_bridge_dual_traits<B, CC>
{
    using coordsys_class = B::coordsys_class_1;
};

template <coordsys_bridge_traits B, coordsys_class CC>
using coordsys_bridge_dual_traits_t = coordsys_bridge_dual_traits<B,CC>::coordsys_class;

/*** coordsys_in_bridge ***/

template< typename F, typename B, typename CC >
concept coordsys_in_bridge =
      coordsys_class<F>
   && coordsys_bridge_traits<B>
   && coordsys_class<CC>

   && ((std::same_as<F, typename B::coordsys_class_1> && std::same_as<CC, typename B::coordsys_class_2>) ||
       (std::same_as<F, typename B::coordsys_class_2> && std::same_as<CC, typename B::coordsys_class_1>));

/*** coordsys_dual_bridge ***/

export
template <coordsys_bridge_traits B>
struct coordsys_dual_bridge : B
{
    using coordsys_class_1 = typename B::coordsys_class_2;
    using coordsys_class_2 = typename B::coordsys_class_1;
};

//_____________________ COORDINATE TRANSFORMATION FUNCTION - Simple____________________

export
template< coordsys_class F, coordsys_class CC = F, coordsys_bridge_traits ... B >
class coord_tf;

export
template <coordsys_class CC> [[nodiscard]] coord_tf<CC>
inv(coord_tf<CC> const& transformation_function);
export
template <coordsys_class CC> [[nodiscard]] coord_tf<CC>
compose(coord_tf<CC> const& left_evaluates_after, coord_tf<CC> const& right_evaluates_first );

export
template <coordsys_class CC>
class coord_tf <CC>
{
    CC::tf_data_type tf_data;

    coordsys<CC> from_CS_data;
    coordsys<CC> to_CS_data;

public:
    using coordsys_classs = std::tuple<CC>;
    using bridge_types = std::tuple<>;

    coord_tf();
    coord_tf(coord_tf const& copy);
    coord_tf const& operator = (coord_tf const& copy);
    // TODO: Move constructor, move operator

    explicit
    coord_tf( coordsys<CC>     const& From,
              CC::tf_data_type const& TFdata,
              coordsys<CC>     const& To );

    coordsys<CC>     const& from_CS() const;
    coordsys<CC>     const&   to_CS() const;

    CC::tf_data_type const& data() const;

    coord_tf friend inv<>    ( coord_tf const& transformation_function );
    coord_tf friend compose<>( coord_tf const& left_evaluates_after,
                               coord_tf const& right_evaluates_first );

    // DOCS: Efficient function because there is no coordsys checking (coord_qty)
    [[nodiscard]] coord_qty<CC>     operator () (coord_qty<CC>     const& q_in) const;
    [[nodiscard]] coord_value<CC>   operator () (coord_value<CC>   const& q_in) const;
    [[nodiscard]] coord_value<CC>   operator () () const;
    template <size_t N>
    [[nodiscard]] coord_cloud<CC,N> operator () (coord_cloud<CC,N> const& q_in) const;

};

//________________ COORDINATE TRANSFORMATION FUNCTION - Simple - Definitions _______________

/*** Constructors ***/
// No-op transformation from WCS to WCS
template <coordsys_class CC>
coord_tf<CC>:: coord_tf()
    = default;

template <coordsys_class CC>
coord_tf<CC>:: coord_tf(coord_tf const& copy)
{
    *this = copy;
}

template <coordsys_class CC>
coord_tf<CC> const&
coord_tf<CC>:: operator = (coord_tf const& copy)
{
    tf_data      = copy.tf_data;
    from_CS_data = copy.from_CS_data.linked_copy();
    to_CS_data   = copy.to_CS_data.linked_copy();
    return *this;
}

template <coordsys_class CC>
coord_tf<CC>:: coord_tf( coordsys<CC>     const& From,
                        CC::tf_data_type const& TFdata,
                        coordsys<CC>     const& To )
  : tf_data      { TFdata        }
  , from_CS_data { From.linked_copy() }
  , to_CS_data   { To.linked_copy()   }
{ }

/*** Projections ***/

template <coordsys_class CC>
coordsys<CC> const&
coord_tf<CC>:: from_CS() const
{
    return from_CS_data;
}

template <coordsys_class CC>
coordsys<CC> const&
coord_tf<CC>:: to_CS() const
{
    return to_CS_data;
}

template <coordsys_class CC>
CC::tf_data_type const&
coord_tf<CC>:: data() const
{
    return tf_data;
}

/*** Friends: invert, compose ***/
template <coordsys_class CC>
[[nodiscard]] coord_tf<CC>
inv( coord_tf<CC> const& transformation_function )
{
    return coord_tf<CC>( transformation_function.to_CS_data,
                        CC::invert_tf(transformation_function.tf_data),
                        transformation_function.from_CS_data );
}

template <coordsys_class CC>
[[nodiscard]] coord_tf<CC>
compose( coord_tf<CC> const& left_evaluates_after,
         coord_tf<CC> const& right_evaluates_first )
{
    if (right_evaluates_first.to_CS_data != left_evaluates_after.from_CS_data)
        throw std::runtime_error("Arguments are not composable: "
                "Chained coordinate systems do not match");

    return coord_tf<CC>{ right_evaluates_first.from_CS_data,
                        CC::compose_tf(left_evaluates_after.tf_data, right_evaluates_first.tf_data),
                        left_evaluates_after.to_CS_data };
}


/*** Operator () ***/
template <coordsys_class CC>
[[nodiscard]] coord_qty<CC>
coord_tf<CC>:: operator () (coord_qty<CC> const& q_in) const
{
    return CC::transform_coords(tf_data, q_in);
}

template <coordsys_class CC>
[[nodiscard]] coord_value<CC>
coord_tf<CC>:: operator () (coord_value<CC> const& q_in) const
{
    [[unlikely]]
    if (q_in.inCS() != from_CS_data)
        throw std::domain_error("Coordinate system mismatch");

    return {to_CS_data, CC::transform_coords(tf_data, q_in)};
}

template <coordsys_class CC>
[[nodiscard]] coord_value<CC>
coord_tf<CC>:: operator () () const
{
    return operator () (coord_value<CC>{from_CS_data});
}

template <coordsys_class CC>
template <size_t N>
[[nodiscard]] coord_cloud<CC, N>
coord_tf<CC>:: operator () (coord_cloud<CC, N> const& q_in) const
{
    [[unlikely]]
    if (q_in.inCS() != from_CS_data)
        throw std::domain_error("Coordinate system mismatch");

    coord_cloud<CC,N> retval {to_CS_data, {}};

    if constexpr (N == std::dynamic_extent)
        retval.resize(q_in.size());

    std::ranges::transform(q_in, retval.begin(),
            [&](coord_qty<CC> const& q){return CC::transform_coords(tf_data, q);} );

    return retval;
}


//_____________________ COORDINATE TRANSFORMATION FUNCTION - Bridged

export
template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
[[nodiscard]] coord_tf<F,CC,B>
inv(coord_tf<F,CC,B> const& transformation_function);
// TODO: Compose(), but before that I need a multi-bridge specialization of coord_tf

export
template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
class coord_tf <F, CC, B>
{
    F::tf_data_type from_data;
    CC::tf_data_type to_data;

    coordsys<F> from_CS_data;
    coordsys<CC> to_CS_data;

public:
    using coordsys_classs = std::tuple<F,CC>;
    using bridge_types = std::tuple<B>;

    coord_tf();
    coord_tf(coord_tf const& copy);
    coord_tf const& operator = (coord_tf const& copy);
    // TODO: Move constructor, move operator

    explicit
    coord_tf( coordsys<F>     const& From,
              F::tf_data_type const& TFdata_from,
              CC::tf_data_type const& TFdata_to,
              coordsys<CC>     const& To );

    coordsys<F> const& from_CS() const;
    coordsys<CC> const& to_CS() const;

    coord_tf friend inv<>( coord_tf const& transformation_function );
    // TODO: compose

    // DOCS: Efficient function because there is no coordsys checking (coord_qty)
    [[nodiscard]] coord_qty<CC>     operator () (coord_qty<F>     const& q_in) const;
    [[nodiscard]] coord_value<CC>   operator () (coord_value<F>   const& q_in) const;
    [[nodiscard]] coord_value<CC>   operator () () const;
    template <size_t N>
    [[nodiscard]] coord_cloud<CC,N> operator () (coord_cloud<F,N> const& q_in) const;

};

//________________ COORDINATE TRANSFORMATION FUNCTION - Bridged - Definitions _______________

/*** Constructors ***/
// No-op transformation from WCS to WCS
template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
coord_tf<F,CC,B>:: coord_tf()
    = default;

template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
coord_tf<F,CC,B>:: coord_tf(coord_tf const& copy)
{
    *this = copy;
}

template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
coord_tf<F,CC,B> const&
coord_tf<F,CC,B>:: operator = (coord_tf const& copy)
{
    from_data    = copy.from_data;
    to_data      = copy.to_data;
    from_CS_data = copy.from_CS_data.linked_copy();
    to_CS_data   = copy.to_CS_data.linked_copy();
    return *this;
}

template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
coord_tf<F,CC,B>:: coord_tf( coordsys<F>     const& From,
                            F::tf_data_type const& TFdata_from,
                            CC::tf_data_type const& TFdata_to,
                            coordsys<CC>     const& To )
  : from_data    { TFdata_from   }
  , to_data      { TFdata_to     }
  , from_CS_data { From.linked_copy() }
  , to_CS_data   { To.linked_copy()   }
{ }

/*** Projections ***/

template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
coordsys<F> const&
coord_tf<F,CC,B>:: from_CS() const
{
    return from_CS_data;
}

template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
coordsys<CC> const&
coord_tf<F,CC,B>:: to_CS() const
{
    return to_CS_data;
}

/*** Friends: invert, compose ***/
template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
[[nodiscard]] coord_tf<CC>
inv( coord_tf<F,CC,B> const& transformation_function )
{
    return coord_tf<CC,F,B>( transformation_function.to_CS_data,
                            CC::invert_tf(transformation_function.to_data),
                            F::invert_tf(transformation_function.from_data),
                            transformation_function.from_CS_data );
}


/*** Operator () ***/
template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
[[nodiscard]] coord_qty<CC>
coord_tf<F,CC,B>:: operator () (coord_qty<F> const& q_in) const
{
    coord_qty<F> bridge_from_side = F::transform_coords(from_data, q_in);
    coord_qty<CC> bridge_to_side   = B::convert(bridge_from_side);
    return CC::transform_coords(to_data, bridge_to_side);
}

template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
[[nodiscard]] coord_value<CC>
coord_tf<F,CC,B>:: operator () (coord_value<F> const& q_in) const
{
    [[unlikely]]
    if (q_in.inCS() != from_CS_data)
        throw std::domain_error("Coordinate system mismatch");

    return { to_CS_data, this->operator () (q_in.qty()) };
}

template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
[[nodiscard]] coord_value<CC>
coord_tf<F,CC,B>:: operator () () const
{
    return operator () (coord_value<F>{from_CS_data, {}});
}

template <coordsys_class F, coordsys_class CC, coordsys_bridge_traits B>
    requires coordsys_in_bridge<F, B, CC>
template <size_t N>
[[nodiscard]] coord_cloud<CC, N>
coord_tf<F,CC,B>:: operator () (coord_cloud<F, N> const& q_in) const
{
    [[unlikely]]
    if (q_in.inCS() != from_CS_data)
        throw std::domain_error("Coordinate system mismatch");

    coord_cloud<CC,N> retval {to_CS_data, {}};

    if constexpr (N == std::dynamic_extent)
        retval.resize(q_in.quantities.size());

    std::ranges::transform(q_in, retval.begin(),
            [&](coord_qty<CC> const& q){return this->operator () (q);} );

    return retval;
}


//____________________________ COORDSYS_BACKEND __________________________

export
template <coordsys_class CC>
class coordsys_backend
{
public:
    using coordsys_class = CC;

    // DOCS: Inverse Transformation Function to World Reference Frame
    // DOCS: The name of this function is based on IEEE 1873-2015
    [[nodiscard]]
    virtual CC::tf_data_type offset() const = 0;

    [[nodiscard]]
    virtual std::shared_ptr<coordsys_backend<CC>> clone() const = 0;

    virtual ~coordsys_backend() = default;
};


template <typename backend>
concept derived_from_coordsys_backend =
       requires
       {
           typename backend::coordsys_class;
       }
    && std::derived_from< backend, coordsys_backend<typename backend::coordsys_class> >;


//____________________ COORDINATE SYSTEM____________________/\/\/\/\/\/\/\/\ __________________


export
template <coordsys_class CC>
class coordsys
{
public:
    using coordsys_class = CC;
    using quantity_type  = typename CC::quantity_type;
    using value_type     = coord_value<CC>;
    using tf_data_type   = typename CC::tf_data_type;
    using coord_tf_type  = coord_tf<CC>;

protected:
    using backend_type   = coordsys_backend<CC>;
    using backend_ptr    = std::shared_ptr< backend_type >;

    constexpr static std::nullptr_t const WCS = nullptr;
    backend_ptr backend = WCS;

public:
    coordsys();
    coordsys(WCS_t);
    coordsys(coordsys const& copy);
    coordsys(coordsys     && move);
    coordsys & operator = (coordsys const& copy);
    coordsys & operator = (coordsys     && move);
    // DOCS: Checks whether 'backend' points to the same object.
    // This is a pointer comparison of the underlying coordinate system backend.
    // Note that:
    //          coordsys A;
    //          coordsys B(A);
    //          assert(A != B); // True, except WCS whose copies are all equal.
    [[nodiscard]] bool operator == (coordsys const& other) const;
protected:
    explicit coordsys(backend_ptr const& interface);
    explicit coordsys(backend_ptr && interface);

public:
    [[nodiscard]] tf_data_type tf_to_WCS()   const;
    [[nodiscard]] tf_data_type offset()      const;
    [[nodiscard]] tf_data_type tf_from_WCS() const;

    [[nodiscard]] value_type origin() const;

    // DOCS: Creates a linked duplicate, used for comparisons e.g. in value.
    // Makes a coordsys with an internal non-cloning copy of the virtual interface.
    // This function is effectively the opposite of a deep copy.
    // (Copy with pointer semantics)
    [[nodiscard]] coordsys linked_copy() const;
};

//________________________________ COORDINATE SYSTEM - Definitions ________________________________


/*** Coordinate System - Regular traits (DOCS: Value semantics) ***/

template <coordsys_class CC>
coordsys<CC>:: coordsys()
    = default;

template <coordsys_class CC>
coordsys<CC>:: coordsys(WCS_t)
  : coordsys()
{ }

template <coordsys_class CC>
coordsys<CC>:: coordsys(coordsys const& copy)
  : backend { copy.backend ? copy.backend->clone() : WCS }
{ }

template <coordsys_class CC>
coordsys<CC>:: coordsys(coordsys && move)
    = default;

template <coordsys_class CC>
coordsys<CC> &
coordsys<CC>:: operator = (coordsys const& copy)
{
    backend = { copy.backend ? copy.backend->clone() : WCS };
    return *this;
}

template <coordsys_class CC>
coordsys<CC> &
coordsys<CC>:: operator = (coordsys && move)
    = default;

// shared_ptr<> compares pointer values.
// DOCS: Checks if two coordsys objects share the same backend object.
template <coordsys_class CC>
[[nodiscard]] bool
coordsys<CC>:: operator == (coordsys const& other) const
    = default;

/*** Coordinate System - Type erasure constructors ***/

template <coordsys_class CC>
coordsys<CC>:: coordsys(backend_ptr const& interface)
  : backend{ interface }
{ }

template <coordsys_class CC>
coordsys<CC>:: coordsys(backend_ptr && interface)
  : backend{ std::move(interface) }
{ }

/*** Coordinate System - NVI idiom - public interface ***/

template <coordsys_class CC>
[[nodiscard]] CC::tf_data_type
coordsys<CC>:: tf_to_WCS() const
{
    if (backend == WCS)
        return {};
    else
        return backend->offset();
}

template <coordsys_class CC>
[[nodiscard]] CC::tf_data_type
coordsys<CC>:: offset() const
{
    return tf_to_WCS();
}

template <coordsys_class CC>
[[nodiscard]] CC::tf_data_type
coordsys<CC>:: tf_from_WCS() const
{
    if (backend == WCS)
        return {};
    else
        return CC::invert_tf(backend->offset());
}

/*** Coordinate System - Other ***/

template <coordsys_class CC>
[[nodiscard]]  coord_value<CC>
coordsys<CC>:: origin() const
{
    return coord_value<CC>{ *this };
}


template <coordsys_class CC>
[[nodiscard]] coordsys<CC>
coordsys<CC>:: linked_copy() const
{
    return coordsys(backend);
}


//_______________________ MAP_CS _______________________

/*** Coordinate mapping between same-class coordinate systems ***/

export
template <coordsys_class CC> [[nodiscard]] coord_tf<CC> mapCS ( coordsys<CC> const& from,
                                                               coordsys<CC> const& to );
export
template <coordsys_class CC> [[nodiscard]] coord_tf<CC> mapCS ( coordsys<CC> const& from, WCS_t );
export
template <coordsys_class CC> [[nodiscard]] coord_tf<CC> mapCS ( WCS_t, coordsys<CC> const& to );


/*** Coordinate mapping between same-class coordinate systems - Definitions ***/

template <coordsys_class CC>
[[nodiscard]] coord_tf<CC>
mapCS (coordsys<CC> const& from, coordsys<CC> const& to)
{
    // TODO: Asynchronous calls to tf_from_WCS() and tf_to_WCS()
    //       offset() should accept non-blocking multi-threading.
    //       If two threads call offset() on the same object, the second one
    //       waits for the first to finish and the same value is returned
    //       to both threads.
    return coord_tf<CC>( from,
                        CC::compose_tf(to.tf_from_WCS(), from.tf_to_WCS()),
                        to );
}
template <coordsys_class CC>
[[nodiscard]] coord_tf<CC>
mapCS (coordsys<CC> const& from, WCS_t)
{
    return mapCS(from, coordsys<CC>{});
}
template <coordsys_class CC>
[[nodiscard]] coord_tf<CC>
mapCS (WCS_t, coordsys<CC> const& to)
{
    return mapCS(coordsys<CC>{}, to);
}


/*** Coordinate mapping between bridged coordinate systems ***/

export
template < coordsys_class F, coordsys_bridge_traits B, coordsys_class CC >
requires coordsys_in_bridge <F,B,CC> [[nodiscard]] coord_tf<F,CC,B>
mapCS ( coordsys<F> const& from, B const& bridge, coordsys<CC> const& to );

export
template < coordsys_class F, coordsys_bridge_traits B>
[[nodiscard]] coord_tf<F,coordsys_bridge_dual_traits_t<B,F>,B>
mapCS ( coordsys<F> const& from, B const& bridge, WCS_t );

export
template < coordsys_bridge_traits B, coordsys_class CC >
[[nodiscard]] coord_tf<coordsys_bridge_dual_traits_t<B,CC>,CC,B>
mapCS ( WCS_t, B const& bridge, coordsys<CC> const& to );

/*** Coordinate mapping between bridged coordinate systems - Definitions ***/

template < coordsys_class        F,
           coordsys_bridge_traits B,
           coordsys_class        CC >
requires coordsys_in_bridge <F,B,CC>
[[nodiscard]]
coord_tf<F,CC,B>
mapCS ( coordsys<F> const& from,
        B           const& bridge,
        coordsys<CC> const& to )
{
    return coord_tf <F,CC,B> {
        from,
        F::compose_tf(static_cast<coordsys<F> const&>(bridge).tf_from_WCS(), from.tf_to_WCS()),
        CC::compose_tf(to.tf_from_WCS(), static_cast<coordsys<CC> const&>(bridge).tf_to_WCS()),
        to
    };
}

template < coordsys_class F, coordsys_bridge_traits B>
[[nodiscard]] coord_tf<F,coordsys_bridge_dual_traits_t<B,F>,B>
mapCS ( coordsys<F> const& from, B const& bridge, WCS_t )
{
    return mapCS(from, bridge, coordsys<coordsys_bridge_dual_traits_t<B,F>> {});
}

template < coordsys_bridge_traits B, coordsys_class CC >
[[nodiscard]] coord_tf<coordsys_bridge_dual_traits_t<B,CC>,CC,B>
mapCS ( WCS_t, B const& bridge, coordsys<CC> const& to )
{
    return mapCS(coordsys<coordsys_bridge_dual_traits_t<B,CC>> {}, bridge, to);
}


//________________________________ COORDSYS_GENERIC ________________________________

export
template <derived_from_coordsys_backend BE>
class coordsys_generic final : public coordsys<typename BE::coordsys_class>
{
protected:
    using backend_type   = BE;
    using backend_ptr    = std::shared_ptr< backend_type >;
    using coordsys_class = BE::coordsys_class;
public:
    template <typename ... CC>
    coordsys_generic( CC && ... args )
      : coordsys<coordsys_class>( std::make_shared<backend_type>(std::forward<CC>(args) ...) )
    { }

    BE      * operator -> ();
    BE const* operator -> () const;
};

/*** Definitions ***/


template <derived_from_coordsys_backend BE>
BE *
coordsys_generic<BE>:: operator -> ()
{
    return dynamic_cast< BE * >(this->backend.get());
}

template <derived_from_coordsys_backend BE>
BE const*
coordsys_generic<BE>:: operator -> () const
{
    return dynamic_cast< BE const* >(this->backend.get());
}

//________________________ COORDSYS TREENODE - BACKEND ____________________

export
template <coordsys_class CC>
class coordsys_tree_backend : public coordsys_backend<CC>
{
protected:
    CC::tf_data_type   tf_to_parent_data;
    coordsys<CC> const parent_data;

public:
    coordsys_tree_backend();
    coordsys_tree_backend(coordsys_tree_backend const& copy) = delete;
    explicit coordsys_tree_backend( coordsys<CC>     const& ParentCS_arg,
                                    CC::tf_data_type const& tf_to_parent );

    [[nodiscard]] CC::tf_data_type offset() const override;
    [[nodiscard]] std::shared_ptr<coordsys_backend<CC>> clone() const override;

    CC::tf_data_type      & tf_to_parent();
    CC::tf_data_type const& tf_to_parent() const;
    coordsys<CC>     const& parent()       const;
};

template <typename backend>
concept derived_from_coordsys_tree_backend =
       derived_from_coordsys_backend< backend >
    && std::derived_from< backend, coordsys_tree_backend<typename backend::coordsys_class> >;


/*** Definitions - Constructors ***/

template <coordsys_class CC>
coordsys_tree_backend<CC>:: coordsys_tree_backend()
    = default;

template <coordsys_class CC>
coordsys_tree_backend<CC>:: coordsys_tree_backend( coordsys<CC>     const& ParentCS_arg,
                                                   CC::tf_data_type const& tf_to_parent )
  : tf_to_parent_data{ tf_to_parent }
  , parent_data{ ParentCS_arg.linked_copy() }
{ }

/*** Definitions - Overrides ***/

template <coordsys_class CC>
[[nodiscard]]
CC::tf_data_type
coordsys_tree_backend<CC>:: offset() const
{
    return CC::compose_tf( parent_data.tf_to_WCS(), tf_to_parent_data );
}

template <coordsys_class CC>
[[nodiscard]] std::shared_ptr<coordsys_backend<CC>>
coordsys_tree_backend<CC>:: clone() const
{
    return std::make_shared<coordsys_tree_backend>(parent_data, tf_to_parent_data);
}

/*** Definitions - projections ***/

template <coordsys_class CC>
CC::tf_data_type &
coordsys_tree_backend<CC>:: tf_to_parent()
{
    return tf_to_parent_data;
}

template <coordsys_class CC>
CC::tf_data_type const&
coordsys_tree_backend<CC>:: tf_to_parent() const
{
    return tf_to_parent_data;
}

template <coordsys_class CC>
coordsys<CC> const&
coordsys_tree_backend<CC>:: parent() const
{
    return parent_data;
}





//________________________ COORDSYS TREENODE ____________________
// DOCS: The invariant is that the backend always contains a shared_ptr<coordsys_tree_backend>

// DOCS: Child node in kinematic tree
// DOCS: Base class for writing coordinate systems that depend on a parent.
//       Derive to add custom constructors.

export
template <derived_from_coordsys_tree_backend BE>
class coordsys_treenode : public coordsys<typename BE::coordsys_class>
{
    using CC = BE::coordsys_class;
protected:
    using backend_type = BE;
    using backend_ptr  = std::shared_ptr< backend_type >;

public:
    coordsys_treenode();
    coordsys_treenode(coordsys_treenode const& copy);
    coordsys_treenode(coordsys_treenode     && move);
    coordsys_treenode & operator = (coordsys_treenode const& copy);
    coordsys_treenode & operator = (coordsys_treenode     && move);

    coordsys_treenode( CC::tf_data_type const& tf_data_to_parent, coordsys<CC> const& parent_CS );
    coordsys_treenode( CC::tf_data_type const& tf_data_to_parent, WCS_t                         );

    BE      * operator -> ();
    BE const* operator -> () const;
};


/*** Definitions - Semiregular traits (DOCS: value semantics) ***/
template <derived_from_coordsys_tree_backend BE>
coordsys_treenode<BE>:: coordsys_treenode()
  : coordsys<CC>{ std::make_shared<coordsys_tree_backend<CC>>() }
{ }

template <derived_from_coordsys_tree_backend BE>
coordsys_treenode<BE>:: coordsys_treenode(coordsys_treenode const& copy)
  : coordsys<CC>{ copy }
{ }

template <derived_from_coordsys_tree_backend BE>
coordsys_treenode<BE>:: coordsys_treenode(coordsys_treenode && move)
    = default;

template <derived_from_coordsys_tree_backend BE>
coordsys_treenode<BE> &
coordsys_treenode<BE>:: operator = (coordsys_treenode const& copy)
{
    coordsys<CC>::operator = (*this, copy);
    return *this;
}

template <derived_from_coordsys_tree_backend BE>
coordsys_treenode<BE> &
coordsys_treenode<BE>:: operator = (coordsys_treenode && move)
    = default;

/*** Definitions - Constructors from transformation data ***/
template <derived_from_coordsys_tree_backend BE>
coordsys_treenode<BE>:: coordsys_treenode( CC::tf_data_type const& tf_data_to_parent,
                                           coordsys<CC>     const& parent_CS )
  : coordsys<CC>{ std::make_shared<coordsys_tree_backend<CC>> (parent_CS, tf_data_to_parent) }
{ }

template <derived_from_coordsys_tree_backend BE>
coordsys_treenode<BE>:: coordsys_treenode( CC::tf_data_type const& tf_data_to_parent, WCS_t )
  : coordsys_treenode{ tf_data_to_parent, coordsys<CC>{} }
{ }

/*** Definitions - Accessor to underlying backend ***/

template <derived_from_coordsys_tree_backend BE>
BE *
coordsys_treenode<BE>:: operator -> ()
{
    return dynamic_cast< BE * >(this->backend.get());
}

template <derived_from_coordsys_tree_backend BE>
BE const*
coordsys_treenode<BE>:: operator -> () const
{
    return dynamic_cast< BE const* >(this->backend.get());
}

//____________________________ DATA AGGREGATES - VALUE - DEFINITIONS ____________________________

/*** Definitions - Regular constructors ***/
template <coordsys_class CC>
coord_value<CC>:: coord_value()
    = default;

template <coordsys_class CC>
coord_value<CC>:: coord_value(coord_value const& copy)
  : coord_qty<CC> {copy}
  , inCS_data {copy.inCS().linked_copy()}
{ }

template <coordsys_class CC>
coord_value<CC>:: coord_value(coord_value && move)
  : coord_qty<CC> {std::move(move.qty())}
  , inCS_data {std::move(move.inCS_data)}
{ }

/*** Definitions - Assignment operator ***/
template <coordsys_class CC>
coord_value<CC> &
coord_value<CC>:: operator = (coord_value const& copy)
{
    inCS_data = copy.inCS().linked_copy();
    this->qty() = copy.qty();
    return *this;
}
template <coordsys_class CC>
coord_value<CC> &
coord_value<CC>:: operator = (coord_value && move)
{
    this->qty() = std::move(move.qty());
    inCS_data = std::move(move.inCS_data);
    return *this;
}

/*** Definitions - Aggregate-like constructors ***/

template <coordsys_class CC>
coord_value<CC>:: coord_value(coord_qty<CC> const& qty_arg)
  : coord_value (coordsys<CC>{}, qty_arg)
{ }

template <coordsys_class CC>
coord_value<CC>:: coord_value(coordsys<CC> const& in_coordsys, coord_qty<CC> const& qty_arg)
  : coord_qty<CC> {qty_arg}
  , inCS_data {in_coordsys.linked_copy()}
{ }

template <coordsys_class CC>
coord_value<CC>:: coord_value(WCS_t, coord_qty<CC> const& qty_arg)
  : coord_value (coordsys<CC>{}, qty_arg)
{ }

template <coordsys_class CC>
coord_value<CC>:: coord_value(coordsys<CC> const& in_coordsys, coord_value<CC> const& value)
  : coord_value(in_coordsys, value.map_to(in_coordsys).qty())
{ }

template <coordsys_class CC>
coord_value<CC>:: coord_value(WCS_t, coord_value<CC> const& value)
  : coord_value (coordsys<CC>{}, value)
{ }

/*** Definitions - Member functions ***/
template <coordsys_class CC>
coordsys<CC> const&
coord_value<CC>:: inCS() const
{
    return inCS_data;
}

template <coordsys_class CC>
coord_value<CC>
coord_value<CC>:: map_to(coordsys<CC> const& to_coordsys) const
{
    // TODO: There is no need to create a transform function, which makes 2 copies of shared_ptr.
    //       I can just call the appropriate function in coordsys_class<CC>.
    coord_tf tf = mapCS(inCS(), to_coordsys);
    coord_qty<CC> transformed_qty = tf( this->qty() );
    return {to_coordsys, transformed_qty};
}

template <coordsys_class CC>
coord_value<CC>
coord_value<CC>:: map_to(WCS_t) const
{
    return map_to(coordsys<CC>{});
}

template <coordsys_class CC>
coord_qty<CC> const&
coord_value<CC>:: qty() const
{
    return *this;
}

template <coordsys_class CC>
coord_qty<CC> &
coord_value<CC>:: qty()
{
    return *this;
}

//____________________________ DATA AGGREGATES - CLOUD - DEFINITIONS ____________________________

/*** Definitions - Regular constructors ***/
template <coordsys_class CC, size_t N>
coord_cloud<CC,N>:: coord_cloud()
    = default;

template <coordsys_class CC, size_t N>
coord_cloud<CC,N>:: coord_cloud(coord_cloud const& copy)
  : container_type {copy}
  , inCS_data {copy.inCS().linked_copy()}
{ }

template <coordsys_class CC, size_t N>
coord_cloud<CC,N>:: coord_cloud(coord_cloud && move)
  : container_type {std::move(move.qty())}
  , inCS_data {std::move(move.inCS_data)}
{ }

/*** Definitions - Assignment operator ***/
template <coordsys_class CC, size_t N>
coord_cloud<CC,N> &
coord_cloud<CC,N>:: operator = (coord_cloud const& copy)
{
    this->qty() = copy;
    inCS_data = copy.inCS().linked_copy();
    return *this;
}
template <coordsys_class CC, size_t N>
coord_cloud<CC,N> &
coord_cloud<CC,N>:: operator = (coord_cloud && move)
{
    this->qty() = std::move(move.qty());
    inCS_data = std::move(move.inCS_data);
    return *this;
}

/*** Definitions - Aggregate-like constructors ***/
template <coordsys_class CC, size_t N>
coord_cloud<CC,N>:: coord_cloud(coordsys<CC> const& in_coordsys, container_type const& qties)
  : container_type {qties}
  , inCS_data {in_coordsys.linked_copy()}
{}

template <coordsys_class CC, size_t N>
coord_cloud<CC,N>:: coord_cloud(WCS_t, container_type const& qties)
  : coord_cloud(coordsys<CC>{}, qties)
{}

template <coordsys_class CC, size_t N>
coord_cloud<CC,N>:: coord_cloud(coordsys<CC> const& in_coordsys,
                               std::initializer_list<coord_qty<CC>> qties)
  : container_type {qties}
  , inCS_data {in_coordsys.linked_copy()}
{}

template <coordsys_class CC, size_t N>
coord_cloud<CC,N>:: coord_cloud(WCS_t, std::initializer_list<coord_qty<CC>> qties)
  : coord_cloud(coordsys<CC>{}, qties)
{}

/*** Definitions - Member functions ***/
template <coordsys_class CC, size_t N>
coordsys<CC> const&
coord_cloud<CC,N>:: inCS() const
{
    return inCS_data;
}

template <coordsys_class CC, size_t N>
coord_cloud<CC,N>
coord_cloud<CC,N>:: map_to(coordsys<CC> const& to_coordsys) const
{
    return mapCS(inCS(), to_coordsys)(*this);
}

template <coordsys_class CC, size_t N>
coord_cloud<CC,N>
coord_cloud<CC,N>:: map_to(WCS_t) const
{
    return map_to(coordsys<CC>{});
}

template <coordsys_class CC, size_t N>
void
coord_cloud<CC,N>:: fetch(coord_cloud const& cloud)
{
    *this = static_cast<container_type>( cloud.map_to(inCS()) );
}

//_______________________ FOOTER _______________________


} // namespace nin


