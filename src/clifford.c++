/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//______________________________ --headers-- ______________________________
module;

#include <algorithm>
#include <array>
#include <cmath>
#include <complex>
#include <concepts>
#include <span>
#include <stdexcept>

export module ninbot.pos:clifford;

import :euclidean;


namespace nin {

//_________________________ Utilities __________________________

/*** approx_equal() for floating types ***/
export
template <std::floating_point T>
constexpr
bool
approx_equal(T lhs, T rhs, unsigned ulp = 8)
{
    T const max_separation = std::max(std::abs(lhs), std::abs(rhs))
                           * std::numeric_limits<T>::epsilon() * ulp;
    T const diffnorm = std::abs(lhs - rhs);

    return diffnorm <= max_separation
        || diffnorm <  std::numeric_limits<T>::min()
        || (  std::abs(lhs) < std::numeric_limits<T>::epsilon() * ulp // Workaround if values are close to 0
           && std::abs(rhs) < std::numeric_limits<T>::epsilon() * ulp );
}

/*** Definition of Euler angles ***/
export
enum struct euler_order
{
    XYX,
    YXY,
    XZX,
    ZXZ,
    YZY,
    ZYZ,
    XYZ,
    XZY,
    YXZ,
    YZX,
    ZXY,
    ZYX,
    /* DIN 9300, ISO 1151-2:1985
        *      Roll  = X: facing forwards
        *      Pitch = Y: right wing
        *      Yaw   = Z: looking downwards */
    RollPitchYaw
};

/*** matrix_ordering ***/
export
enum matrix_ordering
{
    COLUMN_MAJOR,
    ROW_MAJOR,
};


/*** norm of initializer list ***/
//namespace {

template <std::floating_point F>
constexpr
F
squared_norm (std::initializer_list<F> components) noexcept
{
    F result = 0;

    for (auto const component : components)
        result = result + component*component;

    return result;
}

template <std::floating_point F>
constexpr
F
norm (std::initializer_list<F> components) noexcept
{
    return std::sqrt( squared_norm(components) );
}

//} // Anonymous namespace

//_______________________ Quaternion ________________________

/*** Forward declarations ***/
export template <std::floating_point T> class basic_quaternion;

export template <std::floating_point T> constexpr T                  norm_sq (basic_quaternion<T> const& Q) noexcept;
export template <std::floating_point T> constexpr basic_quaternion<T>   conj (basic_quaternion<T> const& Q) noexcept;
export template <std::floating_point T> constexpr T &                   real (basic_quaternion<T>      & Q) noexcept;
export template <std::floating_point T> constexpr T                     real (basic_quaternion<T> const& Q) noexcept;
export template <std::floating_point T> constexpr std::span<T, 3>       imag (basic_quaternion<T>      & Q) noexcept;
export template <std::floating_point T> constexpr std::span<const T, 3> imag (basic_quaternion<T> const& Q) noexcept;

/*** basic_quaternion ***/
export
template <std::floating_point T>
class basic_quaternion
{
    // DOCS: basic_quaternion := q0 + q1*i + q2*j + q3*k
    std::array<T, 4> raw = {};

public:
    using value_type = T;

    constexpr basic_quaternion() noexcept;
    constexpr basic_quaternion(basic_quaternion const& copy) noexcept;
    constexpr basic_quaternion & operator = (basic_quaternion const& copy) noexcept;

    constexpr basic_quaternion(T w, T x, T y, T z) noexcept;

    constexpr void conjugate () noexcept;
    constexpr void normalize ();
    constexpr void invert ();

    constexpr friend T                norm_sq<> (basic_quaternion const& Q) noexcept;
    constexpr friend basic_quaternion conj<>    (basic_quaternion const& Q) noexcept;

    constexpr basic_quaternion operator + () const noexcept;
    constexpr basic_quaternion operator - () const noexcept;

    constexpr basic_quaternion & operator += (basic_quaternion const& rhs) noexcept;
    constexpr basic_quaternion & operator -= (basic_quaternion const& rhs) noexcept;
    constexpr basic_quaternion & operator *= (basic_quaternion const& rhs) noexcept;
    constexpr basic_quaternion & operator /= (basic_quaternion const& rhs);
    template <arithmetic A> constexpr basic_quaternion & operator *= (A scalar) noexcept;
    template <arithmetic A> constexpr basic_quaternion & operator /= (A scalar);

    template <std::floating_point U> constexpr operator basic_quaternion<U> () const noexcept;

    constexpr friend T &                   real<> (basic_quaternion      & Q) noexcept;
    constexpr friend T                     real<> (basic_quaternion const& Q) noexcept;
    constexpr friend std::span<T, 3>       imag<> (basic_quaternion      & Q) noexcept;
    constexpr friend std::span<const T, 3> imag<> (basic_quaternion const& Q) noexcept;

    constexpr T w() const noexcept { return raw[0]; }
    constexpr T x() const noexcept { return raw[1]; }
    constexpr T y() const noexcept { return raw[2]; }
    constexpr T z() const noexcept { return raw[3]; }

    constexpr T & w() noexcept { return raw[0]; }
    constexpr T & x() noexcept { return raw[1]; }
    constexpr T & y() noexcept { return raw[2]; }
    constexpr T & z() noexcept { return raw[3]; }

    constexpr T      * data()       noexcept { return raw.data(); }
    constexpr T const* data() const noexcept { return raw.data(); }

};

export
template <std::floating_point T> constexpr T norm(basic_quaternion<T> const& Q) noexcept;
export
template <std::floating_point T> constexpr basic_quaternion<T> inv(basic_quaternion<T> Q);

#define NIN_CLIF_NMDEF template  <std::floating_point T> constexpr basic_quaternion<T>
#define NIN_CLIF_NMDEFA template <std::floating_point T, arithmetic A> constexpr basic_quaternion<T>

export NIN_CLIF_NMDEF  operator + (basic_quaternion<T> lhs, basic_quaternion<T> const& rhs) noexcept;
export NIN_CLIF_NMDEF  operator - (basic_quaternion<T> lhs, basic_quaternion<T> const& rhs) noexcept;
export NIN_CLIF_NMDEFA operator * (A scalar, basic_quaternion<T> rhs) noexcept;
export NIN_CLIF_NMDEFA operator * (basic_quaternion<T> lhs, A scalar) noexcept;
export NIN_CLIF_NMDEFA operator / (basic_quaternion<T> lhs, A scalar);
export NIN_CLIF_NMDEF  operator * (basic_quaternion<T> lhs, basic_quaternion<T> const& rhs) noexcept;
export NIN_CLIF_NMDEF  operator / (basic_quaternion<T> lhs, basic_quaternion<T> const& rhs);

#undef NIN_CLIF_NMDEF
#undef NIN_CLIF_NMDEFA


export
using quaternion = basic_quaternion<double>;


//________________________________ Quaternion - definitions ________________________________


/*** Semiregular traits ***/

template <std::floating_point T>
constexpr
basic_quaternion<T>:: basic_quaternion () noexcept
    = default;

template <std::floating_point T>
constexpr
basic_quaternion<T>:: basic_quaternion (basic_quaternion const& copy) noexcept
    = default;

template <std::floating_point T>
constexpr
basic_quaternion<T> &
basic_quaternion<T>:: operator = (basic_quaternion const& copy) noexcept
= default;

template <std::floating_point T>
constexpr
basic_quaternion<T>:: basic_quaternion (T w, T x, T y, T z) noexcept
: raw {w, x, y, z}
{ }


/*** Comparisons ***/

// For use when comparing float types with std::numeric_limits<>::epsilon()
// Units in the Last Place

export
template <std::floating_point T>
constexpr
bool
approx_equal (basic_quaternion<T> const& lhs,
              basic_quaternion<T> const& rhs,
              unsigned ulp = 8 ) noexcept
{
    T const max_separation = norm(lhs + rhs) * std::numeric_limits<T>::epsilon() * ulp;
    T const diffnorm = norm(lhs - rhs);

    return diffnorm <= max_separation
        || diffnorm <  std::numeric_limits<T>::min();
}

/*** Math functions: members ***/

template <std::floating_point T>
constexpr
void
basic_quaternion<T>:: conjugate () noexcept
{
    *this = conj(*this);
}

template <std::floating_point T>
constexpr
void
basic_quaternion<T>:: normalize ()
{
    T norm_squared = norm_sq(*this);

    if ( !std::isnormal(norm_squared) ) [[unlikely]]
        throw std::domain_error("The squared norm is "
                "infinite, NaN or subnormal (almost zero)");
    T norm_value = std::sqrt(norm_squared);

    for (T & datum : raw)
        datum /= norm_value;
}

template <std::floating_point T>
constexpr
void
basic_quaternion<T>:: invert ()
{
    T norm_2 = norm_sq(*this);

    if ( !std::isnormal(norm_2) ) [[unlikely]]
        throw std::domain_error("The norm is infinite, NaN or subnormal (almost zero)");

    conjugate();
    *this /= norm_2;
}

/*** Math functions: friends ***/

template <std::floating_point T>
constexpr
T
norm_sq (basic_quaternion<T> const& Q) noexcept
{
    auto const& d = Q.raw;
    return squared_norm({ d[0], d[1], d[2], d[3] });
}

template <std::floating_point T>
constexpr
basic_quaternion<T>
conj (basic_quaternion<T> const& Q) noexcept
{
    return { Q.raw[0], -Q.raw[1], -Q.raw[2], -Q.raw[3] };
}

/*** Unary operators ***/

template <std::floating_point T>
constexpr
basic_quaternion<T>
basic_quaternion<T>:: operator + () const noexcept
{
    return *this;
}

template <std::floating_point T>
constexpr
basic_quaternion<T>
basic_quaternion<T>:: operator - () const noexcept
{
    return { -raw[0], -raw[1], -raw[2], -raw[3] };
}

/*** Binary operators ***/

template <std::floating_point T>
constexpr
basic_quaternion<T> &
basic_quaternion<T>:: operator += (basic_quaternion const& rhs) noexcept
{
    for (int i = 0; i < 4; i++)
        raw[i] += rhs.raw[i];
    return *this;
}

template <std::floating_point T>
constexpr
basic_quaternion<T> &
basic_quaternion<T>:: operator -= (basic_quaternion const& rhs) noexcept
{
    for (int i = 0; i < 4; i++)
        raw[i] -= rhs.raw[i];
    return *this;
}

template <std::floating_point T>
constexpr
basic_quaternion<T> &
basic_quaternion<T>:: operator *= (basic_quaternion const& rhs) noexcept
{
    using R = T const&;
    R a1 = raw[0];  R a2 = rhs.raw[0];
    R b1 = raw[1];  R b2 = rhs.raw[1];
    R c1 = raw[2];  R c2 = rhs.raw[2];
    R d1 = raw[3];  R d2 = rhs.raw[3];

    *this = { a1*a2 - b1*b2 - c1*c2 - d1*d2 ,
              a1*b2 + a2*b1 + c1*d2 - c2*d1 ,
              a1*c2 + a2*c1 + b2*d1 - b1*d2 ,
              a1*d2 + a2*d1 + b1*c2 - c1*b2 };

    return *this;
}

template <std::floating_point T>
constexpr
basic_quaternion<T> &
basic_quaternion<T>:: operator /= (basic_quaternion const& rhs)
{
    basic_quaternion rhs_inv = rhs;
    rhs_inv.invert();
    return *this *= rhs_inv;
}

template <std::floating_point T>
template <arithmetic A>
constexpr
basic_quaternion<T> &
basic_quaternion<T>:: operator *= (A scalar) noexcept
{
    for (T & datum : raw)
        datum *= static_cast<T>(scalar);
    return *this;
}

template <std::floating_point T>
template <arithmetic A>
constexpr
basic_quaternion<T> &
basic_quaternion<T>:: operator /= (A scalar)
{
    if ( scalar <= std::numeric_limits<A>::min() ) [[unlikely]]
        throw std::domain_error("Divide by zero");

    for (T & datum : raw)
        datum /= static_cast<T>(scalar);
    return *this;
}

/*** Conversions ***/
template <std::floating_point T>
template <std::floating_point U>
constexpr
basic_quaternion<T>:: operator basic_quaternion<U> () const noexcept
{
    return { static_cast<U>(raw[0]), static_cast<U>(raw[1]) ,
             static_cast<U>(raw[2]), static_cast<U>(raw[3]) };
}

/*** Projections ***/

template <std::floating_point T>
constexpr
T &
real (basic_quaternion<T> & Q) noexcept
{
    return Q.raw[0];
}

template <std::floating_point T>
constexpr
T
real (basic_quaternion<T> const& Q) noexcept
{
    return Q.raw[0];
}

template <std::floating_point T>
constexpr
std::span<T, 3>
imag (basic_quaternion<T> & Q) noexcept
{
    return std::span<T, 3>(&Q.raw[1], 3);
}

template <std::floating_point T>
constexpr
std::span<const T, 3>
imag (basic_quaternion<T> const& Q) noexcept
{
    return std::span<const T, 3>(&Q.raw[1], 3);
}

/*** Non-member unary operators ***/
template <std::floating_point T>
constexpr
T
norm(basic_quaternion<T> const& Q) noexcept
{
    return norm( {Q.w(), Q.x(), Q.y(), Q.z()} );
}

template <std::floating_point T>
constexpr
basic_quaternion<T>
inv(basic_quaternion<T> Q)
{
    Q.invert();
    return Q;
}


/*** Non-member binary operators ***/

template <std::floating_point T>
constexpr
basic_quaternion<T>
operator + (basic_quaternion<T> lhs, basic_quaternion<T> const& rhs) noexcept
{
    return lhs += rhs;
}

template <std::floating_point T>
constexpr
basic_quaternion<T>
operator - (basic_quaternion<T> lhs, basic_quaternion<T> const& rhs) noexcept
{
    return lhs -= rhs;
}

template <std::floating_point T, arithmetic A>
constexpr
basic_quaternion<T>
operator * (A scalar, basic_quaternion<T> rhs) noexcept
{
    return rhs *= static_cast<T>(scalar);
}

template <std::floating_point T, arithmetic A>
constexpr
basic_quaternion<T>
operator * (basic_quaternion<T> lhs, A scalar) noexcept
{
    return lhs *= static_cast<T>(scalar);
}

template <std::floating_point T, arithmetic A>
constexpr
basic_quaternion<T>
operator / (basic_quaternion<T> lhs, A scalar)
{
    return lhs /= static_cast<T>(scalar);
}

template <std::floating_point T>
constexpr
basic_quaternion<T>
operator * (basic_quaternion<T> lhs, basic_quaternion<T> const& rhs) noexcept
{
    return lhs *= rhs;
}

template <std::floating_point T>
constexpr
basic_quaternion<T>
operator / (basic_quaternion<T> lhs, basic_quaternion<T> const& rhs)
{
    return lhs /= rhs;
}



//_______________________ Orientation quantity ________________________

/** ORIENTATION - ROTATION
* DOCS: Orientations are defined with respect to a reference coordinate system.
* They are not transformations, so operations such as 'inverse' do not make sense, strictly
* speaking.
* Rotations are very similar to orientations, but the set of operations are different.
* With respect to data, the difference between orientations and rotations is that
* rotations can span several turns, whereas orientations do not consider the number
* of turns performed to reach that orientation.
* Orientations can be interpolated (preferably using slerp). Rotations cannot be
* interpolated, but partial transforms are possible.
* Orientations cannot be composed. Rotations can be composed, but the composition
* only makes sense when transforming orientations.
*/

// DOCS: The invariant of orientation is unitary norm of quaternion,
// but we do not need to enforce it because it is compensated when
// obtaining angle by atan() and axis by normalization.
// So, as long as norm(rawQ) > 0, we are good.
export
template <std::floating_point T>
class basic_orientation_quantity
{
    using quaternion = basic_quaternion<T>;
    quaternion rawQ = quaternion{ 1, 0, 0, 0 };

public:
    using value_type = T;

    constexpr basic_orientation_quantity () noexcept;

    constexpr basic_orientation_quantity (basic_orientation_quantity const& copy) noexcept;
    constexpr basic_orientation_quantity & operator =
                                                (basic_orientation_quantity const& copy) noexcept;
    constexpr basic_orientation_quantity (quaternion const& quat);

    constexpr basic_orientation_quantity( T const* matrix, matrix_ordering order) noexcept;
    constexpr basic_orientation_quantity( std::array<T, 9> const& matrix,
                                          matrix_ordering order ) noexcept;

    constexpr basic_orientation_quantity( T const* axisX, T const* axisY );
    constexpr basic_orientation_quantity( T const* axisX, nin::vector<T, 3> const& axisY );
    constexpr basic_orientation_quantity( nin::vector<T, 3> const& axisX, T const* axisY );
    constexpr basic_orientation_quantity( nin::vector<T, 3> const& axisX,
                                          nin::vector<T, 3> const& axisY );

    constexpr basic_orientation_quantity( T const* axis, units::angle_T<T> angle);
    constexpr basic_orientation_quantity( nin::vector<T, 3> const& axis, units::angle_T<T> angle);

    constexpr basic_orientation_quantity( units::angle_T<T> first,
                                          units::angle_T<T> second,
                                          units::angle_T<T> third,
                                          euler_order       order ) noexcept;

    template <std::floating_point U> requires std::convertible_to<T, U>
    constexpr operator basic_orientation_quantity<U> () const noexcept;

    constexpr quaternion const& to_quaternion () const noexcept;

    constexpr std::array<T, 9> to_matrix ( matrix_ordering order ) const noexcept;

    struct axis_angle { nin::vector<T,3> axis; units::angle_T<T> angle; };
    constexpr axis_angle to_axis_angle () const;

private:
    static constexpr basic_quaternion<T> convert_from_matrix( T const* matrix,
                                                              matrix_ordering order ) noexcept;
    static constexpr basic_quaternion<T> convert_from_XYaxis( T Xx, T Xy, T Xz,
                                                              T Yx, T Yy, T Yz );
    static constexpr basic_quaternion<T> convert_from_axisangle (T X, T Y, T Z, T angle);
    static constexpr basic_quaternion<T> convert_from_euler_angles(T first,
                                                                   T second,
                                                                   T third,
                                                                   euler_order euler) noexcept;

    static constexpr std::array<T, 9> convert_to_matrix ( basic_orientation_quantity const& R,
                                                          matrix_ordering order ) noexcept;
    static constexpr std::array<T, 4> convert_to_axisangle ( basic_orientation_quantity const& R );
};

export
template <std::floating_point T>
constexpr bool approx_equal( basic_orientation_quantity<T> const& r1,
                             basic_orientation_quantity<T> const& r2,
                             unsigned ulp = 8 );

export
enum struct slerp_solution { SHORTEST, REFLEX }; // DOCS: Short way or long way

export
template <std::floating_point T>
constexpr basic_orientation_quantity<T> slerp( basic_orientation_quantity<T> const& a,
                                               basic_orientation_quantity<T> const& b,
                                               T t,
                                               slerp_solution length = slerp_solution::SHORTEST );


export
using orientation_quantity = basic_orientation_quantity<double>;


//_______________________ Orientation quantity - definitions________________________

/*** Constructors ***/
template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity () noexcept
    = default;

template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity (basic_orientation_quantity const& copy)
                                                                                           noexcept
    = default;

template <std::floating_point T>
constexpr
basic_orientation_quantity<T> &
basic_orientation_quantity<T>:: operator = (basic_orientation_quantity const& copy) noexcept
    = default;

/*** Converting constructors ***/

// Constructor from quaternion
// Copy assigment operator results in embiguity errors when quat = {}, thus disabled.
template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity (quaternion const& quat)
  : rawQ { quat }
{
    if ( !std::isnormal(norm_sq(quat)) ) [[unlikely]]
        throw std::domain_error("The squared norm of the quaternion is "
                "infinite, NaN or subnormal (almost zero)");
}

// Constructor from orientation matrix. DOCS: P must contain 9 elements

template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( T const* matrix,
                                                            matrix_ordering order) noexcept
  : rawQ { convert_from_matrix(matrix, order) }
{ }

template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( std::array<T,9> const& matrix,
                                                            matrix_ordering order ) noexcept
  : basic_orientation_quantity(matrix.data(), order)
{ }

// Constructor from XY-axis
template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( T const* axisX, T const* axisY )
  : rawQ { convert_from_XYaxis( axisX[0], axisX[1], axisX[2],
                                axisY[0], axisY[1], axisY[2] ) }
{ }

template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( T const* axisX,
                                                            nin::vector<T, 3> const& axisY )
  : basic_orientation_quantity(axisX, axisY.data())
{ }

template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( nin::vector<T, 3> const& axisX,
                                                            T const* axisY )
  : basic_orientation_quantity(axisX.data(), axisY)
{ }

template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( nin::vector<T, 3> const& axisX,
                                                            nin::vector<T, 3> const& axisY )
  : basic_orientation_quantity(axisX.data(), axisY.data())
{ }

// Constructor from axis-angle
template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( T const* axis,
                                                            units::angle_T<T> angle)
  : rawQ { convert_from_axisangle(axis[0], axis[1], axis[2], angle.SI()) }
{ }

template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( nin::vector<T, 3> const& axis,
                                                            units::angle_T<T> angle)
  : basic_orientation_quantity(axis.data(), angle)
{ }

// Constructor from Euler angles
template <std::floating_point T>
constexpr
basic_orientation_quantity<T>:: basic_orientation_quantity( units::angle_T<T> first,
                                                            units::angle_T<T> second,
                                                            units::angle_T<T> third,
                                                            euler_order       order ) noexcept
  : rawQ { convert_from_euler_angles(first.SI(), second.SI(), third.SI(), order) }
{ }

/*** basic_orientation_quantity - Conversions ***/

// basic_orientation_quantity - implicit conversion to template siblings
template <std::floating_point T>
template <std::floating_point U> requires std::convertible_to<T, U>
constexpr
basic_orientation_quantity<T>:: operator basic_orientation_quantity<U> () const noexcept
{
    return { static_cast<basic_quaternion<U>>(rawQ) };
}

// quaternion - Explicit conversion to non-normalized quaternion
template <std::floating_point T>
constexpr
basic_quaternion<T> const&
basic_orientation_quantity<T>:: to_quaternion () const noexcept
{
    return rawQ;
}

// quaternion - Explicit conversion to matrix
template <std::floating_point T>
constexpr
std::array<T, 9>
basic_orientation_quantity<T>:: to_matrix (matrix_ordering order) const noexcept
{
    return convert_to_matrix( *this, order );
}

// quaternion - Explicit conversion to axis-angle (x,y,z, θ)
template <std::floating_point T>
constexpr
basic_orientation_quantity<T>::axis_angle
basic_orientation_quantity<T>:: to_axis_angle () const
{
    std::array<T, 4> result = convert_to_axisangle( *this );
    return { {result[0], result[1], result[2]}, units::radians(result[3]) };
}

/*** basic_orientation_quantity - Import - convert_from_matrix ***/
// DOCS: This is a low level function.
// Please use other conversion functions instead, if possible.
// The matrix must be an orientation matrix: det(M)=1 and orthogonal vectors
// Argument 'matrix' must hold exactly 9 values with the matrix components.
// No checks are made to the matrix to verify the requisites.
template <std::floating_point T>
constexpr
basic_quaternion<T>
basic_orientation_quantity<T>:: convert_from_matrix ( T const* matrix,
                                                      matrix_ordering order ) noexcept
{
    using E = T const&;

    E m11 = matrix[0]; E m12 = matrix[1]; E m13 = matrix[2];
    E m21 = matrix[3]; E m22 = matrix[4]; E m23 = matrix[5];
    E m31 = matrix[6]; E m32 = matrix[7]; E m33 = matrix[8];

    T a =  m11 + m22 + m33;
    T b =  m11 - m22 - m33;
    T c = -m11 + m22 - m33;
    T d = -m11 - m22 + m33;

    if (a >= b && a >= c && a >= d)
    {
            a = std::sqrt(1 + a) / 2;
            T inv_a = 1. / (4 * a);
            b = (m32 - m23) * inv_a;
            c = (m13 - m31) * inv_a;
            d = (m21 - m12) * inv_a;
    }
    else if (b >= c && b >= d)
    {
            b = std::sqrt(1 + b) / 2;
            T inv_b = 1./ (4 * b);
            a = (m32 - m23) * inv_b;
            c = (m12 + m21) * inv_b;
            d = (m13 + m31) * inv_b;
    }
    else if (c >= d)
    {
            c = std::sqrt(1 + c) / 2;
            T inv_c = 1./ (4 * c);
            a = (m13 - m31) * inv_c;
            b = (m12 + m21) * inv_c;
            d = (m23 + m32) * inv_c;
    }
    else
    {
            d = std::sqrt(1 + d) / 2;
            T inv_d = 1./ (4 * d);
            a = (m21 - m12) * inv_d;
            b = (m13 + m31) * inv_d;
            c = (m23 + m32) * inv_d;
    }

    basic_quaternion<T> result {a, b, c, d};
    return (order == matrix_ordering::ROW_MAJOR) ? result : conj(result);
}

/*** basic_orientation_quantity - Import - convert_from_XYaxis ***/
// DOCS: Convenient human-friendly function to define orientations.
// For efficiency, convert from quaternions or matrices instead.
// Requirements on vectors X and Y are loose: They are normalized
// and orthogonalized before calling convert_from_matrix().
// Point Y is projected on perpendicular plane to vector X for orthogonalization.
template <std::floating_point T>
constexpr
basic_quaternion<T>
basic_orientation_quantity<T>:: convert_from_XYaxis( T Xx, T Xy, T Xz,
                                                     T Yx, T Yy, T Yz )
{
    // Normalize X
    const T norm_sqX = squared_norm({ Xx, Xy, Xz });

    if ( !std::isnormal(norm_sqX) ) [[unlikely]]
        throw std::domain_error("The squared norm of "
                "X axis-vector is infinite, NaN or subnormal (almost zero)");

    const T normX = std::sqrt( norm_sqX );
    const T normX_inv = 1./normX;
    Xx *= normX_inv;
    Xy *= normX_inv;
    Xz *= normX_inv;

    // Orthogonalize Y
    const T innerXY = Xx*Yx + Xy*Yy + Xz*Yz;
    Yx = Yx - Xx*innerXY;
    Yy = Yy - Xy*innerXY;
    Yz = Yz - Xz*innerXY;

    // Normalize Y
    const T normY = norm({ Yx, Yy, Yz });
    if (normY < normX * std::numeric_limits<T>::epsilon() * 8)
        throw std::domain_error("The norm of the orthogonal component to X of "
                "Y axis-vector is too small in relation to norm of X");
    const T normY_inv = 1./normY;
    Yx *= normY_inv;
    Yy *= normY_inv;
    Yz *= normY_inv;

    // Obtain the third axis
    T Zx = Xy*Yz - Xz*Yy;
    T Zy = Xz*Yx - Xx*Yz;
    T Zz = Xx*Yy - Xy*Yx;

    std::array<T, 9> orientation_matrix {
        Xx, Yx, Zx,
        Xy, Yy, Zy,
        Xz, Yz, Zz
    };

    return convert_from_matrix( orientation_matrix.data(), matrix_ordering::ROW_MAJOR );
}

/*** basic_orientation_quantity - Import - convert_from_axisangle ***/
template <std::floating_point T>
constexpr
basic_quaternion<T>
basic_orientation_quantity<T>:: convert_from_axisangle (T X, T Y, T Z, T angle)
{
    T norm_value = norm({ X, Y, Z });

    // Support literals in the form rot={0,0,0,0_rad};
    if ( !std::isnormal(angle) ) [[unlikely]]
        return {1, 0, 0, 0};

    if ( !std::isnormal(norm_value) ) [[unlikely]]
        throw std::domain_error("The norm of 'orientation_axis'"
                " is infinite, NaN or subnormal (almost zero)");

    T factor = std::sin( angle / 2. );
    return { std::cos( angle / 2. ) * norm_value,
             X * factor,
             Y * factor,
             Z * factor };
}

/*** basic_orientation_quantity - Import - convert_from_euler_angles ***/
template <std::floating_point T>
constexpr
basic_quaternion<T>
basic_orientation_quantity<T>:: convert_from_euler_angles( T first,
                                                           T second,
                                                           T third,
                                                           euler_order euler ) noexcept
{
    basic_orientation_quantity q1, q2, q3;

    // TODO: Confirm order of axes (May be inverted)
    switch (euler)
    {
        case euler_order::XYX:
        case euler_order::XZX:
        case euler_order::XYZ:
        case euler_order::XZY:
        case euler_order::RollPitchYaw:
            q1 = basic_orientation_quantity<T>{{1, 0, 0}, units::radians(first)};
            break;
        case euler_order::YXY:
        case euler_order::YZY:
        case euler_order::YXZ:
        case euler_order::YZX:
            q1 = basic_orientation_quantity<T>{{0, 1, 0}, units::radians(first)};
            break;
        case euler_order::ZXZ:
        case euler_order::ZYZ:
        case euler_order::ZXY:
        case euler_order::ZYX:
            q1 = basic_orientation_quantity<T>{{0, 0, 1}, units::radians(first)};
            break;
    }

    switch (euler)
    {
        case euler_order::YXY:
        case euler_order::YXZ:
        case euler_order::ZXZ:
        case euler_order::ZXY:
            q2 = basic_orientation_quantity<T>{{1, 0, 0}, units::radians(second)};
            break;
        case euler_order::XYX:
        case euler_order::XYZ:
        case euler_order::ZYZ:
        case euler_order::ZYX:
        case euler_order::RollPitchYaw:
            q2 = basic_orientation_quantity<T>{{0, 1, 0}, units::radians(second)};
            break;
        case euler_order::XZX:
        case euler_order::XZY:
        case euler_order::YZY:
        case euler_order::YZX:
            q2 = basic_orientation_quantity<T>{{0, 0, 1}, units::radians(second)};
            break;
    }

    switch (euler)
    {
        case euler_order::XYX:
        case euler_order::XZX:
        case euler_order::YZX:
        case euler_order::ZYX:
            q3 = basic_orientation_quantity<T>{{1, 0, 0}, units::radians(third)};
            break;
        case euler_order::XZY:
        case euler_order::YXY:
        case euler_order::YZY:
        case euler_order::ZXY:
            q3 = basic_orientation_quantity<T>{{0, 1, 0}, units::radians(third)};
            break;
        case euler_order::XYZ:
        case euler_order::YXZ:
        case euler_order::ZXZ:
        case euler_order::ZYZ:
        case euler_order::RollPitchYaw:
            q3 = basic_orientation_quantity<T>{{0, 0, 1}, units::radians(third)};
            break;
    }

    return {q3.to_quaternion() * q2.to_quaternion() * q1.to_quaternion()};
}


/*** basic_orientation_quantity - Export - convert_to_matrix ***/
template <std::floating_point T>
constexpr
std::array<T, 9>
basic_orientation_quantity<T>:: convert_to_matrix ( basic_orientation_quantity const& R,
                                                    matrix_ordering order ) noexcept
{
    const T & w = R.to_quaternion().w();
    const T & x = R.to_quaternion().x();
    const T & y = R.to_quaternion().y();
    const T & z = R.to_quaternion().z();

    int s = (order == matrix_ordering::ROW_MAJOR) ? 1 : -1;

    return { 2*( w*w +   x*x ) - 1,  2*( x*y - s*w*z ),      2*( x*z + s*w*y ),
             2*( x*y + s*w*z ),      2*( w*w +   y*y ) - 1,  2*( y*z - s*w*x ),
             2*( x*z - s*w*y ),      2*( y*z + s*w*x ),      2*( w*w +   z*z ) - 1 };
}

/*** basic_orientation_quantity - Export - convert_to_axisangle ***/
// DOC: Convert to the corresponding axis and orientation angle in radians.
template <std::floating_point T>
constexpr
std::array<T, 4>
basic_orientation_quantity<T>:: convert_to_axisangle ( basic_orientation_quantity const& R )
{
    basic_quaternion<T> const & Q = R.to_quaternion();
    const T normV2 = Q.x()*Q.x() + Q.y()*Q.y() + Q.z()*Q.z();
    if (std::isnormal(normV2))
        return {Q.x(), Q.y(), Q.z(), 2 * std::atan2( std::sqrt(normV2), Q.w() )};
    else
        return {1, 0, 0, 0};
}



/*** Non-member functions: approx_equal, slerp ***/

// approx_equal
template <std::floating_point T>
constexpr
bool
approx_equal( basic_orientation_quantity<T> const& r1,
              basic_orientation_quantity<T> const& r2,
              unsigned ulp )
{
    basic_quaternion<T> q1 = r1.to_quaternion(); q1.normalize();
    basic_quaternion<T> q2 = r2.to_quaternion(); q2.normalize();

    return approx_equal(  q1, q2, ulp )
        || approx_equal( -q1, q2, ulp );
}

// slerp
template <std::floating_point T>
constexpr
basic_orientation_quantity<T>
slerp( basic_orientation_quantity<T> const& a,
       basic_orientation_quantity<T> const& b,
       T t,
       slerp_solution length )
{
    t = std::clamp<T>(t, 0, 1);

    basic_quaternion<T> A = a.to_quaternion();
    basic_quaternion<T> B = b.to_quaternion();

    T scalar_product = A.w() * B.w()
                     + A.x() * B.x()
                     + A.y() * B.y()
                     + A.z() * B.z();

    // θ is half the angular distance between a and b (as orientations).
    T θ = std::acos( std::abs( scalar_product ) *
                     (length == slerp_solution::SHORTEST ? 1 : -1) );

    T sinθ = std::sin(θ);
    if (std::isnormal(sinθ))
        return basic_quaternion<T> ( std::sin( (1-t)*θ )*A + std::sin( t*θ )*B )
                                   / std::sin(θ);
    else // Small angles are explicitly reduced to linear interpolation to avoid divide by 0.
        return basic_quaternion<T> ( std::lerp(A.w(), B.w(), t),
                                     std::lerp(A.x(), B.x(), t),
                                     std::lerp(A.y(), B.y(), t),
                                     std::lerp(A.z(), B.z(), t) );
}



//______________________________ Rotation ______________________________

// DOCS: The invariant of rotation is that the basis vectors (e_x, e_y and e_z) that define
// the axis of rotation cannot be zero.
// Data is stored as an axis-angle (3+1 variables)
export
template <std::floating_point T>
class basic_rotation
{
    nin::vector<T, 3> rot_vec = {1, 0, 0};
    units::angle_T<T> rot_ang = {};

    void assert_invariant();

public:
    using value_type = T;
    using orientation = basic_orientation_quantity<T>;
    using quaternion = basic_quaternion<T>;

    constexpr basic_rotation () noexcept;

    // TODO: Make T in input arguments a template parameter. Apply to every other class.
    constexpr basic_rotation( basic_quaternion<T> const& quat, int revolutions = 0);
    constexpr basic_rotation( T const* matrix, matrix_ordering order, int revolutions = 0 );
    constexpr basic_rotation( std::array<T, 9> const& matrix, matrix_ordering order, int revs = 0);
    constexpr basic_rotation( T const* axis, units::angle_T<T> angle);
    constexpr basic_rotation( std::array<T, 3> const& axis, units::angle_T<T> angle);
    constexpr basic_rotation( orientation const& reference,
                              orientation const& measure,
                              int revolutions = 0 ) noexcept;

    constexpr void invert() noexcept;

    template <std::floating_point U> requires std::convertible_to<T, U>
    constexpr operator basic_rotation<U> () const noexcept;
    constexpr nin::vector<T, 3> const& axis() const noexcept;
    constexpr nin::vector<T, 3>      & axis()       noexcept;
    constexpr units::angle_T<T>   angle() const noexcept;
    constexpr units::angle_T<T> & angle()       noexcept;

    constexpr orientation                operator () (orientation const& orient = {}) const;
    constexpr nin::vector<T,3>           operator () (nin::vector<T,3> const& vec) const noexcept;
    constexpr basic_position_quantity<T> operator ()
                                           (basic_position_quantity<T> const& point) const noexcept;

private:
    constexpr std::array<T, 3> rotate_coords ( T Px, T Py, T Pz ) const noexcept;
};

export
template <std::floating_point T> constexpr basic_rotation<T> inv( basic_rotation<T> Q ) noexcept;
export
template <std::floating_point T> constexpr basic_rotation<T> compose (
                                                   basic_rotation<T> const& left_evaluates_after,
                                                   basic_rotation<T> const& right_evaluates_first,
                                                   int revolutions = 0 ) noexcept;

export
using rotation = basic_rotation<double>;


//______________________________ Rotation - definitions ______________________________

/*** Invariant ***/

template <std::floating_point T>
void
basic_rotation<T>:: assert_invariant()
{
    if ( !( std::isnormal(rot_vec[0]) ||
            std::isnormal(rot_vec[1]) ||
            std::isnormal(rot_vec[2]) ) ) [[unlikely]]
    {
        if (std::isnormal(rot_ang.SI()))
            throw std::logic_error("Violation of invariant of basic_rotation<>");
        else
            rot_vec[0] = 1;
    }
}

/*** Semiregular traits ***/

template <std::floating_point T>
constexpr
basic_rotation<T>:: basic_rotation () noexcept
    = default;

/*** Converting constructors ***/

// Constructor from quaternion
template <std::floating_point T>
constexpr
basic_rotation<T>:: basic_rotation (basic_quaternion<T> const& quat, int revolutions)
{
    auto axisangle = basic_orientation_quantity(quat).to_axis_angle();
    rot_vec = axisangle.axis;
    rot_ang = axisangle.angle;
    //FIXME: rot_ang = axisangle.angle + degrees(360*revolutions);
    assert_invariant();
}

// Constructor from rotation matrix. P must contain 9 elements
// Limited to ±π revolutions
template <std::floating_point T>
constexpr
basic_rotation<T>:: basic_rotation( T const* matrix, matrix_ordering order, int revolutions )
  : basic_rotation { orientation{matrix, order}.to_quaternion(),
                     revolutions }
{
    assert_invariant();
}

template <std::floating_point T>
constexpr
basic_rotation<T>:: basic_rotation( std::array<T, 9> const& matrix,
                                    matrix_ordering order,
                                    int revolutions )
  : basic_rotation { matrix.data(), order, revolutions }
{ }

// Constructor from axis-angle
template <std::floating_point T>
constexpr
basic_rotation<T>:: basic_rotation( T const* axis, units::angle_T<T> angle)
  : rot_vec {axis[0], axis[1], axis[2]}
  , rot_ang {angle}
{
    assert_invariant();
}

template <std::floating_point T>
constexpr
basic_rotation<T>:: basic_rotation( std::array<T, 3> const& axis, units::angle_T<T> angle)
  : basic_rotation(axis.data(), angle)
{ }

// Constructor from two orientations
template <std::floating_point T>
constexpr
basic_rotation<T>:: basic_rotation( orientation const& reference,
                                    orientation const& measure,
                                    int revolutions ) noexcept
  : basic_rotation( measure.to_quaternion() * inv(reference.to_quaternion()) , revolutions )
{ }

/*** Operators ***/
template <std::floating_point T>
constexpr
void
basic_rotation<T>:: invert() noexcept
{
    rot_ang = -rot_ang;
}

/*** Conversions ***/

// Implicit conversion to template siblings
template <std::floating_point T>
template <std::floating_point U> requires std::convertible_to<T, U>
constexpr
basic_rotation<T>:: operator basic_rotation<U> () const noexcept
{
    return (rot_vec, rot_ang);
}

/*** projections ***/

template <std::floating_point T>
constexpr
nin::vector<T,3> &
basic_rotation<T>:: axis() noexcept
{
    return rot_vec;
}

template <std::floating_point T>
constexpr
nin::vector<T,3> const&
basic_rotation<T>:: axis() const noexcept
{
    return rot_vec;
}

template <std::floating_point T>
constexpr
units::angle_T<T>
basic_rotation<T>:: angle() const noexcept
{
    return rot_ang;
}

template <std::floating_point T>
constexpr
units::angle_T<T> &
basic_rotation<T>:: angle() noexcept
{
    return rot_ang;
}

/*** operator () ***/

template <std::floating_point T>
constexpr
basic_orientation_quantity<T>
basic_rotation<T>:: operator () (orientation const& orient) const
{
    quaternion Qr = orientation{axis().data(), angle()}.to_quaternion();
    return { Qr * orient.to_quaternion() };
}

template <std::floating_point T>
constexpr
nin::vector<T,3>
basic_rotation<T>:: operator () (nin::vector<T,3> const& vec) const noexcept
{
    return {rotate_coords( vec[0], vec[1], vec[2] )};
}

template <std::floating_point T>
constexpr
basic_position_quantity<T>
basic_rotation<T>:: operator () (basic_position_quantity<T> const& point) const noexcept
{
    std::array<T, 3> raw = rotate_coords( point[0].SI(),
                                          point[1].SI(),
                                          point[2].SI() );
    return { metres_T<T> (raw[0]),
             metres_T<T> (raw[1]),
             metres_T<T> (raw[2]) };
}

/*** Rotation of raw coordinates ***/

template <std::floating_point T>
constexpr
std::array<T, 3>
basic_rotation<T>:: rotate_coords ( T Px, T Py, T Pz ) const noexcept
{
    T X = rot_vec[0];
    T Y = rot_vec[1];
    T Z = rot_vec[2];
    T θ = rot_ang.SI();

    T norm_v = std::sqrt(X*X + Y*Y + Z*Z);
    X /= norm_v;
    Y /= norm_v;
    Z /= norm_v;

    T Sθ2 = std::sin(θ/2.);
    T Cθ2 = std::cos(θ/2.);

    T Rw =   Cθ2;
    T Rx = X*Sθ2;
    T Ry = Y*Sθ2;
    T Rz = Z*Sθ2;

    // P' = R P inv(R)

    // TODO: Drop unitary requirement for rotation vectors and add checks for angle ~= 0.
    T N2 = 1;// norm(R)*norm(R);
    T n = 2*Rw*Rw - N2;
    T s = Rx*Px + Ry*Py + Rz*Pz;

    T ret_x = ( Px*n + 2*Rx*s + 2*Rw*( Ry*Pz - Py*Rz ) ) / N2;
    T ret_y = ( Py*n + 2*Ry*s + 2*Rw*( Rz*Px - Pz*Rx ) ) / N2;
    T ret_z = ( Pz*n + 2*Rz*s + 2*Rw*( Rx*Py - Px*Ry ) ) / N2;

    return {ret_x, ret_y, ret_z};
}

/*** Non-member functions ***/
template <std::floating_point T>
constexpr
basic_rotation<T>
inv( basic_rotation<T> Q ) noexcept
{
    Q.invert();
    return Q;
}

// DOCS: The number of full revolutions of each rotation is discarded during composition.
template <std::floating_point T>
constexpr
basic_rotation<T>
compose( basic_rotation<T> const& left_evaluates_after,
         basic_rotation<T> const& right_evaluates_first,
         int revolutions ) noexcept
{
    basic_rotation<T> const& lhs = left_evaluates_after;
    basic_rotation<T> const& rhs = right_evaluates_first;
    using orientation = basic_orientation_quantity<T>;
    basic_quaternion<T> qlhs = orientation{ lhs.axis(), lhs.angle() }.to_quaternion();
    basic_quaternion<T> qrhs = orientation{ rhs.axis(), rhs.angle() }.to_quaternion();
    return {qlhs * qrhs, revolutions};
}

//________________________________ Pose quantity ________________________________

template <std::floating_point T>
struct basic_pose_quantity
{
    nin::basic_position_quantity<T>    position;
    nin::basic_orientation_quantity<T> orientation;
};

export
using pose_quantity = basic_pose_quantity<double>;

export
class pose_value;
export
using pose = pose_value;


//________________________________ Rigid body transformation ________________________________

template <std::floating_point T>
struct basic_rigid_body_tf
{
    nin::basic_translation<T> translation;
    nin::basic_rotation<T>    rotation;

    constexpr basic_position_quantity<T> operator () (basic_position_quantity<T> const& p) const;
    constexpr basic_pose_quantity<T>     operator () (basic_pose_quantity<T> const& p) const;
};

export
using rigid_body_tf = basic_rigid_body_tf<double>;

/*** Non-member functions ***/
export
template <std::floating_point T> [[nodiscard]] constexpr
basic_rigid_body_tf<T> inv( basic_rigid_body_tf<T> const& rbtf );

export
template <std::floating_point T> [[nodiscard]] constexpr
basic_rigid_body_tf<T> compose( basic_rigid_body_tf<T> const& after,
                                basic_rigid_body_tf<T> const& first );

//______________________ Rigid body transformation - definitions ______________________

template <std::floating_point T>
constexpr
basic_position_quantity<T>
basic_rigid_body_tf<T>:: operator () (basic_position_quantity<T> const& p) const
{
    return this->translation( rotation( p ) );
}

template <std::floating_point T>
constexpr
basic_pose_quantity<T>
basic_rigid_body_tf<T>:: operator () (basic_pose_quantity<T> const& p) const
{
    return { operator() (p.position), rotation(p.orientation) };
}

/*** Non-member functions ***/

template <std::floating_point T>
[[nodiscard]] constexpr
basic_rigid_body_tf<T>
inv( basic_rigid_body_tf<T> const& rbtf )
{
    basic_rotation<T>    invrot = inv(rbtf.rotation);
    basic_translation<T> transl {
                invrot({static_cast<std::array<units::length_T<T>, 3> const&>(rbtf.translation)}) };
    return { inv(transl), invrot };
}

template <std::floating_point T>
[[nodiscard]] constexpr
basic_rigid_body_tf<T>
compose( basic_rigid_body_tf<T> const& after,
         basic_rigid_body_tf<T> const& first )
{
    nin::basic_translation<T> first_rot { after.rotation(
                  {static_cast< std::array<units::length_T<T>, 3> const&>(first.translation)}) };
    nin::basic_translation<T> c_tr { after.translation.Δx() + first_rot.Δx(),
                                     after.translation.Δy() + first_rot.Δy(),
                                     after.translation.Δz() + first_rot.Δz() };
    nin::basic_rotation<T> crot = compose(after.rotation, first.rotation, 0);

    return {c_tr, crot};
}


//_______________________ Planar orientation quantity ________________________

export
template <std::floating_point T>
class basic_planar_orientation_quantity
{
    using angle = units::angle_T<T>;
    std::complex<T> rawC = {1, 0};

public:
    using value_type = T;

    constexpr basic_planar_orientation_quantity () noexcept;

    template <typename Tin>
    constexpr basic_planar_orientation_quantity (std::complex<Tin> const& cplx);
    constexpr basic_planar_orientation_quantity (angle const& cplx) noexcept;

    template <std::floating_point U>
    constexpr operator basic_planar_orientation_quantity<U> () const noexcept;

    constexpr std::complex<T> const& to_complex () const noexcept;

    constexpr angle to_angle (angle modulus_centre = units::radians(0.)) const noexcept;
};

export
template <std::floating_point T> constexpr bool
approx_equal ( basic_planar_orientation_quantity<T> const& r1,
               basic_planar_orientation_quantity<T> const& r2,
               unsigned ulp = 8 );


export
using planar_orientation_quantity = basic_planar_orientation_quantity<double>;


//_______________________ Planar orientation quantity - definitions ________________________

template <std::floating_point T>
constexpr
basic_planar_orientation_quantity<T>:: basic_planar_orientation_quantity () noexcept
    = default;

/*** Converting constructors ***/

template <std::floating_point T>
template <typename Tin>
constexpr
basic_planar_orientation_quantity<T>:: basic_planar_orientation_quantity
                                                                  (std::complex<Tin> const& cplx)
  : rawC { cplx }
{
    if ( !std::isnormal(std::abs(cplx)) ) [[unlikely]]
        throw std::domain_error("The magnitude of the orientation in complex form is "
                                "infinite, NaN or subnormal (almost zero)");
}

template <std::floating_point T>
constexpr
basic_planar_orientation_quantity<T>:: basic_planar_orientation_quantity
                                                                    (angle const& value) noexcept
  : rawC { std::cos(value.SI()), std::sin(value.SI()) }
{ }

template <std::floating_point T>
template <std::floating_point U>
constexpr
basic_planar_orientation_quantity<T>::operator basic_planar_orientation_quantity<U>() const noexcept
{
    return {std::complex<U>{ static_cast<U>(rawC.real()), static_cast<U>(rawC.imag()) }};
}

template <std::floating_point T>
constexpr
std::complex<T> const&
basic_planar_orientation_quantity<T>:: to_complex () const noexcept
{
    return rawC;
}

template <std::floating_point T>
constexpr
units::angle_T<T>
basic_planar_orientation_quantity<T>:: to_angle (angle modulus_centre) const noexcept
{
    using nin::constants::π;

    T const rads   = std::arg(rawC);
    T const center = modulus_centre.SI();

    bool fmod_sign = (rads - center > -π );

    T const a = center + (fmod_sign ? -π : +π);
    T const b = π - center;

    T const retval = std::fmod(rads + b, 2*π) + a;

    return nin::radians(retval);
}

/*** Non-member functions ***/

template <std::floating_point T>
constexpr
bool
approx_equal ( basic_planar_orientation_quantity<T> const& r1,
               basic_planar_orientation_quantity<T> const& r2,
               unsigned ulp )
{
    T d1 = std::arg(r1.to_complex());
    T d2 = r2.to_angle(units::radians(d1)).SI();

    return approx_equal (d1, d2, ulp);
}



//______________________________ Planar rotation ______________________________

export
template <std::floating_point T>
class basic_planar_rotation
{
    using planar_ori = basic_planar_orientation_quantity<T>;

    units::angle_T<T> rot_data;

public:
    using value_type = T;

    constexpr basic_planar_rotation () noexcept;

    constexpr basic_planar_rotation( units::angle_T<T> angle ) noexcept;
    constexpr basic_planar_rotation( planar_ori const& reference,
                                     planar_ori const& measure,
                                     int revolutions = 0 ) noexcept;

    constexpr void invert() noexcept;

    template <std::floating_point U> requires std::convertible_to<T, U>
    constexpr operator basic_planar_rotation<U> () const noexcept;
    constexpr units::angle_T<T> angle() const noexcept;

    constexpr planar_ori       operator () (planar_ori const& orient = {}) const noexcept;
    constexpr nin::vector<T,2> operator () (nin::vector<T,2> const& vec) const noexcept;
    constexpr basic_planar_position_quantity<T> operator ()
                                    (basic_planar_position_quantity<T> const& point) const noexcept;

private:
    constexpr std::array<T, 2> rotate_coords ( T Px, T Py ) const noexcept;
};

export
template <std::floating_point T> constexpr bool approx_equal ( basic_planar_rotation<T> const& lhs,
                                                               basic_planar_rotation<T> const& rhs,
                                                               unsigned ulp = 8 );
export
template <std::floating_point T> constexpr basic_planar_rotation<T> inv
                                                          (basic_planar_rotation<T> R) noexcept;
export
template <std::floating_point T> constexpr
basic_planar_rotation<T> compose ( basic_planar_rotation<T> const& left_evaluates_after,
                                   basic_planar_rotation<T> const& right_evaluates_first ) noexcept;


export
using planar_rotation = basic_planar_rotation<double>;


//______________________________ Planar rotation - definitions ______________________________

template <std::floating_point T>
constexpr
basic_planar_rotation<T>:: basic_planar_rotation () noexcept
    = default;

template <std::floating_point T>
constexpr
basic_planar_rotation<T>:: basic_planar_rotation( units::angle_T<T> angle ) noexcept

  : rot_data{ angle }
{ }

template <std::floating_point T>
constexpr
basic_planar_rotation<T>:: basic_planar_rotation( planar_ori const& reference,
                                                  planar_ori const& measure,
                                                  int revolutions ) noexcept
  : rot_data{ planar_ori{measure.to_complex() * std::conj(reference.to_complex())}.to_angle() }
{
    using units::operator""_deg;
    rot_data += 360_deg * revolutions;
}


/*** Operators ***/
// In-place invert
template <std::floating_point T>
constexpr
void
basic_planar_rotation<T>:: invert() noexcept
{
    rot_data = -rot_data;
}


/*** Conversions ***/

// Implicit conversion to template siblings
template <std::floating_point T>
template <std::floating_point U> requires std::convertible_to<T, U>
constexpr
basic_planar_rotation<T>:: operator basic_planar_rotation<U> () const noexcept
{
    return rot_data;
}

template <std::floating_point T>
constexpr
units::angle_T<T>
basic_planar_rotation<T>:: angle() const noexcept
{
    return rot_data;
}

/*** operator ()  ***/

template <std::floating_point T>
constexpr
basic_planar_orientation_quantity<T>
basic_planar_rotation<T>:: operator () (planar_ori const& orient) const noexcept
{
    std::complex<T> rot = planar_ori{ rot_data }.to_complex();
    return { rot * orient.to_complex() };
}

template <std::floating_point T>
constexpr
nin::vector<T,2>
basic_planar_rotation<T>:: operator () (nin::vector<T,2> const& vec) const noexcept
{
    return nin::vector<T,2>{ rotate_coords (vec[0], vec[1]) };
}

template <std::floating_point T>
constexpr
basic_planar_position_quantity<T>
basic_planar_rotation<T>:: operator ()
                                (basic_planar_position_quantity<T> const& point) const noexcept
{
    std::array<T, 2> raw = rotate_coords( point[0].SI(),
                                          point[1].SI() );
    return { metres_T<T> (raw[0]),
             metres_T<T> (raw[1]) };
}

/*** Rotate raw point ***/

template <std::floating_point T>
constexpr
std::array<T, 2>
basic_planar_rotation<T>:: rotate_coords ( T Px, T Py ) const noexcept
{
    T const θ = rot_data.SI();
    return { Px * std::cos(θ)  -  Py * std::sin(θ),
             Px * std::sin(θ)  +  Py * std::cos(θ) };
}


/*** Non-member functions ***/
template <std::floating_point T>
constexpr
bool
approx_equal (basic_planar_rotation<T> const& lhs,
              basic_planar_rotation<T> const& rhs,
              unsigned ulp )
{
    return approx_equal( lhs.angle().SI(), rhs.angle().SI(), ulp );
}

/*** Non-member functions - inv ***/
template <std::floating_point T>
constexpr
basic_planar_rotation<T>
inv( basic_planar_rotation<T> R ) noexcept
{
    R.invert();
    return R;
}

/*** Non-member functions - compose ***/
template <std::floating_point T>
constexpr
basic_planar_rotation<T>
compose( basic_planar_rotation<T> const& left_evaluates_after,
         basic_planar_rotation<T> const& right_evaluates_first ) noexcept
{
    return basic_planar_rotation<T>{left_evaluates_after.angle() + right_evaluates_first.angle()};
}

/*** Aliases ***/

using planar_rotation = basic_planar_rotation<double>;


//________________________________ Planar pose quantity ________________________________

template <std::floating_point T>
struct basic_planar_pose_quantity
{
    nin::basic_planar_position_quantity<T>    position;
    nin::basic_planar_orientation_quantity<T> orientation;
};

export
using planar_pose_quantity = basic_planar_pose_quantity<double>;

export
class planar_pose_value;
export
using planar_pose = planar_pose_value;

//_____________________________ Planar rigid body transformation  _____________________________

template <std::floating_point T>
struct basic_planar_rigid_body_tf
{
    nin::basic_planar_translation<T> translation;
    nin::basic_planar_rotation<T>    rotation;

    constexpr basic_planar_position_quantity<T> operator ()
                                                (basic_planar_position_quantity<T> const& p) const;
    constexpr basic_planar_pose_quantity<T> operator ()
                                                (basic_planar_pose_quantity<T> const& p) const;
};

export
using planar_rigid_body_tf = basic_planar_rigid_body_tf<double>;

/*** Non-member functions ***/

export
template <std::floating_point T> [[nodiscard]] constexpr
basic_planar_rigid_body_tf<T> inv( basic_planar_rigid_body_tf<T> const& rbtf );

export
template <std::floating_point T> [[nodiscard]] constexpr
basic_planar_rigid_body_tf<T> compose( basic_planar_rigid_body_tf<T> const& after,
                                       basic_planar_rigid_body_tf<T> const& first );

//________________________ Planar rigid body transformation - definitions ________________________

template <std::floating_point T>
constexpr
basic_planar_position_quantity<T>
basic_planar_rigid_body_tf<T>:: operator () (basic_planar_position_quantity<T> const& p) const
{
    return this->translation( rotation( p ) );
}

template <std::floating_point T>
constexpr
basic_planar_pose_quantity<T>
basic_planar_rigid_body_tf<T>:: operator () (basic_planar_pose_quantity<T> const& p) const
{
    return { operator() (p.position), rotation(p.orientation) };
}

/*** Non-member functions ***/

template <std::floating_point T>
[[nodiscard]] constexpr
basic_planar_rigid_body_tf<T>
inv( basic_planar_rigid_body_tf<T> const& rbtf )
{
    basic_planar_rotation<T>    invrot = inv(rbtf.rotation);
    basic_planar_translation<T> transl {
                invrot({static_cast<std::array<units::length_T<T>, 2> const&>(rbtf.translation)}) };
    return { inv(transl), invrot };
}


/*** Non-member functions ***/

template <std::floating_point T>
[[nodiscard]] constexpr
basic_planar_rigid_body_tf<T>
compose( basic_planar_rigid_body_tf<T> const& after,
         basic_planar_rigid_body_tf<T> const& first )
{
    nin::basic_planar_translation<T> first_rot { after.rotation(
                  {static_cast< std::array<units::length_T<T>, 2> const&>(first.translation)}) };
    nin::basic_planar_translation<T> c_tr { after.translation.Δx() + first_rot.Δx(),
                                            after.translation.Δy() + first_rot.Δy() };
    nin::basic_planar_rotation<T> crot = compose(after.rotation, first.rotation);

    return {c_tr, crot};
}


//______________________________ --footer-- ______________________________

} // namespace nin

