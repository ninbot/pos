/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test_framework.hh"

#include <array>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <unistd.h>
#include <vector>

import ninbot.pos;

namespace test {

namespace units = nin::units;
using units::operator""_m;
using units::operator""_deg;
using units::operator""_rad;
using namespace nin;
using std::string;
using rbt = nin::rigid_body_tf;


/*** operator << (Used only during test development) ***/


std::ostream &
operator << (std::ostream & sout, pose const& pose)
{
    sout << std::setprecision(3)
         << "{{"
         << pose.position.x().SI() << "_m, "
         << pose.position.y().SI() << "_m, "
         << pose.position.z().SI() << "_m}, quaternion{"
         << pose.orientation.to_quaternion().w() << ", "
         << pose.orientation.to_quaternion().x() << ", "
         << pose.orientation.to_quaternion().y() << ", "
         << pose.orientation.to_quaternion().z() << "}}"
         ;
    return sout;
}
std::ostream &
operator << (std::ostream & sout, rigid_body_tf const& rbt)
{
    pose qpose {{rbt.translation, rbt.rotation()}};
    return sout << qpose;
}

std::ostream & operator << (std::ostream & sout, point const& pos)
{
    sout << " ("
         << pos.x().SI() << ", "
         << pos.y().SI() << ", "
         << pos.z().SI() << ")  "
         ;
    return sout;
}

/*** approx_equal ***/



template <std::floating_point T>
bool approx_equal (T lhs, T rhs, unsigned ulp = 8)
{
    int exponent;
    if (std::min(std::fabs(lhs), std::fabs(rhs)) < std::numeric_limits<T>::min())
        exponent = std::numeric_limits<T>::min_exponent - 1;
    else
        exponent = std::ilogb(std::min(std::fabs(lhs), std::fabs(rhs)));
    return std::fabs(lhs - rhs) <= std::ldexp(std::numeric_limits<T>::epsilon(), exponent) * ulp
        || ( lhs <= std::numeric_limits<T>::epsilon() * ulp
          && rhs <= std::numeric_limits<T>::epsilon() * ulp );
}

bool approx_equal (point const& lhs,
                   point const& rhs,
                   unsigned ulp = 8)
{
    return lhs.inCS() == rhs.inCS() && approx_equal( lhs[0].SI(), rhs[0].SI(), (double)ulp )
                                    && approx_equal( lhs[1].SI(), rhs[1].SI(), (double)ulp )
                                    && approx_equal( lhs[2].SI(), rhs[2].SI(), (double)ulp );
}

bool approx_equal (position_quantity const& lhs,
                   position_quantity const& rhs,
                   unsigned ulp = 8)
{
    return approx_equal(point{WCS, lhs}, point{WCS, rhs});
}

bool approx_equal (pose const& lhs,
                   pose const& rhs,
                   unsigned ulp = 8)
{
    return approx_equal(lhs.position_value(),    rhs.position_value(),    ulp)
        && approx_equal(lhs.orientation_value(), rhs.orientation_value(), ulp);
}


/*** Specifications of kinematics ***/


// D-H parameters of a scara robot with arms length equal to one metre.
constexpr DH_table<3> scara_DH_split = { DH_index_convention::split,
    0_m, 0_deg, 0_m,   0_deg, revolute_joint,
    0_m, 0_deg, 1_m, 180_deg, revolute_joint,
    0_m, 0_deg, 1_m,   0_deg, prismatic_joint
};



/*** test() ***/

void test_UR5();

void test()
{
    position_coordsys_DH<scara_DH_split .structural()> scara_split (nin::WCS);

    position_coordsys link2 = scara_split.link(1);
    ASSERT( approx_equal( link2.origin().map_to(WCS), position_quantity{1_m, 0_m, -70_mm} ));

    ASSERT( approx_equal( scara_split.link(0).origin().map_to(WCS), position_quantity{} ));
    ASSERT( approx_equal( scara_split.link(1).origin().map_to(WCS), position_quantity{1_m} ));
    ASSERT( approx_equal( scara_split.link(2).origin().map_to(WCS), position_quantity{2_m} ));

    position_coordsys link2_lcopy = link2.linked_copy();
    position_coordsys link2_copy  = link2; // Deep copy, but master remains the same
    ASSERT( link2 == link2_lcopy );
    ASSERT( link2 != link2_copy );

    scara_split.joint<1>(90_deg);
    ASSERT( approx_equal( scara_split.link(0).origin().map_to(WCS), position_quantity{} ));
    ASSERT( approx_equal( scara_split.link(1).origin().map_to(WCS), position_quantity{1_m} ));
    ASSERT( approx_equal( scara_split.link(2).origin().map_to(WCS), position_quantity{1_m, -1_m} ));

    scara_split.joints(decltype(scara_split)::joint_values( 90_deg, 180_deg, 7_cm ));
    ASSERT( approx_equal( scara_split.offset().translation({}), position_quantity{0_m, 0_m, -70_mm} ));

    ASSERT( approx_equal( link2_lcopy.origin().map_to(WCS), position_quantity{0_m, 1_m, -70_mm} ));
    ASSERT( approx_equal( link2_copy.origin().map_to(WCS), position_quantity{0_m, 1_m, -70_mm} ));

    test_UR5();
}

//________________________________ UR5 ________________________________

// Universal robots UR5 - Manually
using poseq = nin::pose_quantity;
using coordsys = nin::position_coordsys_treenode;
constexpr length l1 = 0.089159_m;
constexpr length l2 = 0.425_m;
constexpr length l3 = 0.39225_m;
constexpr length l4 = 0.10915_m;
constexpr length l5 = 0.09465_m;
constexpr length l6 = 0.0823_m;

coordsys L_0 { WCS, poseq{{}, {{0,0,1}, 180_deg}} };

coordsys L_1 { L_0, poseq{{0_m, 0_m, l1}, {{1,0,0}, 90_deg}} };
coordsys L_2 { L_1, poseq{{0_m, l2, 0_m}, {{0,0,1}, -90_deg}} };
coordsys L_3 { L_2, poseq{{-l3, 0_m, 0_m}, {}} };
coordsys L_4 { L_3, poseq{{0_m, 0_m, l4}, (quaternion{1,0,0,-1} * quaternion{1,1,0,0})} };
coordsys L_5 { L_4, poseq{{0_m, 0_m, l5}, {{1,0,0}, -90_deg}} };
coordsys L_6 { L_5, poseq{{0_m, 0_m, l6}, {}} };

// Universal robots UR5 - Using DH
constexpr nin::DH_table<6> UR5 = { nin::DH_index_convention::split,
    0_m,   0_deg, 0_m,   0_deg, nin::revolute_joint,
     l1, 180_deg, 0_m,  90_deg, nin::revolute_joint,
    0_m,  90_deg,  l2, 180_deg, nin::revolute_joint,
    0_m,   0_deg,  l3, 180_deg, nin::revolute_joint,
     l4,  90_deg, 0_m,  90_deg, nin::revolute_joint,
     l5, 180_deg, 0_m,  90_deg, nin::revolute_joint };

void test_UR5()
{
    position_coordsys_DH<UR5.structural()> UR5(WCS);
    std::array<nin::position_coordsys, 6> links;
    for (size_t i = 0; i < 6; i++)
        links[i] = UR5.link(i);

    ASSERT( approx_equal(L_0.origin().map_to(WCS), links[0].origin().map_to(WCS)) );
    ASSERT( approx_equal(L_1.origin().map_to(WCS), links[1].origin().map_to(WCS)) );
    ASSERT( approx_equal(L_2.origin().map_to(WCS), links[2].origin().map_to(WCS)) );
    ASSERT( approx_equal(L_3.origin().map_to(WCS), links[3].origin().map_to(WCS)) );
    ASSERT( approx_equal(L_4.origin().map_to(WCS), links[4].origin().map_to(WCS)) );
    ASSERT( approx_equal(L_5.origin().map_to(WCS), links[5].origin().map_to(WCS)) );

    ASSERT(!approx_equal(L_3.origin().map_to(WCS), links[2].origin().map_to(WCS)) );

}


} // namespace test

