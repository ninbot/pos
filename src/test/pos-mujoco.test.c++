/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test_framework.hh"

#include <mujoco/mujoco.h>

#include <iostream>
#include <iomanip>
#include <unistd.h>

import ninbot.pos;

namespace test {

namespace units = nin::units;
using units::operator""_m;
using units::operator""_deg;
using units::operator""_rad;
using namespace nin;
using std::string;
using rbt = nin::rigid_body_tf;


std::ostream &
operator << (std::ostream & sout, pose const& pose)
{
    sout << std::setprecision(16)
        << "{{"
        << pose.position.x().SI() << "_m, "
        << pose.position.y().SI() << "_m, "
        << pose.position.z().SI() << "_m}, quaternion{"
        << pose.orientation.to_quaternion().w() << ", "
        << pose.orientation.to_quaternion().x() << ", "
        << pose.orientation.to_quaternion().y() << ", "
        << pose.orientation.to_quaternion().z() << "}}"
        ;
    return sout;
}
std::ostream &
operator << (std::ostream & sout, rigid_body_tf const& rbt)
{
    pose qpose {{rbt.translation, rbt.rotation()}};
    return sout << qpose;
}

std::ostream & operator << (std::ostream & sout, point const& pos)
{
    sout << " ("
         << pos.x().SI() << ", "
         << pos.y().SI() << ", "
         << pos.z().SI() << ")  "
         ;
    return sout;
}


template <std::floating_point T>
bool approx_equal (T lhs, T rhs, unsigned ulp = 8)
{
    int exponent;
    if (std::min(std::fabs(lhs), std::fabs(rhs)) < std::numeric_limits<T>::min())
        exponent = std::numeric_limits<T>::min_exponent - 1;
    else
        exponent = std::ilogb(std::min(std::fabs(lhs), std::fabs(rhs)));
    return std::fabs(lhs - rhs) <= std::ldexp(std::numeric_limits<T>::epsilon(), exponent) * ulp
        || ( lhs <= std::numeric_limits<T>::epsilon() * ulp
          && rhs <= std::numeric_limits<T>::epsilon() * ulp );
}

bool approx_equal (point const& lhs,
                   point const& rhs,
                   unsigned ulp = 8)
{
    return lhs.inCS() == rhs.inCS() && approx_equal( lhs[0].SI(), rhs[0].SI(), (double)ulp )
                                    && approx_equal( lhs[1].SI(), rhs[1].SI(), (double)ulp )
                                    && approx_equal( lhs[2].SI(), rhs[2].SI(), (double)ulp );
}

bool approx_equal (position_quantity const& lhs,
                   position_quantity const& rhs,
                   unsigned ulp = 8)
{
    return approx_equal(point{WCS, lhs}, point{WCS, rhs});
}

bool approx_equal (pose const& lhs,
                   pose const& rhs,
                   unsigned ulp = 8)
{
    return approx_equal(lhs.position_value(),    rhs.position_value(),    ulp)
        && approx_equal(lhs.orientation_value(), rhs.orientation_value(), ulp);
}

mjModel * mmodel;
mjData  * mdata;

void test()
{
    std::cout << "Testing POS functions for MuJoCo ..." << '\n';
    const char * file = "pos-mujoco.test.xml";

    // Load MuJoCo model
    char error[256] = "Could not load XML model";
    mmodel = mj_loadXML(file, nullptr, error, 255);
    if (!mmodel)
        throw std::runtime_error(std::string("Error loading model file: ") + std::string(error));

    mdata = mj_makeData(mmodel);
    mj_forward(mmodel, mdata);

    const units::length r3_2 = units::metres(std::sqrt(3.) / 2.);

    point p_123 {{1_m, 2_m, 3_m}};
    orientation o_X_1rad {{{1, 0, 0}, 1_rad}};

    position_coordsys_backend_mujoco(mmodel, mdata, mjOBJ_BODY, "X");

    position_coordsys_mujoco X (mmodel, mdata, mjOBJ_XBODY, "X");
    ASSERT( approx_equal( mapCS(X, WCS)(), point{WCS, { 4_m, 0_m, 0_m}} ));
    ASSERT( approx_equal( mapCS(WCS, X)(), point{X,   {-4_m, 0_m, 0_m}} ));
    ASSERT(!approx_equal( mapCS(WCS, X)(), point{X,   {-4_m, 0_m, 1e-10_m}} ));

    ASSERT_THROW( position_coordsys_mujoco error1_cs (mmodel, mdata, mjOBJ_JOINT, "Type not supported" ) );
    ASSERT_THROW( position_coordsys_mujoco error2_cs (mmodel, mdata, mjOBJ_BODY, "Body does not exist" ) );

    pose_coordsys RZ   { position_coordsys_mujoco{mmodel, mdata, mjOBJ_BODY, "RZ"  } };
    pose_coordsys ZRZ  { position_coordsys_mujoco{mmodel, mdata, mjOBJ_BODY, "ZRZ" } };
    pose_coordsys Z_RX { position_coordsys_mujoco{mmodel, mdata, mjOBJ_BODY, "Z-RX"} };




    ASSERT( approx_equal( mapCS(RZ, WCS)(), pose{WCS, {{}, {{0, 0, 1},      120_deg}}} ));
    ASSERT( approx_equal( mapCS(WCS, RZ)(), pose{RZ,  {{}, {{0, 0, 1},     -120_deg}}} ));
    ASSERT(!approx_equal( mapCS(WCS, RZ)(), pose{RZ,  {{}, {{0, 1e-10, 1}, -120_deg}}} ));

    ASSERT( approx_equal( mapCS(ZRZ, WCS) (), pose{WCS,  {{0_m, 0_m,  5_m}, {{0, 0, 1},  180_deg}}} ));
    ASSERT( approx_equal( mapCS(WCS, ZRZ) (), pose{ZRZ,  {{0_m, 0_m, -5_m}, {{0, 0, 1}, -180_deg}}} ));

    ASSERT( approx_equal( mapCS(Z_RX, WCS)(), pose{WCS,  {{3_m, 0_m,  0_m}, {{0, 0, 1},  90_deg}}} ));
    ASSERT( approx_equal( mapCS(WCS, Z_RX)(), pose{Z_RX, {{0_m, 3_m,  0_m}, {{0, 0, 1}, -90_deg}}} ));

    auto PCS_X = pose_coordsys{X};
    ASSERT( approx_equal(     mapCS(pose_coordsys{X}, RZ) (), pose{RZ,    {{-2_m, -4.*r3_2, 0_m}, {{0, 0, 1}, -120_deg}}} ));
    ASSERT( approx_equal(     mapCS(RZ,            PCS_X) (), pose{PCS_X, {{-4_m, 0_m,      0_m}, {{0, 0, 1},  120_deg}}} ));
    //ASSERT( approx_equal( inv(mapCS(pose_coordsys{X}, RZ))(), pose{RZ,    {{-4_m, 0_m,      0_m}, {{0, 0, 1},  120_deg}}} ));

    ASSERT( approx_equal( mapCS(X, WCS)({10_m, 100_m, 1000_m}), position_quantity{14_m, 100_m, 1000_m} ));
    ASSERT( approx_equal( mapCS(WCS, RZ)({10_m, 100_m, 1000_m}), pose_quantity{{ 81.6025403784439_m, -58.6602540378444_m, 1000_m}, {{0,     0, 1}, -120_deg}} ));
    ASSERT(!approx_equal( mapCS(WCS, RZ)({10_m, 100_m, 1000_m}), pose_quantity{{ 81.6025403784439_m, -58.6602540378444_m, 1000_m}, {{0, 1e-12, 1}, -120_deg}} ));
    ASSERT( approx_equal( mapCS(RZ, WCS)({10_m, 100_m, 1000_m}), pose_quantity{{-91.6025403784439_m, -41.3397459621556_m, 1000_m}, {{0,     0, 1},  120_deg}} ));

    pose_coordsys poseX = pose_coordsys{X};
    auto fwd = mapCS(poseX, RZ);
    auto inv = mapCS(RZ, poseX);
    ASSERT( approx_equal( fwd(inv()), {RZ} ));
    ASSERT( approx_equal( inv(fwd()), {poseX} ));
    ASSERT_THROW( auto inv_inv = inv(inv()) );
    ASSERT_THROW( auto fwd_fwd = fwd(fwd()) );

    char const* from        = "probe";
    char const* from_parent = "box";
    char const* to          = "world";
    pose_coordsys CSfrom        { position_coordsys_mujoco{mmodel, mdata, mjOBJ_BODY, from} };
    pose_coordsys CSfrom_parent { position_coordsys_mujoco{mmodel, mdata, mjOBJ_BODY, from_parent} };
    pose_coordsys CSto          { position_coordsys_mujoco{mmodel, mdata, mjOBJ_BODY, to} };

    auto PF = mapCS( CSfrom_parent, CSfrom );
    auto PI = mapCS( CSfrom, CSfrom_parent );
    auto GF = mapCS( WCS, CSfrom );
    auto GI = mapCS( CSfrom, WCS );

    ASSERT( approx_equal( PF().orientation, orientation{quaternion{0.996917333733128, -0.078459095727845, 0, 0}} )); // Ground truth
    ASSERT( approx_equal( PF().orientation, orientation{quaternion{0.996917333733084, -0.078459095727845, 0, 0}} )); // Still matches - 64 units - 6 bits of error tolerance
    ASSERT(!approx_equal( PF().orientation, orientation{quaternion{0.996917333733083, -0.078459095727845, 0, 0}} )); // No match

    ASSERT( approx_equal(PF(), pose{CSfrom,        {{0_m, -0.987688340595138_m, 0.156434465040231_m}, quaternion{0.996917333733128,  -0.078459095727845, 0, 0}}}));
    ASSERT( approx_equal(PI(), pose{CSfrom_parent, {{0_m, 1_m, 0_m}, quaternion{0.996917333733128, 0.078459095727845, 0, 0}}}));
    ASSERT( approx_equal(GF(), pose{CSfrom,        {{-0.5_m, -0.132325146618051_m, 0.0209582442879623_m}, quaternion{0.863355736485938, -0.0679475700582689, -0.0392295478639225, -0.498458666866564}}}));
    ASSERT( approx_equal(GI(), pose{WCS,           {{0.133974596215561_m, 0.5_m, 0_m}, quaternion{0.863355736485938, 0.0679475700582689, 0.0392295478639225, 0.498458666866564}}}, 15));
    ASSERT(!approx_equal(GI(), pose{WCS,           {{0.133974596215561_m, 0.5_m, 0_m}, quaternion{0.863355736485938, 0.0679475700582689, 0.0392295478639225, 0.498458666866564}}}, 14));

    ASSERT( approx_equal(PI(PF()), {CSfrom_parent}));
    ASSERT( approx_equal(PF(PI()), {CSfrom}));
    ASSERT( approx_equal(GI(GF()), {WCS}));
    ASSERT( approx_equal(GF(GI()), {CSfrom}));

    ASSERT(!approx_equal(GI(PF()), {WCS}));
    ASSERT(!approx_equal(PI(GF()), {CSfrom_parent}));
    ASSERT_THROW( approx_equal(GF(PI()), {CSfrom}) );

    auto fun = mapCS(CSfrom, CSto);
    pose_quantity poseq = fun( pose_quantity{{0_m, 2_m, 3_m}, {}} );
        ASSERT( approx_equal(poseq, pose_quantity{{-1.170323129481806_m, 1.253036643034791_m, 3.275933951865874_m}, quaternion{0.8633557364859381, 0.06794757005826886, 0.03922954786392247, 0.4984586668665641}}));

    orientation orient (poseq.orientation);
        ASSERT( approx_equal(pose_quantity{{}, orient}, pose_quantity{{0_m, 0_m, 0_m}, quaternion{0.863355736485938, 0.0679475700582689, 0.0392295478639225, 0.498458666866564}}));
    rotation rot ({}, orient);
    auto a = rot({0, 10, 0});
        ASSERT( approx_equal(a[0], -8.553631939770867) );
        ASSERT( approx_equal(a[1],  4.938441702975684) );
        ASSERT( approx_equal(a[2],  1.564344650402309) );
}

} // namespace test

