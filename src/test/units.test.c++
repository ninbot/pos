/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <iostream>
#include <iomanip>
#include <chrono>

#include "test_framework.hh"

import ninbot.pos;


using namespace nin::units;
using namespace nin::constants;

void alias_fun(length l);

inline
std::ostream &
operator << (std::ostream & sout, dimension dim)
{
    bool some_dimension_positive =
         (dim.α > 0) ||
         (dim.β > 0) ||
         (dim.γ > 0) ||
         (dim.δ > 0) ||
         (dim.ε > 0) ||
         (dim.ζ > 0) ||
         (dim.η > 0) ;
    bool some_dimension_negative =
         (dim.α < 0) ||
         (dim.β < 0) ||
         (dim.γ < 0) ||
         (dim.δ < 0) ||
         (dim.ε < 0) ||
         (dim.ζ < 0) ||
         (dim.η < 0) ;

    if (!some_dimension_positive && !some_dimension_negative)
        return sout << "[ ]";

    sout << "[ ";
    if (some_dimension_positive)
    {
        if (dim.α > 0) { sout << "T"; if (dim.α != 1) sout << "^" << static_cast<int>(dim.α); sout << " "; }
        if (dim.β > 0) { sout << "L"; if (dim.β != 1) sout << "^" << static_cast<int>(dim.β); sout << " "; }
        if (dim.γ > 0) { sout << "M"; if (dim.γ != 1) sout << "^" << static_cast<int>(dim.γ); sout << " "; }
        if (dim.δ > 0) { sout << "I"; if (dim.δ != 1) sout << "^" << static_cast<int>(dim.δ); sout << " "; }
        if (dim.ε > 0) { sout << "Θ"; if (dim.ε != 1) sout << "^" << static_cast<int>(dim.ε); sout << " "; }
        if (dim.ζ > 0) { sout << "N"; if (dim.ζ != 1) sout << "^" << static_cast<int>(dim.ζ); sout << " "; }
        if (dim.η > 0) { sout << "J"; if (dim.η != 1) sout << "^" << static_cast<int>(dim.η); sout << " "; }
    }
    else
    {
        sout << "1 ";
    }

    if ( (dim.α < 0) ||
         (dim.β < 0) ||
         (dim.γ < 0) ||
         (dim.δ < 0) ||
         (dim.ε < 0) ||
         (dim.ζ < 0) ||
         (dim.η < 0) )
    {
        sout << "/ ";
        if (dim.α < 0) sout << "T^" << static_cast<int>(dim.α) << " ";
        if (dim.β < 0) sout << "L^" << static_cast<int>(dim.β) << " ";
        if (dim.γ < 0) sout << "M^" << static_cast<int>(dim.γ) << " ";
        if (dim.δ < 0) sout << "I^" << static_cast<int>(dim.δ) << " ";
        if (dim.ε < 0) sout << "Θ^" << static_cast<int>(dim.ε) << " ";
        if (dim.ζ < 0) sout << "N^" << static_cast<int>(dim.ζ) << " ";
        if (dim.η < 0) sout << "J^" << static_cast<int>(dim.η) << " ";
    }

    sout << "]";

    return sout;
}

template <dimension D, std::floating_point T>
std::ostream & operator << (std::ostream & sout, value<D, T> value) noexcept
{
    return sout << value.SI() << " " << value.dimensions;
}


namespace test {

void test()
{
    auto g = millimetres(250'000);
    auto speed = g / 3_s;
    auto period = 0_s_F;
    //period += (1./Δv_cs);
    period = 55_s;
    speed = 18_m_s;
    std::cerr << "speed = " <<

    g .SI()

    << '\n';

    std::cerr << "temperature = " <<

        1

    << '\n';
    //std::cerr << "times = " <<


    //period.SI() << " " <<
    //(1.D/Δv_cs) .SI()

    //<< '\n';

    //std::cerr << "Other = " <<
    //std::setw(20) << std::setprecision(20) <<

    //G

    //<< '\n';

    std::chrono::milliseconds ms(18);
    ms += 2_ks;
    std::cerr << "Chrono " << ms.count() << "\n";


    std::cerr << "\n\n\n";

    value<g> length = micrometres(1) + 1_zm;
    std::cerr << "length " << length.SI() << "\n";
    std::cerr << "length " << (1_mm).SI() << "\n";
    std::cerr << "exalength " << exametres(1).SI() << "\n";


    value<dimension{}> deg {};
    value<dimension{}> rad = deg + 5_rad;
    //angular_velocity avel = rad / 3_s;
    //std::cerr << "avel: " << avel << '\n';

    // Check template deduction
    angle_T<double> ang1;
    angle ang2 = 1_rad;

    std::cerr << "180 degrees as rad is " << 180_deg .SI() << '\n';

    static_assert( std::convertible_to<decltype(ang1/ang2), double>);
    static_assert( std::same_as<decltype(ang1/ang2), value<{}>>);
    static_assert(!std::same_as<decltype(1/ang2), double>);
    static_assert(!std::same_as<decltype(ang1/1), double>);

    std::cerr << "0.000250m: " << (250_µm).SI() << '\n'; // DEBUG
    //std::cerr << "Gravity constant: " << G.SI() << '\n'; // DEBUG

    // FIXME:
    //static_assert(!std::same_as<decltype(ang1), decltype(1/ang2)>);

    nin::units::length sun = astronomical_units(2);
    std::cerr << "sun: " << (sun/2).in(1_au) << " AU\n";

    std::cerr << "timetosun: " << (1_au / c).in(1_min) << " minutes\n";

    // Should throw
    //std::cerr << "ang2 in 0 degrees: " << ang2.in(0_deg) << std::endl;

}

} // namespace test
