/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "test_framework.hh"

#include <cmath>

import ninbot.pos;

namespace test {

using namespace nin;

//___________________________ PROLOGUE ____________________________

/*** Type imports ***/

using std::cout;
namespace units = nin::units;
using units::operator "" _m;
using units::literals::operator "" _cm;
using units::operator "" _mm;
using units::operator "" _rad;
using units::operator "" _deg;
using nin::constants::π;

/*** Utilities ***/

template <std::floating_point Tl, std::floating_point Tr>
bool approx_equal(Tl lhs, Tr rhs, unsigned ulp = 8)
{
    using T = std::conditional<sizeof(Tl) < sizeof(Tr), Tl, Tr>::type;
    T diff = std::fabs(lhs - rhs);
    T max_diff = std::numeric_limits<T>::epsilon() * std::fmax(std::fabs(lhs),std::fabs(rhs)) * ulp;
    return ( diff <= max_diff );
}


static_assert(std::semiregular<        position_quantity >);
static_assert(std::semiregular< planar_position_quantity >);
static_assert(std::semiregular<              translation >);
static_assert(std::semiregular<       planar_translation >);



void test()
{

}


} // namespace test

