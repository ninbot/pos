/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cmath>
#include "test_framework.hh"

import ninbot.pos;

namespace test {

using namespace nin;

//___________________________ PROLOGUE ____________________________

/*** Type imports ***/

using namespace nin;


/*** Utilities ***/

// Approx_equal si difficult to implement. Specially when one operand is zero. To make it
// correct, we need the maximum value that the floating point numberas reach during operations.

template <std::floating_point T>
bool approx_equal (T lhs, T rhs, unsigned ulp)
{
    T max = std::max(std::fabs(lhs), std::fabs(rhs));
    T const max_separation = double(ulp) * std::max(max, 1.) * std::numeric_limits<T>::epsilon();
    return std::fabs(lhs - rhs) <= max_separation;
}


units::length squared_norm (point const& pos)
{
    double X = pos.x().SI();
    double Y = pos.y().SI();
    double Z = pos.z().SI();
    return units::metres( X*X + Y*Y + Z*Z );
}

units::length norm (point const& pos)
{
    return units::metres( std::sqrt( squared_norm(pos).SI() ) );
}

// T is coordsys_treenode<T, .. >
template <typename T>
void test_value_semantics_but_equality(typename T::tf_data_type tf)
{
    // Test WCS
    coordsys<typename T::coordsys_class> WCS_A;
    coordsys<typename T::coordsys_class> WCS_B;
    ASSERT( (WCS_A == WCS_B));
    ASSERT(!(WCS_A != WCS_B));

    // Test child
    T A { tf, WCS_A };
    T B (A);
    ASSERT(!(A == B));
    ASSERT( (A != B));

    coordsys<typename T::coordsys_class> phony1 { WCS_A };
}

//__________________________ TEST POSITION COORDINATE SYSTEM _____________________________

std::ostream & operator << (std::ostream & sout, point const& P)
{
    return sout << std::setprecision(15)
                << "( " << P.x().SI()
                << ", " << P.y().SI()
                << ", " << P.z().SI()
                << " )";
}

bool approx_equal (point const& lhs,
                   point const& rhs,
                   unsigned ulp = 8)
{
    return lhs.inCS() == rhs.inCS() && approx_equal( lhs[0].SI(), rhs[0].SI(), ulp )
                                    && approx_equal( lhs[1].SI(), rhs[1].SI(), ulp )
                                    && approx_equal( lhs[2].SI(), rhs[2].SI(), ulp );
}

bool approx_equal (length const& lhs, length const& rhs, unsigned ulp = 8)
{
    return approx_equal(lhs.SI(), rhs.SI(), ulp);
}

void test_pos_coordsys()
{
    std::clog << "testing position_coordsys ..." << '\n';

    test_semiregular< position_coordsys >();
    translation regular = {1_km, 1_Mm, 1_Tm};
    test_value_semantics_but_equality<position_coordsys_treenode>(nin::rigid_body_tf{regular, {}});


    position_coordsys PositionCS0;

    position_coordsys defaultCS;
    position_coordsys_treenode defaultCS_treenode;
    position_coordsys_treenode pcs1  { rigid_body_tf{1_m, 0_m, 0_m}, WCS  };
    position_coordsys_treenode pcs11 { rigid_body_tf{1_m, 0_m, 0_m}, pcs1 };
    position_coordsys_treenode pcs2  { rigid_body_tf{0_m, 1_m, 0_m}, WCS  };

    position_coordsys_treenode from_pose1 { WCS, {1_m, 0_m, 0_m} };
    position_coordsys_treenode from_pose2 { WCS, pose_quantity{1_m, 0_m, 0_m} };
    // position_coordsys_treenode from_pose3 = { WCS, {1_m, 0_m, 0_m} }; // Error (good, explicit)
    // position_coordsys_treenode from_pose4 = { WCS, pose_quantity{1_m, 0_m, 0_m} }; // Error (same)

    position_coordsys_treenode pcs1bis { rigid_body_tf{1_m, 0_m, 0_m}, WCS };
    ASSERT(pcs1 == pcs1);
    ASSERT(pcs1 != pcs1bis);
    ASSERT(pcs1 != pcs11);

    position_point pmusc  {   WCS, {0_m, 0_m, 7_m}};
    position_point pmusc2 ({0_m, 0_m, 7_m});
    //position_point pmusc3 {0_m, 0_m, 7_m};
    position_point pmusc4 {{0_m, 0_m, 7_m}};
    //position_point pmusc5 = {0_m, 0_m, 7_m}; // Explicit error (good)
    //position_point pmusc6 = {{0_m, 0_m, 7_m}}; // Explicit error (good)
    position_point pdedo  { pcs11, {0_m, 0_m, 1_m}};

    ASSERT(approx_equal( mapCS(pcs1, WCS)(), {WCS,  { 1_m, 0_m, 0_m}}));
    ASSERT(approx_equal( mapCS(WCS, pcs1)(), {pcs1, {-1_m, 0_m, 0_m}}));

    ASSERT(approx_equal( pmusc.map_to(pcs1), {pcs1, {-1_m, 0_m, 7_m}}));
    ASSERT(approx_equal( pdedo.map_to(WCS),  {WCS,  { 2_m, 0_m, 1_m}}));

    auto pcs11a2 = mapCS(pcs11, pcs2);
    position_point pdedo2 {pcs11, {{0_m, 0_m, 1_m}}};
    int repetitions = 50'000'000 /1e7; // Performance tests
    for (int i = 0; i < repetitions; i++)
    {
        pdedo2.x() += 1_mm;
        std::ignore = pcs11a2(pdedo2);
    }
    ASSERT(approx_equal(pcs11a2(pdedo), {pcs11a2.to_CS(), {2_m, -1_m, 1_m}}));

    position_value change_coords_q {WCS, {{0_m, 2_m, 0_m}}};
    position_coordsys_treenode change_coords (WCS, {change_coords_q, {{0, 0, 1}, 1_rad}});
    ASSERT( approx_equal(mapCS(change_coords, WCS) (), change_coords_q));

    // TODO
    //change_coords.offset() = translation(change_coords_q.qty.data);
    //ASSERT( approx_equal(mapCS(change_coords, WCS) ().qty, change_coords_q.qty));

    //change_coords.offset() = translation{0_m, 3_m, 0_m};
    //ASSERT(!approx_equal(mapCS(change_coords, WCS) ().qty, change_coords_q.qty));
    //ASSERT( approx_equal(mapCS(change_coords, WCS) ().qty, {0_m, 3_m, 0_m}));

    ASSERT( approx_equal(pcs1.origin().map_to(WCS), point{WCS, {1_m, 0_m, 0_m}}));

    position_point dist1 {pcs11, {3_m, 0_m, 0_m}};
    position_point dist2 {pcs11, {0_m, 4_m, 0_m}};
    ASSERT( approx_equal(distance(dist1, dist2), 5_m));
    ASSERT( approx_equal(distance(dist1, dist2.map_to(WCS)), 5_m));
    ASSERT( approx_equal(distance(dist1.map_to(WCS), dist2), 5_m));
    ASSERT( approx_equal(distance(dist1, position_quantity{2_m, 0_m, 0_m}), 1_m));

}

//__________________________ TEST ORIENTATION COORDINATE SYSTEM _____________________________

std::ostream & operator << (std::ostream & sout, orientation_quantity const& OQ)
{
    quaternion const& O = OQ.to_quaternion();
    return sout << std::setprecision(15)
                << "( "  << O.w()
                << ";  " << O.x()
                << ", "  << O.y()
                << ", "  << O.z()
                << " )";
}

bool approx_equal (orientation_value const& lhs,
                   orientation_value const& rhs,
                   unsigned ulp = 8)
{
    return lhs.inCS() == rhs.inCS() && approx_equal( static_cast<orientation_quantity>(lhs),
                                                     static_cast<orientation_quantity>(rhs), ulp );
}

bool approx_equal (orientation_value const& lhs,
                   quaternion const& rhs,
                   unsigned ulp = 8)
{
    return approx_equal(lhs, orientation_value{lhs.inCS(), {rhs}}, ulp);
}

void test_ori_coordsys()
{
    std::cout << "Testing orientation_coordsys ...\n";

    using namespace nin;

    test_semiregular< orientation_coordsys >();

    rotation rot_regular ({0, 0, 1},  90_deg);
    test_value_semantics_but_equality<orientation_coordsys_treenode>(rot_regular);

    // Simple tests
    orientation_coordsys_treenode rotZ  ( rotation({0, 0, 1},  90_deg ), WCS );
    orientation_coordsys_treenode rotX  ( rotation({1, 0, 0},  90_deg ), WCS );
    orientation_coordsys_treenode rot_Z ( rotation({0, 0, 1}, -90_deg ), WCS );

    auto map1_fwd = mapCS(WCS, rotZ);
    auto map1_inv = mapCS(rotZ, WCS);
    orientation_value fwd = map1_fwd();
    orientation_value inv = map1_inv();

    ASSERT(rotZ == rotZ);
    ASSERT(rotZ == map1_fwd().inCS());
    ASSERT(rotZ == fwd.inCS());
    ASSERT(fwd.inCS() == map1_fwd().inCS());

    orientation_value invfwd = map1_inv(fwd);
    orientation_value fwdinv = map1_fwd(inv);
    ASSERT(approx_equal(fwd,    {{0,0,1}, -90_deg }));
    ASSERT(approx_equal(inv,    {{0,0,1},  90_deg }));
    ASSERT(approx_equal(invfwd, {{0,0,1},   0_deg }));
    ASSERT(approx_equal(fwdinv, {{0,0,1},   0_deg }));

    auto map2Z  = mapCS(rotZ, rot_Z);
    auto map_2Z = mapCS(rot_Z, rotZ);
    orientation_value fwd_2Z = map2Z();
    orientation_value fwd2Z  = map_2Z();
    ASSERT(approx_equal(fwd_2Z, {{0,0,1},  180_deg }));
    ASSERT(approx_equal(fwd_2Z, {{0,0,1}, -180_deg }));
    ASSERT(approx_equal(fwd2Z,  {{0,0,1}, -180_deg }));

    auto mapZX = mapCS(rotZ,rotX);
    orientation_value ZX = mapZX();
    ASSERT(approx_equal(ZX, {rotX, quaternion{ 0.5,  -0.5, 0.5, 0.5 }}));

    orientation_quantity invZrotZ = map1_inv({{0, 0, 1}, 90_deg});
    ASSERT(approx_equal(invZrotZ, {quaternion{ 0,  0, 0, 1 }}));
    orientation_quantity invXrotZ = map1_inv({{0, 1, 0}, .01_rad});
    ASSERT(approx_equal(invXrotZ, {quaternion{ 0.707097942370197,
                                              -0.00353551917455988,
                                               0.00353551917455988,
                                               0.707097942370197 }}));

    orientation_quantity fwdZrotZ = map1_fwd({{0, 0, 1}, 90_deg});
    ASSERT(approx_equal(fwdZrotZ, {}));
    orientation_quantity fwdXrotZ = map1_fwd({{0, 1, 0}, .01_rad});
    ASSERT(approx_equal(fwdXrotZ, {quaternion{ 0.707097942370197,
                                               0.00353551917455988,
                                               0.00353551917455988,
                                              -0.707097942370197 }}));

    orientation_coordsys defaultCS;
    orientation_coordsys_treenode ocs1  ( rotation({1, 0, 0},  60_deg ), WCS );
    orientation_coordsys_treenode ocs11 ( rotation({1, 0, 0},  30_deg ), ocs1 );
    orientation_coordsys_treenode ocs2  ( rotation({1, 3, 5}, 2.8_rad ), WCS );

    orientation_value odedo {ocs11, {{0, 0, 1}, 1*π/2_rad } };
    ASSERT(approx_equal( mapCS(ocs11, rotX)(), orientation_value{rotX} ));
    ASSERT(approx_equal( mapCS(ocs11, rotX)(odedo), {rotX, odedo} ));
    auto ocs11_PInv = mapCS(ocs11, ocs1);
    auto ocs11_GInv = mapCS(ocs11, WCS);
    ASSERT(approx_equal(ocs11_PInv(odedo), quaternion{ 0.683012701892219,
                                                       0.183012701892219,
                                                      -0.183012701892219,
                                                       0.683012701892219 }));
    ASSERT(approx_equal(ocs11_GInv(odedo), quaternion{ 0.5,  0.5, -0.5, 0.5 }));

    orientation_value omusc {WCS, {{0, 0, 1}, 90_deg } };
    auto ocs1_PFwd  = mapCS(WCS, ocs1);
    auto ocs11_GFwd = mapCS(WCS, ocs11);
    ASSERT(approx_equal(ocs1_PFwd(omusc), quaternion{  0.612372435695795,
                                                      -0.353553390593274,
                                                       0.353553390593274,
                                                       0.612372435695794 }));
    ASSERT(approx_equal(ocs11_GFwd(omusc), quaternion{ 0.5, -0.5,  0.5, 0.5 }));

    auto ocs11a2 = mapCS(ocs11, ocs2);
    orientation_value odedo2 {ocs11, {{0, 1, 1}, 180_deg} };
    ASSERT(approx_equal(ocs11a2(), quaternion{ 0.237968690357969 ,
                                               0.00240114828935732,
                                              -0.942270168274449,
                                              -0.235567542068612  }));
    ASSERT(approx_equal(ocs11a2(odedo2), quaternion{ 0.832857032120815,
                                                    -0.499714219272489,
                                                     0.166571406424163,
                                                     0.169967142900241 }));

    rotation change_coords_q {{1, 1, 0}, 24_deg};
    auto change_coords = orientation_coordsys_treenode( change_coords_q, WCS );
    //ASSERT( approx_equal(mapCS(change_coords, WCS) ().qty, change_coords_q.qty));

    //change_coords.offset() = change_coords_q;
    //ASSERT( approx_equal(mapCS(change_coords, WCS) ().qty, change_coords_q.qty));
    //ASSERT(!approx_equal(mapCS(change_coords, WCS) ().qty, {{1, 1, 0}, 27_deg}));

    //change_coords.change_tree_offset({{1, 1, 0}, 27_deg});
    //ASSERT( approx_equal(mapCS(change_coords, WCS) ().qty, {{1, 1, 0}, 27_deg}));

    //change_coords.change_tree_offset({ocs1, {{1, 0, 0}, 27_deg}});
    //ASSERT( approx_equal(mapCS(change_coords, WCS) ().qty, {{1, 0, 0}, 87_deg}));
}

//__________________________ TEST POSE _____________________________

std::ostream & operator << (std::ostream & sout, pose const& pose)
{
    return sout << "[POS " << pose.position_value() << " QUAT" << pose.orientation_value() << "]";
}

//bool approx_equal (pose_value const& lhs,
                   //pose_value const& rhs,
                   //unsigned ulp = 8)
//{
    //return lhs.inCS() == rhs.inCS()
        //&& approx_equal( lhs.qty.position,    rhs.qty.position,    ulp )
        //&& approx_equal( lhs.qty.orientation, rhs.qty.orientation, ulp );
//}

bool approx_equal (pose const& lhs,
                   pose const& rhs,
                   unsigned ulp = 8)
{
    return approx_equal(lhs.position_value(),    rhs.position_value(),    ulp)
        && approx_equal(lhs.orientation_value(), rhs.orientation_value(), ulp);
}

void test_pose()
{
    std::cout << "Testing poses ...\n";

    using namespace nin;

    const double r3_2 = std::sqrt(3.)/2.;
    const double r2_2 = std::sqrt(2.)/2.;

    pose_coordsys defaultCS;
    pose_coordsys cs1 {position_coordsys_treenode(
            rigid_body_tf{ {0_m, 0_m, 2.4_m}, {{0, 0, 1}, π/3_rad} }, WCS )};

    ASSERT(approx_equal( mapCS(cs1, WCS)().pose_quantity::orientation,
                         orientation_quantity{{0, 0, 1}, π/3_rad} ));

    pose test1 { cs1, {{0_m, 7_m, 30_cm}, {{0, 0, 1}, 0_deg}} };
    pose copy1 = test1;
    ASSERT( approx_equal(copy1, test1));

    ASSERT(test1.inCS().position == test1.inCS().position);
    ASSERT(test1.inCS().position == cs1.position);
    ASSERT(test1.inCS().orientation == cs1.orientation);
    ASSERT(test1.inCS() == cs1);

    auto cs1PInv = mapCS(cs1, WCS);
    pose pqq = pose_quantity{ {-6.06217782649107_m, 3.5_m, 2.7_m}, quaternion{r3_2, 0., 0., 0.5} };
    ASSERT(cs1PInv(test1).inCS().position == pqq.inCS().position);
    ASSERT(cs1PInv(test1).inCS().orientation == pqq.inCS().orientation);
    ASSERT(approx_equal(cs1PInv(test1), pose_quantity{ {-6.06217782649107_m, 3.5_m, 2.7_m},
                                                       quaternion{r3_2, 0., 0., 0.5} } ));

    ASSERT( pose_coordsys{position_coordsys{}} != pose_coordsys{position_coordsys{}} );
    ASSERT( pose_coordsys{position_coordsys{}} != position_coordsys{} );
    ASSERT( pose_coordsys{WCS} == pose_coordsys{WCS} );
    ASSERT( pose_coordsys{WCS, WCS} == pose_coordsys{WCS, WCS} );
    ASSERT( pose_coordsys{} == pose_coordsys{WCS, WCS} );

    // Invert transformations
    //auto cs1PInv_inverted = inv(cs1PInv);
    auto cs1PInv_inverted = mapCS(WCS, cs1);
    pose test1_inv {WCS, pose_quantity { {0_m, 7_m, 30_cm},  {{2, 1, 4.4}, 28_deg} }};
    ASSERT(approx_equal( cs1PInv_inverted(test1_inv), test1_inv.map_to(cs1) ));
    ASSERT(approx_equal( cs1PInv_inverted(test1_inv), mapCS(WCS, cs1)(test1_inv) ));

    // Composition of a Pose with its inverse yields the identity transformation.
    pose_coordsys first  (position_coordsys_treenode{ WCS, pose_quantity{ {1_m, 0_m, 0_m},
                                                                      {{0,0,1}, 60_deg} } });
    pose_coordsys second (position_coordsys_treenode{ first.position,
                                            pose_quantity{ {0_m, 1_m, 0_m}, {{1,0,0}, 90_deg} } });

    pose_quantity const zero {};

    auto FF = mapCS(WCS, first);
    auto FI = mapCS(first, WCS);
    ASSERT( approx_equal( FF(), {first, {{-0.5_m, units::metres(r3_2), 0_m}, quaternion{r3_2, 0, 0, -0.5}}} ) );
    ASSERT(!approx_equal( FF(), {first, {{ 0.5_m, units::metres(r3_2), 0_m}, quaternion{r3_2, 0, 0, -0.5}}} ));
    ASSERT( approx_equal( FI(), {WCS, {{1_m, 0_m, 0_m}, quaternion{r3_2, 0, 0,  0.5}}} ));
    ASSERT(!approx_equal( FI(), {WCS, {{1_m, 0_m, 0_m}, quaternion{r3_2, 0, 0, -0.5}}} ));
    ASSERT( approx_equal( FI(FF( {WCS,   zero} )), {WCS,   zero} ) );
    ASSERT( approx_equal( FF(FI( {first, zero} )), {first, zero} ) );

    auto PF = mapCS(first, second);
    auto PI = mapCS(second, first);
    auto GF = mapCS(WCS, second);
    auto GI = mapCS(second, WCS);

    ASSERT( approx_equal( PF({ first, zero}), {second, {{0_m, 0_m, 1_m}, quaternion{r2_2, -r2_2, 0, 0}}} ));
    ASSERT( approx_equal( PI({second, zero}), {first,  {{0_m, 1_m, 0_m}, quaternion{r2_2,  r2_2, 0, 0}}} ));

    ASSERT( approx_equal( PI(PF( {first,  zero} )), {first,  zero} ) );
    ASSERT( approx_equal( PF(PI( {second, zero} )), {second, zero} ) );
    ASSERT( approx_equal( GI(GF( {WCS,    zero} )), {WCS,    zero} ) );
    ASSERT( approx_equal( GF(GI( {second, zero} )), {second, zero} ) );

    pose_quantity const onetwothree { 1_m, 2_m, 3_m };
    ASSERT( approx_equal( PI(PF( {first,  onetwothree} )), {first,  onetwothree} ) );
    ASSERT( approx_equal( PF(PI( {second, onetwothree} )), {second, onetwothree} ) );
    ASSERT( approx_equal( GI(GF( {WCS,    onetwothree} )), {WCS,    onetwothree}, 9 ) );
    ASSERT( approx_equal( GF(GI( {second, onetwothree} )), {second, onetwothree} ) );

    pose_value change_coords_q {WCS, { {0_m, 2_m, 0_m}, {{1, 1, 0}, 24_deg}} };
    position_coordsys_treenode change_coords_postree(WCS, change_coords_q);
    pose_coordsys change_coords = {change_coords_postree};
    ASSERT( approx_equal(mapCS(change_coords, WCS) (), {WCS, change_coords_q}));
    change_coords_postree.set_origin_in_parent(change_coords_q);
    ASSERT( approx_equal(mapCS(change_coords, WCS) (), change_coords_q));
    ASSERT(!approx_equal(mapCS(change_coords, WCS) (), {WCS, { {0_m, 2_m, 0_m}, {{1, 1, 1}, 24_deg}}}));
    change_coords_postree.set_origin_in_parent({{0_m, 3_m, 0_m}, {{1, 1, 0}, 29_deg}});
    ASSERT( approx_equal(mapCS(change_coords, WCS) (), {WCS, { {0_m, 3_m, 0_m}, {{1, 1, 0}, 29_deg}}}));
}

//__________________________ TEST PLANAR POSE COORDINATE SYSTEM _____________________________

std::ostream & operator << (std::ostream & sout, planar_position_quantity const& position)
{
    return sout << "(" << std::setprecision(15)
                << position.x().SI() << ", "
                << position.y().SI() << ")";
}
std::ostream & operator << (std::ostream & sout, planar_pose_quantity const& pose)
{
    return sout << "(" << std::setprecision(15)
                << pose.position.x().SI() << ", "
                << pose.position.y().SI() << "; "
                << pose.orientation.to_angle().SI() << ")";
}

bool approx_equal (planar_point const& lhs,
                   planar_point const& rhs,
                   unsigned ulp = 8)
{
    return approx_equal( lhs.x().SI(), rhs.x().SI(), ulp)
        && approx_equal( lhs.y().SI(), rhs.y().SI(), ulp);
}

bool approx_equal (planar_pose const& lhs,
                   planar_pose const& rhs,
                   unsigned ulp = 8)
{
    return approx_equal(lhs.position_value(), rhs.position_value())
        && approx_equal( lhs.orientation.to_angle().SI(), rhs.orientation.to_angle().SI(), ulp);
}


void test_planar_pose()
{
    std::cout << "Testing Planar Pose Coordinate System...\n";

    planar_pose_coordsys PPECS {
        planar_position_coordsys_treenode{WCS, planar_pose{{1_m, 2_m, 10_deg}}} };

    auto tf1 = mapCS(PPECS, WCS);
    ASSERT( approx_equal( tf1(), {{1_m, 2_m, 10_deg}} ) );

    planar_pose_cloud test1 (PPECS, {{0_m, 0_m, 0_deg}, {{1_m, 0_m}, -10_deg}});
    test1 = tf1(test1);
    ASSERT( approx_equal( test1[0], {{1_m, 2_m, 10_deg}} ) );
    ASSERT( approx_equal( test1[1], {{1.98480775301221_m, 2.17364817766693_m, 0_deg}} ) );

    planar_position_coordsys const* ignore;
    using PoseCS = planar_pose_coordsys;

    // Test brace elision in aggregate initialization
    planar_pose pps {{1_m, 0_m, 20_deg}};
    planar_pose copypps = pps;
    ASSERT( approx_equal(copypps, pps));

    auto posCS_560 = planar_position_coordsys_treenode( WCS, planar_pose_quantity {{1_m, 0_m}, 30*π/180._rad} );
    planar_pose_coordsys poseCS_560 = posCS_560;
    auto Forward = mapCS(WCS, poseCS_560);
    auto Inverse = mapCS(poseCS_560, WCS);

    ASSERT(approx_equal( Forward(), planar_pose_value{ {{ -0.866025403784439_m,
                                                           0.5_m},
                                                           -0.523598775598299_rad }} ));
    ASSERT(approx_equal( Inverse({{0_m, 10_m}, 0_rad}), planar_pose_value{{ -4._m,
                                                                             8.66025403784439_m,
                                                                             0.523598775598299_rad }} ));
    ASSERT(approx_equal( Forward(nin::planar_pose{{{10_m, 0_m}, 0_rad}}), planar_pose_value{{  7.79422863405995_m,
                                                                                              -4.5_m,
                                                                                              -0.523598775598299_rad }} ));
    ASSERT(approx_equal( Forward(nin::planar_pose{{{0_m, 10_m}, 0_rad}}), planar_pose{{  4.13397459621556_m,
                                                                                         9.16025403784439_m,
                                                                                        -0.523598775598299_rad }} ));


    ASSERT(!approx_equal( Forward(), planar_pose{{ -0.866025403784439_m,
                                                    0.5_m,
                                                   -0.5235987756_rad}} ));

    // Composition of a Pose with its inverse yields the identity transformation.
    auto pos_first  = planar_position_coordsys_treenode(       WCS, planar_pose_quantity {{1_m, 0_m},  87. *π/180._rad} );
    auto pos_second = planar_position_coordsys_treenode( pos_first, planar_pose_quantity {{0_m, 1_m}, -57. *π/180._rad} );
    planar_pose_coordsys first (pos_first );
    planar_pose_coordsys second(pos_second);

    auto PF = mapCS( first,  second );
    auto PI = mapCS( second, first );
    auto GF = mapCS( WCS,    second );
    auto GI = mapCS( second, WCS );

    ASSERT( approx_equal(PI(PF()), {} ) );
    ASSERT( approx_equal(PF(PI()), {} ) );
    ASSERT( approx_equal(GI(GF()), {} ) );
    ASSERT( approx_equal(GF(GI()), {} ) );

    ASSERT( approx_equal(poseCS_560.origin().map_to(WCS), planar_pose{{{1_m, 0_m}, 30*π/180._rad}}) );
}

//__________________________  TEST COORDINATE BRIDGE __________________________


void test_CoordinateBridge()
{
    std::cout << "Testing Coordinate bridge...\n";

    static_assert(coordsys_bridge_traits<bridge_point_and_planar_point>);
    static_assert(coordsys_bridge_traits<coordsys_dual_bridge<bridge_point_and_planar_point>>);

    position_coordsys CSp1  = position_coordsys_treenode( WCS, {4_m, 6_m, 7_m} );
    planar_position_coordsys CSpp1 = planar_position_coordsys_treenode( WCS, {0_m, -1_m} );

    planar_point p {WCS, {6_m, 5_m}};
    bridge_point_and_planar_point bridge {CSp1, CSpp1};
    auto br_tf = mapCS(planar_position_coordsys{}, bridge, WCS);
    ASSERT(approx_equal( br_tf(p), point {WCS, {10_m, 12_m, 7_m}} ));

}




//__________________________  TEST() __________________________
void test()
{

    static_assert(std::semiregular<pose_quantity >);
    static_assert(std::semiregular<rigid_body_tf >);
    static_assert(std::semiregular<planar_pose_quantity >);
    static_assert(std::semiregular<planar_rigid_body_tf >);


    test_pos_coordsys();
    test_ori_coordsys();
    test_pose();
    test_planar_pose();

    test_CoordinateBridge();
}

} // namespace test


