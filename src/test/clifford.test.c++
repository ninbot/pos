/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "test_framework.hh"

#include <cmath>
#include <complex>
#include <iostream>

import ninbot.pos;

// std operations on complex numbers are not constexpr ー_ー
// Maybe something can be done in C++26. Then un-comment constexpr in the header // TODO
#define static_assert(X) ASSERT(X)
#define constexpr

namespace units = nin::units;
namespace test {

using namespace nin;
using units::operator""_rad;
using units::operator""_deg;

//_______________________ QUATERNION ________________________

void test_quaternion()
{
    std::clog << "Testing quaternions ..." << "  All tests passed at compile time" << '\n';

    test_semiregular<quaternion>();

    /*** Constructors ***/
    using quat = quaternion;
    constexpr quat Q { 34, 2, 4.34, 234};
    basic_quaternion<float> Qf (Q);
    //constexpr quat Q_array { 34, std::array<long double, 3>{2, 4.34, 234}.begin() };
    //static_assert( approx_equal(Q, Q_array) );
    //constexpr int axis_vec[3] {2, 4, 34};
    //static_assert( approx_equal(quat{0, 2, 4, 34}, quat{0, axis_vec}) );

    /*** Comparisons ***/
    static_assert( approx_equal(Q, Q, 1) );
    static_assert( approx_equal(Q, Q, 0) );

    static_assert( approx_equal(inv(inv(Q)), Q, 1) );
    static_assert(!approx_equal(inv(inv(Q)), Q, 0) );


    /*** Math functions ***/
    constexpr quat Qconj = [](quat Q){Q.conjugate(); return Q;} (Q);
    static_assert( approx_equal(Qconj, {34, -2, -4.34, -234}) );
    static_assert( approx_equal(Qconj, conj(Q)) );

    constexpr quat Qnorm = [](quat Q) constexpr {Q.normalize(); return Q;} (Q);
    static_assert( norm_sq(Q) == 55934.8356 );
    static_assert( norm(Qnorm) - 1 < 1.e-14 );

    constexpr quat Qinv = [](quat Q){Q.invert(); return Q;} (Q);
    static_assert( (norm(Qinv) / norm(Q) - 1) < 1.e-14 );
    static_assert( approx_equal(Qinv, inv(Q)) );
    static_assert( approx_equal(Qinv, quat{34, -2, -4.34, -234}/55934.8356) );


    /*** Unary operators ***/
    static_assert( approx_equal(+Q, Q) );
    static_assert( approx_equal(-Q, {-34, -2, -4.34, -234} ) );


    /*** Binary operators ***/
    constexpr quat W27 { 27, 0, 0,  0 };
    constexpr quat X1  {  0, 1, 0,  0 };
    constexpr quat Y4  {  0, 0, 4,  0 };
    constexpr quat Z10 {  0, 0, 0, 10 };
    constexpr quat WXYZ = W27+X1+Y4-Z10;

    constexpr quat Qae = [](quat Q){Q += quat{3, 0, 0, 0}; return Q;} (W27);
    static_assert( approx_equal(Qae, {30, 0, 0, 0}) );
    static_assert( approx_equal(Qae, W27 + quat{3, 0, 0, 0}) );

    constexpr quat Qse = [](quat Q){Q -= quat{7, 1, 1, 9}; return Q;} (WXYZ);
    static_assert( approx_equal(Qse, {20, 0, 3, -19}) );
    static_assert( approx_equal(Qse, WXYZ - quat{7, 1, 1, 9}) );

    static_assert( approx_equal(WXYZ, {27, 1, 4, -10}) );



    constexpr quat Qpse = [](quat Q){Q *= 2; return Q;} (Q);
    static_assert( approx_equal(Qpse, {68, 4, 8.68, 468}) );
    static_assert( approx_equal(Qpse, Q*2.) );
    static_assert( approx_equal(Qpse, 2*Q) );

    constexpr quat Qdse = [](quat Q){Q /= 2; return Q;} (Q);
    static_assert( approx_equal(Qdse, {17, 1, 2.17, 117}) );
    static_assert( approx_equal(Qdse, Q/2.) );
    static_assert( approx_equal(Qdse, Q/2) );

    constexpr quat Qpe = [](quat Q){Q *= quat{7, 1, 1, 9}; return Q;} (Q);
    static_assert( approx_equal(Qpe , {-1874.34, -146.94, 280.38, 1941.66}) );
    static_assert( approx_equal(Qpe , Q*quat{7, 1, 1, 9}) );
    static_assert( approx_equal(X1*Y4 , {0, 0, 0, 4}) );

    constexpr quat Qde = [](quat Q){Q /= quat{7, 1, 1, 9}; return Q;} (Q);
    static_assert( approx_equal(Q, quat{1,0,0,0}/Qinv) );
    static_assert( approx_equal(Q, Q/quat{1,0,0,0}) );
    static_assert( norm(Q/Q) - 1 < 1.e-14 );
    static_assert( approx_equal(Qde, Q*inv(quat{7, 1, 1, 9})) );
    static_assert( approx_equal(X1*Y4 , {0, 0, 0, 4}) );


    /*** Projections ***/
    static_assert( real(Q)    == 34   );
    static_assert( imag(Q)[0] == 2    );
    static_assert( imag(Q)[1] == 4.34 );
    static_assert( imag(Q)[2] == 234  );

    static_assert( Q.w() == 34   );
    static_assert( Q.x() == 2    );
    static_assert( Q.y() == 4.34 );
    static_assert( Q.z() == 234  );

    constexpr quat Qproj = [](quat Q)
    {
        Q.w() = 12.8;
        Q.x() = 7894;
        Q.y() = 9.34;
        Q.z() = 0.01;
        return Q;
    } (Q);
    static_assert( Qproj.w() == 12.8 );
    static_assert( Qproj.x() == 7894 );
    static_assert( Qproj.y() == 9.34 );
    static_assert( Qproj.z() == 0.01 );
}

//_____________________ ORIENTATION _________________________

void test_orientation()
{
    std::clog << "Testing orientations ..." << " All tests passed at compile time" << '\n';

    test_semiregular< orientation_quantity >();

    using orientf = basic_orientation_quantity<float>;
    using orient  = basic_orientation_quantity<double>;
    using orientd = basic_orientation_quantity<double>;
    using orientl = basic_orientation_quantity<long double>;

    using std::numbers::pi;

    // Default constructor
    static_assert(approx_equal( orient(), orient( {1, 0, 0}, 0_rad), 1 ));
    constexpr orient defaulted2 = {};

    // Orientation from quaternion
    constexpr orient Oquat = quaternion(1, 0, 0, 1); // 90 deg around Z
    static_assert(approx_equal( Oquat, orient( {0, 0, 1}, 90_deg ), 1 ));

    // Orientation from matrix and Euler angles
    constexpr std::array<double, 9> Morient_case0 = {
        1, 0, 0,
        0, 1, 0,
        0, 0, 1 };
    constexpr std::array<double, 9> Morient_case1 = {
        1, 0, 0,
        0,-1, 0,
        0, 0,-1 };
    constexpr std::array<double, 9> Morient_case2 = {
       -1, 0, 0,
        0, 1, 0,
        0, 0,-1 };
    constexpr std::array<double, 9> Morient_case3 = {
       -1, 0, 0,
        0,-1, 0,
        0, 0, 1 };

    constexpr orientd r1 (   1_rad,  0_rad, -1_rad,  euler_order::XZX );
    constexpr orientd rx ( 180_deg,  0_rad,  0_rad,  euler_order::XYZ );
    constexpr orientd ry ( 180_deg,  0_rad,  0_rad,  euler_order::YZY );
    constexpr orientd rz ( 180_deg,  0_rad,  0_rad,  euler_order::ZXY );
    static_assert(approx_equal( r1, orient{Morient_case0.data(), matrix_ordering::ROW_MAJOR}, 1 ));
    static_assert(approx_equal( rx, orient{Morient_case1.data(), matrix_ordering::ROW_MAJOR}, 1 ));
    static_assert(approx_equal( ry, orient{Morient_case2.data(), matrix_ordering::ROW_MAJOR}, 1 ));
    static_assert(approx_equal( rz, orient{Morient_case3.data(), matrix_ordering::ROW_MAJOR}, 1 ));
    static_assert(approx_equal( r1, orient(), 1 ));

    constexpr std::array<double, 9> Morient_null = {
        0, 0, 0,
        0, 0, 0,
        0, 0, 0 };
    constexpr orient rnull { Morient_null.data(), matrix_ordering::ROW_MAJOR };
    static_assert(approx_equal( rnull, orient(), 1 ));

    // Orientation from XY axes
    constexpr orient XY_Z ( {0, 1, 0}, {-1, 1, 0} );
    static_assert(approx_equal( XY_Z, orient( {0, 0, 1}, 90_deg ) ));

    constexpr orient true_XY = quaternion{
            0.108981101164,
           -0.206449184668,
           -0.497908856432,
           -0.835217710795 };
    constexpr orient XY (
            { -0.891003707478,  0.023539863231, 0.453385341735 },
            {  0.387631646567, -0.480419780551, 0.786726471548 } );
    // Double supports ~15 decimal places, but only 12 were specified above.
    static_assert(!approx_equal( XY, true_XY, 2 << 8 ));
    static_assert( approx_equal( XY, true_XY, 2 << 9 ));

    // Orientation from axis-angle (additional)
    constexpr orient true_axisangle = { true_XY };
    constexpr orient Oaxisangle = { { 1, 2.41177439006, 4.04563337045 }, -15.4895672914_rad };

    static_assert(!approx_equal(Oaxisangle, true_axisangle, 2 << 13) );
    static_assert( approx_equal(Oaxisangle, true_axisangle, 2 << 14) );

    // Orientation from Euler (additional)
    constexpr orient Qeuler( 1._rad, -2.3_rad, 0.4_rad, euler_order::XYZ );
    constexpr orient true_Qeuler = quaternion{
             0.264397527923,
             0.351074806855,
            -0.746151268385,
             0.500098702165 };
    static_assert(!approx_equal(Qeuler, true_Qeuler, 2 << 9) );
    static_assert( approx_equal(Qeuler, true_Qeuler, 2 << 10) );


    // Conversion to matrix
    static_assert(approx_equal( XY, orient(XY.to_matrix(matrix_ordering::ROW_MAJOR).data(),
                                        matrix_ordering::ROW_MAJOR) ));
    static_assert(approx_equal( conj(XY.to_quaternion()),
                                orient( XY.to_matrix(matrix_ordering::ROW_MAJOR).data(),
                                     matrix_ordering::COLUMN_MAJOR
                                   ).to_quaternion()
                              )
                 );

    // Conversion to axis-angle
    constexpr orientation_quantity::axis_angle aa = true_XY.to_axis_angle();
    static_assert(approx_equal( true_XY, orient( { aa.axis[0],
                                                   aa.axis[1],
                                                   aa.axis[2] },
                                                   aa.angle) ));
    static_assert(approx_equal( orientation_quantity{{0, 0, 0}, 0_rad}, orientation_quantity{} ));

    // SLERP

    constexpr orient a1 { {1, 0, 0},  1.0_rad  };
    constexpr orient b1 { {1, 0, 0},  2.0_rad  };
    static_assert( approx_equal( slerp(a1, b1, 0.75), orient{{1,0,0}, 1.75_rad} ) );

    constexpr orient a2 = quaternion{
             0.498836732226,
             0.605748,
            -0.619795,
            -0.009243 };
    constexpr orient b2 = quaternion{
             0.499575896869,
             0.807027,
             0.27449,
             0.154229 };
    constexpr orient sol2 = quaternion{
             0.547154998664767,
             0.722269182581655,
            -0.421396915836708,
             0.037057720515904 };
    static_assert( approx_equal( slerp(a2, b2, 0.248), sol2 ) );

    static_assert( approx_equal( slerp(a2, a2, 0.348), a2 ) ); // Check no divide by 0
    static_assert( approx_equal( slerp({}, {}, 0.5), {} ) );

    // Reset orientation to default.
    orientation_quantity default_orient = a1;
    default_orient = {};
}

//_____________________ ROTATION _________________________

std::ostream & operator << (std::ostream & sout, nin::rotation const& rot)
{
    sout <<   "x=" << rot.axis()[0]
         <<  " y=" << rot.axis()[1]
         <<  " z=" << rot.axis()[2]
         << "  θ=" << rot.angle().SI();
    return sout;
}

template<typename T>
bool
compare_rots ( basic_rotation<T> const& r1,
               basic_rotation<T> const& r2,
               unsigned ulp = 8)
{
    basic_rotation<T> zero = compose(r1, inv(r2));
     return approx_equal (zero.angle().SI(), 0., ulp);
}

void test_rotation()
{
    std::clog << "Testing rotations ..." << "    All tests passed at compile time" << '\n';

    test_semiregular<rotation>();

    using rot   = basic_rotation<double>;
    using rotf  = basic_rotation<float>;
    using quatf = basic_quaternion<float>;
    using ori   = basic_orientation_quantity<double>;
    using orif  = basic_orientation_quantity<float>;

    // Converting constructors
    static_assert( rot(quaternion(0, 0,0,1), 0).angle().in(1_deg) == 180 );

    constexpr double C30 = std::sqrt(3.) / 2.;
    constexpr std::array<double, 9> M {
        C30, -0.5, 0,
        0.5,  C30, 0,
          0,    0, 1 };
    constexpr rot rotM (M, ROW_MAJOR, 3);
        static_assert( (rotM.angle() - (3*360_deg + 30_deg)).in(1_deg) == -1080);

    constexpr rot rotAA ({0.4, 385, 2.4}, 95_deg);
        static_assert( std::abs((rotAA.angle() - 95_deg).SI()) < 1e-15 );

    constexpr rot rotdiff( ori({0,0,1}, 55_deg),
                           ori({-1,1,1}, {-1,-1,1}),  -1);
        static_assert( rotdiff.axis()[0] > 0 );
        static_assert( rotdiff.axis()[1] > 0 );
        static_assert( rotdiff.axis()[2] > 0 );
        static_assert( approx_equal<double>(rotdiff.angle().SI(), 2*π-4.5693942378118777) );

    static_assert(compare_rots( inv(inv(rotM)), rotM ));

    rotation rotaa ({}, {{3,4,12}, 55_deg}, -1);
        double rotaa_factor = sin(2.*π/360. * 55./2.);
        static_assert( approx_equal(rotaa.axis()[0],  3. * rotaa_factor ) );
        static_assert( approx_equal(rotaa.axis()[1],  4. * rotaa_factor ) );
        static_assert( approx_equal(rotaa.axis()[2], 12. * rotaa_factor ) );
        static_assert( approx_equal(rotaa.angle().in(1_deg), 55.) );

    constexpr ori Qeuler( 1._rad, -2.3_rad, 0.4_rad, euler_order::XYZ );
    constexpr rot REuler ({}, Qeuler, 0);

    // Composition of orientations
    constexpr rot compX ( {1, 0, 0},  1.0_rad );
    constexpr rot compY ( {0, 1, 0}, -2.3_rad );
    constexpr rot compZ ( {0, 0, 1},  0.4_rad );
    static_assert( compare_rots(REuler, compose( compose(compZ,  compY, 0), compX, 0) ));

    static_assert( compare_rots(REuler, compose( compose(compZ,  compY, 0), compX, 0) ));
    static_assert( compare_rots(REuler, compose( compZ,  compose(compY, compX, 0), 0) ));
    static_assert(!compare_rots(REuler, compose( compose(compX,  compY, 0), compZ, 0) ));
    static_assert(!compare_rots(REuler, compose( compX,  compose(compY, compX, 0), 0) ));

    static_assert( compare_rots( rot{ {}, orientation_quantity{ 0.4_rad, -2.3_rad, 1_rad,
                euler_order::ZYX }, 0 }, compose( compX, compose(compY, compZ, 0), 0), 16 ));


    // Transformation of points
    constexpr rot  Rx ({1, 0, 0}, 180_deg);
    constexpr rot  Ry ({0, 1, 0}, 180_deg);
    constexpr rotf Rz ({0, 0, 1}, 180_deg);
    constexpr std::array<double,3> Px = Rx({7, 2, -8});
    static_assert(approx_equal( quaternion(0, Px[0], Px[1], Px[2]),
                                quaternion(0,     7,    -2,     8) ));

    constexpr std::array<double,3> Py = Ry({1, 1, 1});
    static_assert(approx_equal( quaternion(0, Py[0], Py[1], Py[2]),
                                quaternion(0,    -1,     1,    -1) ));

    constexpr std::array<float,3> Pz = Rz({1, 2, 3});
    static_assert(approx_equal( quatf(0, Pz[0], Pz[1], Pz[2]),
                                quatf(0,    -1,    -2,     3) ));

    constexpr ori rand_orient ( -0.9694_rad, 2.3189_rad, -1.8348_rad,  euler_order::ZYZ );
    constexpr rotation rand_rot ( {}, rand_orient, 0 );
    constexpr std::array<double,3> resultP = rand_rot({44.8, -71.8, 0.1});
    constexpr std::array<double,3> true_randP { -80.9050474643,
                                                 -2.0625391422,
                                                 24.7469033837 };
    static_assert(!approx_equal( quaternion( 0,    resultP[0],    resultP[1],    resultP[2]),
                                 quaternion( 0, true_randP[0], true_randP[1], true_randP[2]),
                                 2 << 12 ));
    static_assert( approx_equal( quaternion( 0,    resultP[0],    resultP[1],    resultP[2]),
                                 quaternion( 0, true_randP[0], true_randP[1], true_randP[2]),
                                 2 << 13 ));

    // Apply rotation to orientation
    static_assert( approx_equal( rotation{{0,0,1}, 40_deg}( ori{{0,0,1}, 50_deg} ),
                                 ori{{0,0,1}, 90_deg} ) );
    static_assert( approx_equal( rotation{{0,1,0}, 90_deg}( ori{{1,0,0}, 180_deg} ),
                                 ori{{1,0,-1}, 180_deg} ) );

    // Reset rotation to default.
    rotation default_rot = compX;
    default_rot = {};
}

//_____________________ PLANAR ORIENTATION _________________________

void test_planar_orientation()
{
    //std::clog << "Testing planar_orientations ..." << " All tests passed at compile time" << '\n';
    std::clog << "Testing planar_orientations ...\n";

    test_semiregular< planar_orientation_quantity >();

    using orientf = basic_planar_orientation_quantity<float>;
    using orient  = basic_planar_orientation_quantity<double>;
    using orientd = basic_planar_orientation_quantity<double>;
    using orientl = basic_planar_orientation_quantity<long double>;

    using std::numbers::pi;

    // Default constructor
    static_assert( approx_equal( orient(), orient(0_rad), 1 ));

    // Orientation from complex_number
    constexpr orient Ocomplex_int (std::complex<float>(0, 1));
    static_assert( approx_equal( Ocomplex_int, orient( 90_deg )));
    constexpr orientd Ocomplex = std::complex<double>(0, 1);
    static_assert( approx_equal( Ocomplex, orientd( 90_deg )));

    // Orientation from units::angle
    units::angle_d angled = units::degrees(60);
    constexpr orient ori60a = angled;
    static_assert( approx_equal( ori60a, orient( units::radians( nin::constants::π/3 ) ), 1 << 0));

    constexpr orientf ori60f = ori60a;
    static_assert( approx_equal( ori60f, static_cast<orientf>(ori60a), 1 << 0));
    static_assert(!approx_equal( static_cast<orientd>(ori60f), ori60a, 1 << 24));
    static_assert( approx_equal( static_cast<orientd>(ori60f), ori60a, 1 << 25));

    // to_angle
    constexpr orient ori0;
    static_assert( approx_equal( ori0.to_angle(                         ) .in(1_deg),    0. , 1 << 0));
    static_assert( approx_equal( ori0.to_angle(                  90_deg ) .in(1_deg),    0. , 1 << 0));
    static_assert( approx_equal( ori0.to_angle(                 180_deg ) .in(1_deg),    0. , 1 << 0));
    static_assert( approx_equal( ori0.to_angle( -179.99999999999999_deg ) .in(1_deg),    0. , 1 << 0));
    // The following test should be -360_deg, but there are rounding errors in to_angle()
    static_assert( approx_equal( ori0.to_angle(                -180_deg ) .in(1_deg),    0. , 1 << 0));
    static_assert(!approx_equal( ori0.to_angle( -180.00000000000001_deg ) .in(1_deg),    0. , 1 << 0));
    static_assert( approx_equal( ori0.to_angle( -180.00000000000001_deg ) .in(1_deg), -360. , 1 << 0));
    static_assert( approx_equal( ori0.to_angle(                -181_deg ) .in(1_deg), -360. , 1 << 0));
    static_assert( approx_equal( ori0.to_angle(                 181_deg ) .in(1_deg),  360. , 1 << 0));

    constexpr orient ori90 = std::complex(0., 90.);
    static_assert( approx_equal( ori90.to_angle().in(1_deg), 90. ));
    static_assert( approx_equal( ori90.to_angle(-91_deg).in(1_deg), -270. ));

}

//_____________________ PLANAR ROTATION _________________________

std::ostream & operator << (std::ostream & sout, nin::planar_rotation const& rot)
{
    return sout <<  "θ=" << rot.angle().SI() << "rad";
}

void test_planar_rotation()
{
    //std::clog << "Testing planar_rotations ..." << " All tests passed at compile time" << '\n';
    std::clog << "Testing planar_rotations ...\n";

    test_semiregular<planar_rotation>();


    units::angle ang5 = 5_rad;
    planar_rotation rot5 (ang5);

    units::angle ang6 = rot5.angle() + 1_rad;
    static_assert( approx_equal(ang6.SI(), 6.));

    constexpr std::array<double, 4> Mrot_null = { 0, 0,
                                                  0, 0 };
    constexpr planar_orientation_quantity ori1 = 1_rad;
    constexpr planar_orientation_quantity ori3 {3_rad};
    constexpr planar_rotation rot2 {ori1, ori3};
    static_assert( approx_equal( rot2, planar_rotation(2_rad) ));
    static_assert(!approx_equal( rot2, planar_rotation(1.99999_rad) ));

    // Transformation of points
    constexpr nin::vector<double, 2> ptd = {14, 16};
    constexpr planar_rotation rot_pt (-270_deg - 360_deg*3);
    static_assert( approx_equal( rot_pt(ptd)[0], -16., 1<<4 ));
    static_assert( approx_equal( rot_pt(ptd)[1],  14., 1<<4 ));
    static_assert(!approx_equal( rot_pt(ptd)[0], -16., 1<<3 )); // Puaj...
    static_assert(!approx_equal( rot_pt(ptd)[1],  14., 1<<3 )); // Puaj...

    constexpr planar_position_quantity ptu = {14_m, 16_cm};
    static_assert( approx_equal( rot_pt(ptu)[0].in(1_cm),  -16., 1 << 11 )); // FIXME: Precision is too low
    static_assert( approx_equal( rot_pt(ptu)[1].in(1_cm), 1400., 0 ));

    // Transformation of orientations
    static_assert( approx_equal( rot_pt(ori1).to_angle().SI(), (1_rad + 90_deg).SI(), 1<<3 ));
    static_assert(!approx_equal( rot_pt(ori1).to_angle().SI(), (1_rad + 90_deg).SI(), 1<<2 ));


    static_assert( approx_equal( inv(inv(rot2)), rot2 ));
    static_assert(!approx_equal( inv(rot2), rot2 ));
    static_assert(!approx_equal( inv(rot2), planar_rotation() ));

    constexpr planar_rotation rot1 {1_deg};
    constexpr planar_rotation rot3 {3_deg};

    static_assert( approx_equal( compose(compose(rot1, inv(rot3)), rot1), inv(rot1) ));
    static_assert( approx_equal( compose(rot1, inv(rot1)), planar_rotation{} ));

    planar_orientation_quantity example_ori = 20_deg;
    planar_rotation example_rot { 896*361_deg };

    // Rotations are as precise as the binary exponent of the rotation: log_2(896) ~ 9.8
    static_assert( approx_equal( inv(example_rot)(example_ori).to_angle().in(1._deg), -156., 1<<11 ));
    static_assert(!approx_equal( inv(example_rot)(example_ori).to_angle().in(1._deg), -156., 1<<10 ));


}

//_____________________ FOOTER test() _________________________

void test()
{
    test_quaternion();
    test_orientation();
    test_rotation();
    test_planar_orientation();
    test_planar_rotation();
}

} //namespace test
