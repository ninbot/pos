/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module;

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>

export module ninbot.pos:euclidean;

export import :units;

namespace nin
{

//________________________________ Euclidian or spatial vector ________________________________

// TODO: Bound vectors, free vectors, zero vector
// TODO: Norm(magnitude), direction (normalized), parallel/orthogonal, dot/cross/triple product, equality

#define NIN_GEOM_T_D_vector template <std::floating_point T, size_t D> vector<T,D>

export
template <std::floating_point Type, size_t Dim>
struct vector : std::array<Type, Dim>
{
    vector & operator += (vector const& rhs);
    vector & operator -= (vector const& rhs);
    vector & operator *= (Type factor);
    vector & operator /= (Type factor);
};


/*** Non-member arithmetic operators ***/

export NIN_GEOM_T_D_vector  operator + (vector<T,D> const& rhs);
export NIN_GEOM_T_D_vector  operator - (vector<T,D> const& rhs);

export NIN_GEOM_T_D_vector  operator + (vector<T,D> const& lhs, vector<T,D> const& rhs);
export NIN_GEOM_T_D_vector  operator - (vector<T,D> const& lhs, vector<T,D> const& rhs);
export NIN_GEOM_T_D_vector  operator * (vector<T,D> const& lhs, T factor              );
export NIN_GEOM_T_D_vector  operator * (T factor              , vector<T,D> const& rhs);
export NIN_GEOM_T_D_vector  operator / (vector<T,D> const& lhs, T factor              );



/*** DEFINITIONS - member arithmetic operators ***/


NIN_GEOM_T_D_vector &
vector<T,D>:: operator += (vector<T,D> const& rhs)
{
    for (uint8_t i = 0; i < D; i++)
        (*this)[i] += rhs[i];

    return *this;
}

NIN_GEOM_T_D_vector &
vector<T,D>:: operator -= (vector<T,D> const& rhs)
{
    return *this += (-rhs);
}

NIN_GEOM_T_D_vector &
vector<T,D>:: operator *= (T factor)
{
    for (T & element : *this)
        element *= factor;

    return *this;
}

NIN_GEOM_T_D_vector &
vector<T,D>:: operator /= (T factor)
{
    return *this *= (1. / factor);
}

/*** DEFINITIONS - Non-member arithmetic operators ***/

NIN_GEOM_T_D_vector
operator + (vector<T,D> const& rhs)
{
    return rhs; // Makes a copy
}

NIN_GEOM_T_D_vector
operator - (vector<T,D> rhs)
{
    vector<T,D> ret;
    std::ranges::transform(rhs, ret.begin(), [](T v){ return -v; });
    return ret;
}


NIN_GEOM_T_D_vector &
operator + (vector<T,D> const& lhs, vector<T,D> const& rhs)
{
    vector<T,D> ret = lhs;
    return ret += rhs;
}

NIN_GEOM_T_D_vector &
operator - (vector<T,D> const& lhs, vector<T,D> const& rhs)
{
    vector<T,D> ret = lhs;
    return ret -= rhs;
}

NIN_GEOM_T_D_vector &
operator * (vector<T,D> const& lhs, T factor)
{
    vector<T,D> ret = lhs;
    return ret *= factor;
}

NIN_GEOM_T_D_vector &
operator * (T factor, vector<T,D> const& rhs)
{
    vector<T,D> ret = rhs;
    return rhs *= factor;
}

NIN_GEOM_T_D_vector &
operator / (vector<T,D> const& lhs, T factor)
{
    vector<T,D> ret = lhs;
    return ret /= factor;
}


#undef NIN_GEOM_T_D_vector



//________________________________ Position quantity ________________________________

template <arithmetic T>
struct basic_position_quantity : std::array<units::length_T<T>, 3>
{
    using arithmetic_type = T;
    using unit_type = units::length_T<T>;

    [[nodiscard]] constexpr unit_type      & x()       noexcept { return (*this)[0]; }
    [[nodiscard]] constexpr unit_type      & y()       noexcept { return (*this)[1]; }
    [[nodiscard]] constexpr unit_type      & z()       noexcept { return (*this)[2]; }
    [[nodiscard]] constexpr unit_type const& x() const noexcept { return (*this)[0]; }
    [[nodiscard]] constexpr unit_type const& y() const noexcept { return (*this)[1]; }
    [[nodiscard]] constexpr unit_type const& z() const noexcept { return (*this)[2]; }
};

export
using position_quantity = basic_position_quantity<double>;


//________________________________ Planar position quantity ________________________________

template <arithmetic T>
struct basic_planar_position_quantity : std::array<units::length_T<T>, 2>
{
    using arithmetic_type = T;
    using unit_type = units::length_T<T>;

    [[nodiscard]] constexpr unit_type      & x()       noexcept { return (*this)[0]; }
    [[nodiscard]] constexpr unit_type      & y()       noexcept { return (*this)[1]; }
    [[nodiscard]] constexpr unit_type const& x() const noexcept { return (*this)[0]; }
    [[nodiscard]] constexpr unit_type const& y() const noexcept { return (*this)[1]; }
};

export
using planar_position_quantity = basic_planar_position_quantity<double>;


//________________________________ Translation ________________________________

template <std::floating_point T>
struct basic_translation : public std::array<units::length_T<T>, 3>
{
    [[nodiscard]] constexpr units::length_T<T> & Δx() noexcept { return (*this)[0]; }
    [[nodiscard]] constexpr units::length_T<T> & Δy() noexcept { return (*this)[1]; }
    [[nodiscard]] constexpr units::length_T<T> & Δz() noexcept { return (*this)[2]; }
    [[nodiscard]] constexpr units::length_T<T> const& Δx() const noexcept { return (*this)[0]; }
    [[nodiscard]] constexpr units::length_T<T> const& Δy() const noexcept { return (*this)[1]; }
    [[nodiscard]] constexpr units::length_T<T> const& Δz() const noexcept { return (*this)[2]; }

    constexpr void invert() noexcept;
    [[nodiscard]] constexpr basic_position_quantity<T> operator ()
                                            (basic_position_quantity<T> const& p) const noexcept;
};

export
template <std::floating_point T> constexpr basic_translation<T> inv
                    ( basic_translation<T> inv_translation ) noexcept;
export
template <std::floating_point T> constexpr basic_translation<T> compose
                    ( basic_translation<T> const& lhs, basic_translation<T> const& rhs ) noexcept;

export
using translation = basic_translation<double>;

/*** Definitions ***/

template <std::floating_point T>
constexpr
void
basic_translation<T>:: invert() noexcept
{
    std::ranges::transform( *this, this->begin(), [](auto & in){ return -in; } );
}

template <std::floating_point T>
constexpr
basic_position_quantity<T>
basic_translation<T>:: operator () (basic_position_quantity<T> const& p) const noexcept
{
    basic_position_quantity<T> retpos = p;
    for (int i = 0; i < this->size(); i++)
        retpos[i] += (*this)[i];
    return retpos;
}

/*** Definitions - Non-member functions ***/


template <std::floating_point T>
constexpr
basic_translation<T>
inv( basic_translation<T> inv_translation ) noexcept
{
    inv_translation.invert();
    return inv_translation;
}

template <std::floating_point T>
constexpr
basic_translation<T>
compose( basic_translation<T> const& lhs,
         basic_translation<T> const& rhs ) noexcept
{
    return basic_translation{{ lhs.Δx() + rhs.Δx(),
                               lhs.Δy() + rhs.Δy(),
                               lhs.Δz() + rhs.Δz() }};
}


//________________________________ Planar translation ________________________________

template <std::floating_point T>
struct basic_planar_translation : public std::array<units::length_T<T>, 2>
{
    [[nodiscard]] constexpr units::length_T<T> & Δx() noexcept { return (*this)[0]; }
    [[nodiscard]] constexpr units::length_T<T> & Δy() noexcept { return (*this)[1]; }
    [[nodiscard]] constexpr units::length_T<T> const& Δx() const noexcept { return (*this)[0]; }
    [[nodiscard]] constexpr units::length_T<T> const& Δy() const noexcept { return (*this)[1]; }

    constexpr void invert() noexcept;
    [[nodiscard]] constexpr basic_planar_position_quantity<T> operator ()
                                    (basic_planar_position_quantity<T> const& p) const noexcept;
};

export
template <std::floating_point T> constexpr basic_planar_translation<T> inv (
                                             basic_planar_translation<T> inv_translation ) noexcept;
export
template <std::floating_point T> constexpr basic_planar_translation<T> compose (
                                                basic_planar_translation<T> const& lhs,
                                                basic_planar_translation<T> const& rhs ) noexcept;

export
using planar_translation = basic_planar_translation<double>;

/*** Definitions ***/

template <std::floating_point T>
constexpr
void
basic_planar_translation<T>:: invert() noexcept
{
    std::ranges::transform( *this, this->begin(), [](auto & in){ return -in; } );
}

template <std::floating_point T>
constexpr
basic_planar_position_quantity<T>
basic_planar_translation<T>:: operator () (basic_planar_position_quantity<T> const& p) const noexcept
{
    basic_planar_position_quantity<T> retpos = p;
    for (int i = 0; i < this->size(); i++)
        retpos[i] += (*this)[i];
    return retpos;
}

/*** Definitions - Non-member functions ***/


template <std::floating_point T>
constexpr
basic_planar_translation<T>
inv( basic_planar_translation<T> inv_translation ) noexcept
{
    inv_translation.invert();
    return inv_translation;
}

template <std::floating_point T>
constexpr
basic_planar_translation<T>
compose( basic_planar_translation<T> const& lhs,
         basic_planar_translation<T> const& rhs ) noexcept
{
    return basic_planar_translation{{ lhs.Δx() + rhs.Δx(),
                                      lhs.Δy() + rhs.Δy() }};
}


//________________________________ --footer-- ________________________________

} // namespace nin

