
#add_library(utils INTERFACE type_ops.hh)
add_library(utils INTERFACE)
target_include_directories(utils INTERFACE .)


install(FILES type_ops.hh
        CONFIGURATIONS Release
	PUBLIC_HEADER
	DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/ninbot)
