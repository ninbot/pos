/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2019-2023  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEST_FRAMEWORK_HH
#define TEST_FRAMEWORK_HH

#include <concepts>
#include <iomanip>
#include <iostream>
#include <type_traits>
#include <vector>

namespace test
{

extern bool verbose;

std::string fill_spaces(unsigned num);

// XXX TODO: Use std::source_location

#define LOG(MSG, TYPE, ...) { std::cerr \
    << __FILE__ << ":" << __LINE__ << ":" << ::test::fill_spaces(__LINE__) \
    << MSG << #TYPE << "(" << #__VA_ARGS__ << ")" << std::endl; }

#define LOG_PASS(...) \
{::test::test_counter++;  if (::test::verbose)   { LOG("TEST OK:     ", __VA_ARGS__) } }

#define LOG_FAIL(...) \
{ ::test::test_counter++; ::test::error_counter++; LOG("TEST FAILED: ", __VA_ARGS__) }


#define ASSERT(...) if (static_cast<bool>(__VA_ARGS__)) \
                        LOG_PASS(ASSERT, __VA_ARGS__) \
                    else \
                        LOG_FAIL(ASSERT, __VA_ARGS__)

class did_not_throw { };
#define ASSERT_THROW(...) \
    try { __VA_ARGS__; throw ::test::did_not_throw { }; } \
    catch (::test::did_not_throw const&) LOG_FAIL(ASSERT_THROW, __VA_ARGS__) \
    catch (...)                          LOG_PASS(ASSERT_THROW, __VA_ARGS__)

extern int test_counter;
extern int error_counter;


bool
operator == (std::vector<double> const& lhs, std::vector<double> const& rhs);


template <typename R>
void test_semiregular()
{
    static_assert(std::semiregular<R>);
}


// Usage: // test_regular<type to be tested>(sample data to initialize R with);
template <typename R, typename T>
void test_regular(T const& data, bool move_destructs_source = true)
{
    static_assert(std::regular<R>);

    const R empty {};
    const R full {data};
    R copy_assign = empty;
    R move_assign {};

    ASSERT(copy_assign == copy_assign);
    ASSERT(full        != copy_assign);

    copy_assign = full;
    ASSERT(copy_assign == full);
    ASSERT(copy_assign != empty);

    const R copy_const{copy_assign};
    ASSERT(copy_const == full);
    ASSERT(copy_const == copy_assign);
    ASSERT(copy_const != empty);

    copy_assign = full;
    ASSERT(copy_assign != empty);
    ASSERT(copy_assign == full);

    move_assign = std::move(copy_assign);
    ASSERT(move_assign == full);
    ASSERT(move_assign != empty);
    if (move_destructs_source) {
        ASSERT(move_assign != copy_assign);
        ASSERT(copy_assign == empty);
    } else {
        ASSERT(move_assign == copy_assign);
        ASSERT(copy_assign != empty);
    }

    const R move_const {std::move(move_assign)};
    ASSERT(move_const == full);
    ASSERT(move_const != empty);
    if (move_destructs_source) {
        ASSERT(move_const  != move_assign);
        ASSERT(move_assign == empty);
    } else {
        ASSERT(move_const  == move_assign);
        ASSERT(move_assign != empty);
    }
}

} // namespace test

#endif // TEST_FRAMEWORK_HH
