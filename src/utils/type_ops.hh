/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2021-2023  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TYPE_UTILS_HH
#define TYPE_UTILS_HH

// FIMXE: Move to tests along with static_asserts
#include <algorithm>
#include <array>
#include <span>
#include <tuple>
#include <variant>
#include <vector>

#include <type_traits>

//_______________________ OPERATIONS ON TYPES: type_ops ______________________||
namespace nin::type_ops {
/*** type_in_pack: Check if Type is one of List ... ***/

template <typename Type, typename ... List>
struct type_in_pack
{
    static constexpr bool value = (std::is_same_v<Type, List> || ... );
};

template <typename ... Params>
constexpr bool type_in_pack_v = type_in_pack<Params ... >::value;



static_assert(type_in_pack_v<char, float, double, unsigned, char>);
static_assert(!type_in_pack_v<int, float, double, unsigned, char>);


/*** add_type to container ***/

template <typename,
          typename>
struct add_type : std::false_type {};

template <typename Type,
          typename ... List,
          template <typename ...> typename Container>
struct add_type <Type, Container<List ... > >
{
    using type = Container<Type, List ... >;
};

template <typename A,
          typename B>
using add_type_t = typename add_type<A, B>::type;

static_assert(std::same_as<std::tuple<char, int, float>,
    add_type<char, std::tuple<int, float> >::type >);
static_assert(std::same_as<std::variant<char, int, float>,
    add_type<char, std::variant<int, float> >::type >);


/*** add_type_if to container ***/

template <bool Test, typename Type, typename Container>
using add_type_if_t
    = typename std::conditional<Test,
                       typename add_type<Type, Container>::type,
                       Container
                >::type;


/*** unique_types: Remove duplicate types and store them in a Container ***/

template <template <typename ...> typename Container,
          typename Type,
          typename ... More>
struct unique_types
{
    using type = add_type_if_t <
            !type_in_pack_v<Type, More ...>,
            Type,
            typename unique_types<Container, More ... >::type
    >;
};

template <template <typename ...> typename Container,
          typename Type>
struct unique_types<Container, Type>
{
    using type = Container<Type>;
};

template <template <typename ...> typename Container,
          typename ... More>
using unique_types_t = typename unique_types<Container, More ...>::type;



// FIXME: Move to test file
static_assert(std::same_as< std::variant<int,char,double>,
        unique_types<std::variant, int, char, double>::type >);

static_assert(std::same_as< std::variant<int,char,double>,
        unique_types<std::variant, int, char, char, double>::type >);

static_assert(!std::same_as< std::variant<int,char,double>,
        unique_types<std::variant, int, char, int, double>::type >);

static_assert(std::same_as< std::variant<int,char,double>,
        unique_types<std::variant, char, int, char, double>::type >);

static_assert(std::same_as< std::variant<int,char,double>,
        unique_types<std::variant, int, char, double, double, double>::type >);


/*** get: Get type of element at index from a list of types ***/

template <size_t index, auto t, auto ... ttt>
struct get
{
    using type = typename get<index - 1, ttt ...>::type;
};

template <auto t, auto ... ttt>
struct get <0, t, ttt ... >
{
    using type = decltype(t);
};


/*** template_of ***/

template <template <typename ...> typename Template,
          typename Specialization>
constexpr bool is_template_of = false;

template <template <typename ...> typename T,
          typename ... List>
constexpr bool is_template_of< T, T<List ...> > = true;


/*** is_nontype_template_of ***/

template <template <auto ...> typename Template,
          typename Specialization>
constexpr bool is_nontype_template_of = false;

template <template <auto ...> typename T,
          auto ... List>
constexpr bool is_nontype_template_of< T, T<List ...> > = true;


/*** for_each_type() ***/

template<size_t   I = 0,
         template <typename ...> typename Container,
         typename Function,
         typename ... Tp>
void
for_each_type(Container< Tp ... > & tuple, Function &&f)
{
    f(std::get<I>(tuple));

    if constexpr(I+1 < sizeof...(Tp))
        for_each<I+1>(tuple, std::forward<Function>(f));
}

template<size_t   I = 0,
         template <typename ...> typename Container,
         typename Function,
         typename ... Tp>
void
for_each_type(Container< Tp ... > const& tuple, Function &&f)
{
    f(std::get<I>(tuple));

    if constexpr(I+1 < sizeof...(Tp))
        for_each<I+1>(tuple, std::forward<Function>(f));
}

//________________________________ SEQUENCE CONTAINERS - Used in clouds ________________________________

template <typename T, size_t N>
struct sequence_container
{
    using type = std::array< T, N >;
};

template <typename T>
struct sequence_container<T, std::dynamic_extent>
{
    using type = std::vector< T >;
};

template <typename T, size_t N>
using sequence_container_t = sequence_container<T,N>::type;

//_________________ end footer ___________________

} // namespace nin::type_ops

#endif
