/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
module;

#include <array>
#include <memory>
#include <tuple>
#include "utils/type_ops.hh"


export module ninbot.pos:d_h;

import :coordsys;
import :spatial;
import :clifford;
import :units;


/* Split convention   |   C++ index
 * ---------------------------------
 *     0 (base)           implicit
 *        1                  0
 *        2                  1
 *        3                  2
 *       ...                ...
 *        N                 N-1
 *
 * The base (=link 0) is implicit because a conversion to index i implies 'from base'
 *
 */

namespace nin {


//________________________________ TYPES ________________________________


/*** index conventions ***/
export
enum struct DH_index_convention
{
    original,
    distal,
    proximal,
    split
};


/*** Parameters ***/

export
enum joint_type
{
    revolute_joint,
    prismatic_joint
};

export
struct DH_quadruplet
{
    length d;
    angle  θ;
    length a;
    angle  α;

    joint_type type;
};

/*** DH_backend_data ***/

template< size_t N >
struct DH_backend_data
{
    constexpr size_t size() const {return N;}
    DH_index_convention       notation;
    std::array<double, 4*N>   raw_parameters_SI;
    std::array<joint_type, N> joint_types;
};

template <typename TF>
concept DH_bd = type_ops::is_nontype_template_of<DH_backend_data, TF>;


/*** extract joint_types from DH_backend_data ***/

// Past-the-end joint
template < size_t N, DH_backend_data<N> data, size_t index = 0, typename ... joint_type_list>
struct tuple_of_joint_types
{
    using type = std::tuple<joint_type_list ...>;
};

// Revolute joint
template < size_t N, DH_backend_data<N> data, size_t index, typename ... joint_type_list>
requires (index < N && data.joint_types[index] == revolute_joint)
struct tuple_of_joint_types< N, data, index, joint_type_list ... >
{
    using type = tuple_of_joint_types<N, data, index + 1, joint_type_list ... , angle>::type;
};

// Prismatic joint
template < size_t N, DH_backend_data<N> data, size_t index, typename ... joint_type_list>
requires (index < N && data.joint_types[index] == prismatic_joint)
struct tuple_of_joint_types< N, data, index, joint_type_list ... >
{
    using type = tuple_of_joint_types<N, data, index + 1, joint_type_list ... , length>::type;
};


/*** DH_table ***/

export
template <size_t N>
struct DH_table
{
    DH_index_convention index_convention;
    std::array<DH_quadruplet, N> parameters;

    static constexpr size_t length = N;

    // TODO: In the split convention, check that parallel axes are well formed.

    constexpr DH_backend_data<N> structural () const;
};


template <size_t N>
constexpr
DH_backend_data<N>
DH_table<N>:: structural () const
{
    DH_backend_data<N> retval {index_convention};
    for (size_t i = 0; i < N; i++)
    {
        retval.raw_parameters_SI[4*i + 0] = parameters[i].d.SI();
        retval.raw_parameters_SI[4*i + 1] = parameters[i].θ.SI();
        retval.raw_parameters_SI[4*i + 2] = parameters[i].a.SI();
        retval.raw_parameters_SI[4*i + 3] = parameters[i].α.SI();
        retval.joint_types[i] = parameters[i].type;
    }
    return retval;
}

//________________________________ BACKEND ________________________________

template< DH_bd auto TF, size_t to >
class position_coordsys_denavit_hartenberg_link_backend;
export template< DH_bd auto TF> class position_coordsys_DH;

template< DH_bd auto TF >
class position_coordsys_denavit_hartenberg_backend
                                        : public coordsys_backend< position_coordsys_class >
{
private: // Transformations
    static /* FIXME C++26: consteval */ std::array<rigid_body_tf, TF.size()> make_neutral_pose_tf();

public: // Types and constants
    constexpr static size_t N = TF.size();
    using joint_values = tuple_of_joint_types<N, TF>::type;

private: // Data members
    position_coordsys const parent;

    static /* FIXME C++26: constexpr */ const std::array<rigid_body_tf, TF.size()> N_i;

    joint_values q_i; // Always call update_cache() after modifying q_i.
    std::array<rigid_body_tf, N+1> B_bj_cache; // j=base..N-1
    template <size_t index = 0> void update_cache();

public: // Constructors
    position_coordsys_denavit_hartenberg_backend( position_coordsys const& ParentCS_arg,
                                                  joint_values      const& initial_pose = {});

public: // Virtual overrides
    [[nodiscard]] rigid_body_tf offset() const override;
    [[nodiscard]] std::shared_ptr<coordsys_backend<position_coordsys_class>> clone() const override;


private: // Friends
    template< DH_bd auto TFu, size_t to >
    friend class position_coordsys_denavit_hartenberg_link_backend;

    friend class position_coordsys_DH<TF>;
};


//________________________________ COORDSYS ________________________________

export
template< DH_bd auto TF >
class position_coordsys_DH : public position_coordsys
{
public:
    using joint_values = position_coordsys_denavit_hartenberg_backend<TF>::joint_values;

    position_coordsys_DH();

    position_coordsys_DH(position_coordsys const& ParentCS_arg);
    position_coordsys_DH(WCS_t);

    template<size_t index> void joint(std::tuple_element_t<index, joint_values> q_i);
    template<size_t index> std::tuple_element_t<index, joint_values> const& joint() const;
    void joints(joint_values q);
    joint_values const& joints() const;

    template< size_t IDX = 0 > position_coordsys link(unsigned index) const;
};


//________________________________ LINK_COORDSYS_BACKEND ________________________________
/* Intermediate links of a Denavit-Hartenberg kinematic chain.
 * Depends on position_coordsys_denavit_hartenberg_backend.
 */
template< DH_bd auto TF, size_t to >
class position_coordsys_denavit_hartenberg_link_backend
                                        : public coordsys_backend< position_coordsys_class >
{
    static_assert (to < position_coordsys_denavit_hartenberg_backend<TF>::N);

    using linked_type = std::shared_ptr<position_coordsys_denavit_hartenberg_backend<TF>>;

    linked_type const linked_chain;

public:
    position_coordsys_denavit_hartenberg_link_backend(linked_type const& linked_chain_arg)
      : linked_chain{linked_chain_arg} { }

public:
    [[nodiscard]] rigid_body_tf offset() const override;
    [[nodiscard]] std::shared_ptr<coordsys_backend<position_coordsys_class>> clone() const override;
};

//________________________________ BACKEND - Definitions ________________________________

/*** Static data members ***/

template< DH_bd auto TF >
std::array<rigid_body_tf, TF.size()> const
position_coordsys_denavit_hartenberg_backend<TF>:: N_i
    = position_coordsys_denavit_hartenberg_backend::make_neutral_pose_tf();

/*** Definitions - Constructors ***/
template< DH_bd auto TF >
position_coordsys_denavit_hartenberg_backend<TF>::
    position_coordsys_denavit_hartenberg_backend( position_coordsys const& ParentCS_arg,
                                                  joint_values      const& initial_pose )
  : parent{ ParentCS_arg.linked_copy() }
  , q_i{ initial_pose }
{
    update_cache();
}

/*** Definitions - Transformations ***/

template< DH_bd auto TF >
// FIXME C++26: consteval
std::array<rigid_body_tf, TF.size()>
position_coordsys_denavit_hartenberg_backend<TF>:: make_neutral_pose_tf()
{
    using namespace units::literals;

    std::array<rigid_body_tf, N> retvec;

    for (int i = 0; i < N; i++)
    {
        length d = units::metres (TF.raw_parameters_SI[4*i + 0]);
        angle  θ = units::radians(TF.raw_parameters_SI[4*i + 1]);
        length a = units::metres (TF.raw_parameters_SI[4*i + 2]);
        angle  α = units::radians(TF.raw_parameters_SI[4*i + 3]);
        rigid_body_tf z_axis {{0_m, 0_m, d}, {{0, 0, 1}, θ} };
        rigid_body_tf x_axis {{a, 0_m, 0_m}, {{1, 0, 0}, α} };
        retvec[i] = compose(z_axis, x_axis);
    }
    return retvec;
}

/*** Definitions - Overrides ***/

template< DH_bd auto TF >
[[nodiscard]]
rigid_body_tf
position_coordsys_denavit_hartenberg_backend<TF>:: offset() const
{
    return position_coordsys_class::compose_tf( parent.offset(), B_bj_cache[N-1]);
}

template< DH_bd auto TF >
[[nodiscard]]
std::shared_ptr< coordsys_backend<position_coordsys_class> >
position_coordsys_denavit_hartenberg_backend<TF>:: clone() const
{
    return std::make_shared<position_coordsys_denavit_hartenberg_backend>(*this);
}

/*** Definitions - Other member functions ***/

template< DH_bd auto TF >
template <size_t index>
void
position_coordsys_denavit_hartenberg_backend<TF>:: update_cache()
{
    if constexpr (index < N)
    {
        rigid_body_tf J;
        if constexpr (TF.joint_types[index] == revolute_joint)
            J = rigid_body_tf{ {}, {{0, 0, 1}, std::get<index>(q_i)} };
        else
            J = rigid_body_tf{ {0_m, 0_m, std::get<index>(q_i)}, {} };

        rigid_body_tf B;
        //TODO: if constexpr (TF.notation == DH_index_convention::split)
            B = compose( N_i[index], J );
        //TODO: else
            //TODO: return compose(  compose( joint, N_i[from] ), B_bj<from+1, to>()  );

        rigid_body_tf B_prev;
        if constexpr (index > 0)
            B_prev = B_bj_cache[index - 1];

        B_bj_cache[index] = compose(B_prev, B);

        update_cache<index+1>();
    }
}


//________________________________ COORDSYS - Definitions ________________________________

template< DH_bd auto TF >
position_coordsys_DH<TF>:: position_coordsys_DH()
  : position_coordsys_DH(nin::WCS)
{ }

template< DH_bd auto TF >
position_coordsys_DH<TF>:: position_coordsys_DH(position_coordsys const& ParentCS_arg)
  : coordsys< position_coordsys_class >
        { std::make_shared<position_coordsys_denavit_hartenberg_backend<TF>>(ParentCS_arg) }
{ }

template< DH_bd auto TF >
position_coordsys_DH<TF>:: position_coordsys_DH(WCS_t)
  : position_coordsys_DH(position_coordsys{})
{ }


template< DH_bd auto TF >
template<size_t index>
void
position_coordsys_DH<TF>:: joint(std::tuple_element_t<index, joint_values> q_i)
{
    auto ptr = static_cast<position_coordsys_denavit_hartenberg_backend<TF> *>(backend.get());
    std::get<index>(ptr->q_i) = q_i;
    ptr->update_cache();
}

template< DH_bd auto TF >
template<size_t index>
std::tuple_element_t<index, typename position_coordsys_DH<TF>::joint_values> const&
position_coordsys_DH<TF>:: joint() const
{
    auto ptr = static_cast<position_coordsys_denavit_hartenberg_backend<TF> *>(backend.get());
    return std::get<index>(ptr->q_i);
}

template< DH_bd auto TF >
void
position_coordsys_DH<TF>:: joints(joint_values q)
{
    auto ptr = static_cast<position_coordsys_denavit_hartenberg_backend<TF> *>(backend.get());
    ptr->q_i = q;
    ptr->update_cache();
}

template< DH_bd auto TF >
typename position_coordsys_DH<TF>::joint_values const&
position_coordsys_DH<TF>:: joints() const
{
    auto ptr = static_cast<position_coordsys_denavit_hartenberg_backend<TF> *>(backend.get());
    return ptr->q_i;
}


template< DH_bd auto TF >
template< size_t IDX >
position_coordsys
position_coordsys_DH<TF>:: link(unsigned index) const
{
    if constexpr (IDX >= position_coordsys_denavit_hartenberg_backend<TF>::N)
        throw std::domain_error("index is too big");

    else if (IDX == index)
        return position_coordsys(
                coordsys_generic<position_coordsys_denavit_hartenberg_link_backend<TF, IDX>>(
                static_pointer_cast<position_coordsys_denavit_hartenberg_backend<TF>>(backend)) );
    else
        return link< IDX+1 >(index);
}


//_____________________________ LINK_COORDSYS_BACKEND - Definitions _____________________________

template< DH_bd auto TF, size_t to >
[[nodiscard]]
rigid_body_tf
position_coordsys_denavit_hartenberg_link_backend<TF, to>:: offset() const
{
    return linked_chain->B_bj_cache[to];
}

template< DH_bd auto TF, size_t to >
[[nodiscard]]
std::shared_ptr< coordsys_backend<position_coordsys_class> >
position_coordsys_denavit_hartenberg_link_backend<TF, to>:: clone() const
{
    return std::make_shared<position_coordsys_denavit_hartenberg_link_backend>(*this);
}

//_______________________ footer _______________________

} // namespace nin
