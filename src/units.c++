/* The POS Library - a highly customisable coordinate system library for C++
 * Copyright (C) 2022-2024  Francisco Jesús Arjonilla García
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* Units: A library for physical units.
 * See accompanying README file for more information about this library.  */

module;

#include <array>
#include <chrono>
#include <cmath>
#include <type_traits>

export module ninbot.pos:units;

/* Table of operations between values (absolute magnitudes) and transformations (relative)
 *
 *       Domain1       operation     Domain2         Result
 *
 *       Value         difference    Value           Transformation
 *       Transf.       Transform     Value           Value
 *       Transf.       Componer      Transf.         Transf.
 *
 *       Highly beneficial for distinguishing between absolute termperatures and differences.
 */

/* TODO:
 * - Tests
 * - Absolute temperatures and temperature changes
 * - Geometric functions on units typically manipulated with vectors:
 *      angle, length, speed, acceleration, force, etc.
 * - Check interoperability with std::chrono
 */

namespace nin {


template <typename T>
concept arithmetic = std::is_arithmetic<T>::value;

//__________________ MATH CONSTANTS ____________________
inline namespace constants {

// 50 first digits of π. (π is unicode \u03c0)
export
constexpr std::floating_point auto π = 3.1415926535897932384626433832795028841971693993751L;

// 50 first digits of Euler's number ℯ. (ℯ is unicode \u212f)
// Careful! Note that the latin 'e' character is reserved by the elementary charge.
export
constexpr std::floating_point auto ℯ = 2.71828182845904523536028747135266249775724709369996L;

}

//______________ DIMENSION _______________

inline namespace units {
/*** struct dimension ***/
export
struct dimension
{
    int8_t α  = 0; // T = _s
    int8_t β  = 0; // L = _m
    int8_t γ  = 0; // M = _kg
    int8_t δ  = 0; // I = _A
    int8_t ε  = 0; // Θ = _K (Absolute temperature)
    int8_t ζ  = 0; // N = _mol
    int8_t η  = 0; // J = _cd

    // Implementation extras
    int8_t Δε = 0; // Θ = _K (Differential temperature)
    int8_t ρ  = 0; // angle = _rad

    // TODO: - User-defined literals, factories, aliases for differential temperature
    //       - Adimensional units: counts/revolutions/cycles/bits/bytes/bauds/ratios
    //         https://en.wikipedia.org/wiki/List_of_dimensionless_quantities

    constexpr
    dimension &
    operator *= (dimension const& rhs) noexcept
    {
        α += rhs.α;
        β += rhs.β;
        γ += rhs.γ;
        δ += rhs.δ;
        ε += rhs.ε;
        ζ += rhs.ζ;
        η += rhs.η;
        return *this;
    }

    constexpr
    dimension &
    operator /= (dimension const& rhs) noexcept
    {
        α -= rhs.α;
        β -= rhs.β;
        γ -= rhs.γ;
        δ -= rhs.δ;
        ε -= rhs.ε;
        ζ -= rhs.ζ;
        η -= rhs.η;
        return *this;
    }

    constexpr
    bool
    operator == (dimension const& rhs) const noexcept = default;

};


/*** Non-member function definitions ***/

constexpr
dimension
operator * (dimension lhs, dimension const& rhs) noexcept
{
    return lhs *= rhs;
}

constexpr
dimension
operator / (dimension lhs, dimension const& rhs) noexcept
{
    return lhs /= rhs;
}

//TODO:
//std::string format(value)
//{
//    constexpr char superidx[] = "⁰¹²³⁴⁵⁶⁷⁸⁹";
//    constexpr char minus_sign = '⁻';
//    constexpr char center_dot = '·';
//    18.7·10⁻² m²·s⁻¹
//}


//____________________________ VALUE ____________________________

/*** Forward friend template declarations ***/
export
template <dimension D, std::floating_point T = double>
class value;

template <dimension D, std::floating_point T>
constexpr
value<D, T> value_from_magnitude_SI(T magnitude);

/*** class value ***/
export
template <dimension D, std::floating_point T>
class value
{
/*** Private invariant ***/
private:
    // Always SI - International System of Units
    T magnitude { 0 };

/*** Types ***/
public:
    constexpr const static dimension dimensions = D;
    using floating_point_type = T;

/*** Constructors ***/
public:
    // Force constexpr constructors
    constexpr value() noexcept = default;
    constexpr value(value const& copy) noexcept = default;
    constexpr value & operator = (value const& copy) noexcept = default;

private:
    // This constructor is for exclusive use by value_from_magnitude_SI().
    explicit
    constexpr value(T magn) noexcept : magnitude {magn} { }

/*** Friends ***/
private:
    // All value types are friends
    template <dimension, std::floating_point>
    friend class value;

    constexpr
    friend
    value value_from_magnitude_SI<D, T>(T magnitude);


/*** Boolean comparisons ***/
public:
    constexpr std::partial_ordering operator <=> (value const&) const = default;

/*** Binary operators with other values  +=  -=   ***/
public:
    template <dimension D2, std::floating_point T2>
        constexpr
        value &
        operator += (value<D2, T2> const& rhs) noexcept
        {
            static_assert(D == D2, "Inconsistent units");
            magnitude += rhs.magnitude;
            return *this;
        }

    template <dimension D2, std::floating_point T2>
        constexpr
        value &
        operator -= (value<D2, T2> const& rhs) noexcept
        {
            static_assert(D == D2, "Inconsistent units");
            magnitude -= rhs.magnitude;
            return *this;
        }

        // ¿? operator *= (value<D2, T2> rhs) = delete;
        // ¿? operator /= (value<D2, T2> rhs) = delete;

/*** Binary operators with dimension change  *  /   ***/

    template <dimension D2, std::floating_point T2>
        constexpr
        value<operator * (D, D2), decltype(T() * T2())>
        operator * (value<D2, T2> const& rhs) const noexcept
        {
            return value<D * D2, decltype(T() * T2())>{ magnitude * rhs.magnitude };
        }

    template <dimension D2, std::floating_point T2>
        constexpr
        value<operator / (D, D2), decltype(T() / T2())>
        operator / (value<D2, T2> const& rhs) const noexcept
        {
            return value<D / D2, decltype(T() / T2())>{ magnitude / rhs.magnitude };
        }


/*** Binary operators with scalars  *=  /=   ***/
    constexpr
        value
        operator *= (T rhs) noexcept
        {
            magnitude *= rhs;
            return *this;
        }

    constexpr
        value
        operator /= (T rhs) noexcept
        {
            magnitude /= rhs;
            return *this;
        }

/*** Underlying-type conversion operators ***/
    template <std::floating_point T2>
        constexpr
        operator value<D, T2> () noexcept
        {
            return value<D, T2>(magnitude);
        }


/*** Conversion between unit systems ***/
protected:
    static consteval
        T convert_factor(std::array<double, 7> factors) noexcept
        {
            int dimensions[7] = { D.α, D.β, D.γ, D.δ, D.ε, D.ζ, D.η };

            T retval = 1.;
            for (int i = 0; i < 7; i++)
            {
                while (dimensions[i]-- > 0)
                    retval /= factors[i];

                while (dimensions[i]++ < 0)
                    retval *= factors[i];
            }
            return retval;
        }


/*** Projection of dimension ***/
// Useful for declaration of unit types from unit literals, where the magnitude is ignored.
// value<1_mm> length = 28_µm
public:
    constexpr
        operator dimension () const noexcept
        {
            return dimensions;
        }

/*** Conversion to underlying fundamental type ***/
public:

    constexpr
        T
        SI() const noexcept
        {
            return magnitude;
        }

    constexpr
        T
        CGS() const noexcept
        {
            return magnitude * convert_factor( {
                        1.L, // _s
                    10.e-3L, // _cm
                     1.e-3L, // _g
                        1.L,
                        1.L,
                        1.L,
                        1.L
            } );
        }

    template <dimension D2, std::floating_point T2>
        constexpr
        T
        in(value<D2, T2> target_unit) const noexcept(!std::is_constant_evaluated())
        {
            static_assert(target_unit.dimensions == D, "Incompatible units requested");
            if constexpr (std::is_constant_evaluated())
                if (!std::isnormal(target_unit.magnitude))
                    throw std::runtime_error("nin::units::value::in(): "
                                             "Magnitude of target unit is invalid");
            #ifndef NDEBUG
                if (!std::isnormal(target_unit.magnitude))
                    throw std::runtime_error("nin::units::value::in(): "
                                             "Magnitude of target unit is invalid");
            #endif

            return magnitude / target_unit.magnitude;
        }


/*** Dimension-specific functions - Adimensional ***/
    constexpr
        operator T () const
        requires ( dimensions == dimension{} ) // TODO: static assert to help debug user code?
        {
            return magnitude;
        }

/*** Dimension-specific functions - Temperature ***/
    constexpr
        T
        degC() const noexcept
        requires ( dimension { 0, 0, 0, 0, 1, 0, 0 } == D )
        {
            return magnitude - 273.15L;
        }
    constexpr
        T
        degF() const noexcept
        requires ( dimension { 0, 0, 0, 0, 1, 0, 0 } == D )
        {
            return magnitude * (9.L / 5.L)  - 459.67L;
        }

/*** Dimension-specific functions - Time ***/
    template <typename Rep, typename Period>
        constexpr
        operator std::chrono::duration<Rep, Period> () const
        requires (D == dimension {1,0,0,0,0,0,0})
        {
            Rep count = magnitude * Period::den / Period::num;
            return std::chrono::duration<Rep, Period>(count);
        }

    //TODO: Constructor from std::chrono

/*** Error detection honeypots ***/
    template <dimension D2, std::floating_point T2>
        constexpr
        operator value<D2, T2> () const
        requires ( D2 != D )
        {
            static_assert(D2 == D, "Inconsistent units");
            return {};
        }
};


/*** Non-member unary operators  +  -   ***/


export
template <dimension D, std::floating_point T>
constexpr
value<D, T>
operator + (value<D, T> rhs) noexcept
{
    return rhs;
}

export
template <dimension D, std::floating_point T>
constexpr
value<D, T>
operator - (value<D, T> rhs) noexcept
{
    return value<D,T>{} * 0.L - rhs;
}


/*** Non-member binary operators  +  -  *  /   ***/
export
template <dimension D1, std::floating_point T1, dimension D2, std::floating_point T2>
constexpr
value<D1, decltype(T1() + T2())>
operator + (value<D1, T1> lhs, value<D2, T2> const& rhs) noexcept
{
    return lhs += rhs;
}


export
template <dimension D1, std::floating_point T1, dimension D2, std::floating_point T2>
constexpr
value<D1, decltype(T1() - T2())>
operator - (value<D1, T1> lhs, value<D2, T2> const& rhs) noexcept
{
    return lhs -= rhs;
}


export
template <dimension D, std::floating_point T, arithmetic F>
constexpr
value<D, T>
operator * (value<D, T> lhs, F rhs) noexcept
{
    lhs *= rhs;
    return lhs;
}

export
template <dimension D, std::floating_point T, arithmetic F>
constexpr
value<D, T>
operator * (F lhs, value<D, T> rhs)
{
    rhs *= lhs;
    return rhs;
}

export
template <dimension D, std::floating_point T, arithmetic F>
constexpr
value<D, T>
operator / (value<D, T> lhs, F rhs)
{
    lhs /= rhs;
    return lhs;
}

export
template <dimension D, std::floating_point T, arithmetic F>
constexpr
value<operator / (dimension{}, D), T>
operator / (F lhs, value<D, T> const& rhs)
{
    value<operator / (dimension{}, D), T> lhs_value
        = value_from_magnitude_SI<operator / (dimension{}, D), T>(static_cast<T>(lhs));
    return lhs_value / rhs.SI();
}


/*** Non-member factories ***/


template <dimension D, std::floating_point T>
constexpr
value<D, T> value_from_magnitude_SI(T magnitude)
{
    return value<D, T>{magnitude};
}


//___________________________ LITERALS ______________________________

/*** Macro definitions ***/
// The trailing underscore '_' in '_unit' is added here to support prefixes
// operator""_Symbol must not contain spaces (i.e. operator "" _Symbol) to prevent macro expansion.
#define NINBOT_UNITS_LITERALOPERATOR_RAW(type, dim_1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol) \
inline namespace literals { \
export \
inline \
consteval auto operator "" _##literalsymbol(long double magnitude) noexcept \
{ return value_from_magnitude_SI<dimension{dim_1,d2,d3,d4,d5,d6,d7},type>( magnitude * ratio ); } \
\
export \
inline \
consteval auto operator "" _##literalsymbol(unsigned long long int magnitude) noexcept \
{ return value_from_magnitude_SI<dimension{dim_1,d2,d3,d4,d5,d6,d7},type>( magnitude * ratio ); } \
} /* namespace literals */ \

#define NINBOT_UNITS_UNITFACTORY_RAW_TEMPLATE(dim_1, d2, d3, d4, d5, d6, d7, ratio, factoryname) \
inline namespace factories \
{ \
    export \
    template <std::floating_point T> \
    constexpr \
    value<dimension{dim_1,d2,d3,d4,d5,d6,d7}, T> factoryname##_T (T magnitude) noexcept \
    { \
        return value_from_magnitude_SI<dimension{dim_1,d2,d3,d4,d5,d6,d7}, T> \
                                      (static_cast<T>(ratio) * magnitude); \
    } \
    \
} /* namespace factories */ \

#define NINBOT_UNITS_UNITFACTORY_RAW(type, dim_1, d2, d3, d4, d5, d6, d7, ratio, factoryname) \
inline namespace factories \
{ \
    export \
    constexpr \
    value<dimension{dim_1,d2,d3,d4,d5,d6,d7}, type> factoryname (type magnitude) noexcept \
    { \
        return value_from_magnitude_SI<dimension{dim_1,d2,d3,d4,d5,d6,d7}, type> \
                                      (static_cast<type>(ratio) * magnitude); \
    } \
    \
} /* namespace factories */ \


#define NINBOT_UNITS_UNITALIAS_RAW_TEMPLATE(dim_1, d2, d3, d4, d5, d6, d7, ratio, aliasname) \
inline namespace aliases \
{ \
    export \
    template <std::floating_point T> \
    using aliasname##_T = value<dimension{dim_1,d2,d3,d4,d5,d6,d7}, T>; \
} /* namespace aliases */ \

#define NINBOT_UNITS_UNITALIAS_RAW(type, aliasname, suffix) \
inline namespace aliases \
{ \
    export \
    using aliasname##suffix = aliasname##_T<type>; \
} /* namespace aliases */ \


#define NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol    ) \
NINBOT_UNITS_LITERALOPERATOR_RAW(  float,       d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol##_f) \
NINBOT_UNITS_LITERALOPERATOR_RAW(  float,       d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol##_F) \
NINBOT_UNITS_LITERALOPERATOR_RAW(  double,      d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol    ) \
NINBOT_UNITS_LITERALOPERATOR_RAW(  double,      d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol##_d) \
NINBOT_UNITS_LITERALOPERATOR_RAW(  double,      d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol##_D) \
NINBOT_UNITS_LITERALOPERATOR_RAW(  long double, d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol##_l) \
NINBOT_UNITS_LITERALOPERATOR_RAW(  long double, d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol##_L) \

#define NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(d1, d2, d3, d4, d5, d6, d7, ratio, factoryname    ) \
NINBOT_UNITS_UNITFACTORY_RAW_TEMPLATE(      d1, d2, d3, d4, d5, d6, d7, ratio, factoryname    ) \
NINBOT_UNITS_UNITFACTORY_RAW(  float,       d1, d2, d3, d4, d5, d6, d7, ratio, factoryname##_f) \
NINBOT_UNITS_UNITFACTORY_RAW(  float,       d1, d2, d3, d4, d5, d6, d7, ratio, factoryname##_F) \
NINBOT_UNITS_UNITFACTORY_RAW(  double,      d1, d2, d3, d4, d5, d6, d7, ratio, factoryname    ) \
NINBOT_UNITS_UNITFACTORY_RAW(  double,      d1, d2, d3, d4, d5, d6, d7, ratio, factoryname##_d) \
NINBOT_UNITS_UNITFACTORY_RAW(  double,      d1, d2, d3, d4, d5, d6, d7, ratio, factoryname##_D) \
NINBOT_UNITS_UNITFACTORY_RAW(  long double, d1, d2, d3, d4, d5, d6, d7, ratio, factoryname##_l) \
NINBOT_UNITS_UNITFACTORY_RAW(  long double, d1, d2, d3, d4, d5, d6, d7, ratio, factoryname##_L) \

#define NINBOT_UNITS_UNITALIAS_TYPESUFFIX(d1, d2, d3, d4, d5, d6, d7, ratio, aliasname ) \
NINBOT_UNITS_UNITALIAS_RAW_TEMPLATE(      d1, d2, d3, d4, d5, d6, d7, ratio, aliasname ) \
NINBOT_UNITS_UNITALIAS_RAW(  float,       aliasname, _f) \
NINBOT_UNITS_UNITALIAS_RAW(  float,       aliasname, _F) \
NINBOT_UNITS_UNITALIAS_RAW(  double,      aliasname,   ) \
NINBOT_UNITS_UNITALIAS_RAW(  double,      aliasname, _d) \
NINBOT_UNITS_UNITALIAS_RAW(  double,      aliasname, _D) \
NINBOT_UNITS_UNITALIAS_RAW(  long double, aliasname, _l) \
NINBOT_UNITS_UNITALIAS_RAW(  long double, aliasname, _L) \

#define NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(d1, d2, d3, d4, d5, d6, d7, ratio,                 literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-30L,    q##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-27L,    r##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-24L,    y##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-21L,    z##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-18L,    a##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-15L,    f##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-12L,    p##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-9L ,    n##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-6L ,    µ##literalsymbol ) /* µ is unicode \u00b5. This letter is the Micro sign. */ \
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-6L ,    μ##literalsymbol ) /* Alternative for \u00b5  with unicode \u03bc. This is greek mu. */ \
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-3L ,    m##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-2L ,    c##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-1L ,    d##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e0L  ,       literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e1L  ,   da##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e2L  ,    h##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e3L  ,    k##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e6L  ,    M##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e9L  ,    G##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e12L ,    T##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e15L ,    P##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e18L ,    E##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e21L ,    Z##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e24L ,    Y##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e27L ,    R##literalsymbol )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e30L ,    Q##literalsymbol )\

#define NINBOT_UNITS_UNITFACTORY_RATIOPREFIX(d1, d2, d3, d4, d5, d6, d7, ratio,                   factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-30L, quecto##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-27L,  ronto##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-24L,  yocto##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-21L,  zepto##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-18L,   atto##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-15L,  femto##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-12L,   pico##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-9L ,   nano##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-6L ,  micro##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-3L ,  milli##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-2L ,  centi##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e-1L ,   deci##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e0L  ,         factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e1L  ,   deca##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e2L  ,  hecto##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e3L  ,   kilo##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e6L  ,   mega##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e9L  ,   giga##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e12L ,   tera##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e15L ,   peta##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e18L ,    exa##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e21L ,  zetta##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e24L ,  yotta##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e27L ,  ronna##factoryname )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(         d1, d2, d3, d4, d5, d6, d7, ratio * 1.e30L , quetta##factoryname )\

#define NINBOT_UNITS_GENERATE_ALL(       d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol, factoryname, aliasname )\
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol                         )\
NINBOT_UNITS_UNITFACTORY_RATIOPREFIX(    d1, d2, d3, d4, d5, d6, d7, ratio,                factoryname            )\
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       d1, d2, d3, d4, d5, d6, d7, ratio,                             aliasname )\

#define NINBOT_UNITS_LITERALOPS_UNITALIAS(d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol,             aliasname )\
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol                        )\
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        d1, d2, d3, d4, d5, d6, d7, ratio,                            aliasname )\

#define NINBOT_UNITS_LITRL_FACTR_NORATIO( d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol, factoryname            )\
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(  d1, d2, d3, d4, d5, d6, d7, ratio, literalsymbol                         )\
NINBOT_UNITS_UNITFACTORY_TYPESUFFIX(      d1, d2, d3, d4, d5, d6, d7, ratio,                factoryname            )\


/*** Base and special derived units ***/

// Section 1.3.1: SI Base units      T  L  M  I  Θ  N  J
NINBOT_UNITS_GENERATE_ALL( 1, 0, 0, 0, 0, 0, 0,    1.L,  s,   seconds,  time                )
NINBOT_UNITS_GENERATE_ALL( 0, 1, 0, 0, 0, 0, 0,    1.L,  m,   metres,   length              )
NINBOT_UNITS_GENERATE_ALL( 0, 0, 1, 0, 0, 0, 0, 1.e-3L,  g,   grams,    mass                )
NINBOT_UNITS_GENERATE_ALL( 0, 0, 0, 1, 0, 0, 0,    1.L,  A,   amperes,  electric_current    )
NINBOT_UNITS_GENERATE_ALL( 0, 0, 0, 0, 1, 0, 0,    1.L,  K,   kelvins,  temperature         )
NINBOT_UNITS_GENERATE_ALL( 0, 0, 0, 0, 0, 1, 0,    1.L,  mol, mols,     amount_of_substance )
NINBOT_UNITS_GENERATE_ALL( 0, 0, 0, 0, 0, 0, 1,    1.L,  cd,  candelas, luminous_intensity  )

// Include factory for consistency
template <std::floating_point T = double>
consteval
auto
degK (long double magn)
{
    value<dimension{0, 0, 0, 0, 1, 0, 0}, T> retval;
    return retval * magn;
}

// Section 2.3.4 Table 4: Derived units with special names and symbols
NINBOT_UNITS_GENERATE_ALL(                 0,  0,  0,  0,  0,  0,  0, 1.L,  rad,   radians,    angle                   ) // m/m
NINBOT_UNITS_GENERATE_ALL(                 0,  0,  0,  0,  0,  0,  0, 1.L,  sr,    steradians, solid_angle             ) // m2/m2
NINBOT_UNITS_GENERATE_ALL(                -1,  0,  0,  0,  0,  0,  0, 1.L,  Hz,    hertzs,     frequency               )
NINBOT_UNITS_GENERATE_ALL(                -2,  1,  1,  0,  0,  0,  0, 1.L,  N,     newtons,    force                   )
NINBOT_UNITS_GENERATE_ALL(                -2, -1,  1,  0,  0,  0,  0, 1.L,  Pa,    pascals,    pressure                )
NINBOT_UNITS_GENERATE_ALL(                -2,  2,  1,  0,  0,  0,  0, 1.L,  J,     joules,     energy                  )
#if 0
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( -2,  2,  1,  0,  0,  0,  0, 1.L,  Nm                                         )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        -2,  2,  1,  0,  0,  0,  0, 1.L,                     work                    )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        -2,  2,  1,  0,  0,  0,  0, 1.L,                     amount_of_heat          )
NINBOT_UNITS_GENERATE_ALL(                -3,  2,  1,  0,  0,  0,  0, 1.L,  W,     watts,      power                   )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( -3,  2,  1,  0,  0,  0,  0, 1.L,  J_s                                        )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        -3,  2,  1,  0,  0,  0,  0, 1.L,                     radiant_flux            )
NINBOT_UNITS_GENERATE_ALL(                 1,  0,  0,  1,  0,  0,  0, 1.L,  C,     coulombs,   electric_charge         )
NINBOT_UNITS_GENERATE_ALL(                -3,  2,  1, -1,  0,  0,  0, 1.L,  V,     volts,      electric_potential_diff )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        -3,  2,  1, -1,  0,  0,  0, 1.L,                     voltage                 )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        -3,  2,  1, -1,  0,  0,  0, 1.L,                     electric_tension        )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        -3,  2,  1, -1,  0,  0,  0, 1.L,                     tension                 )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( -3,  2,  1, -1,  0,  0,  0, 1.L,  W_A                                        )
NINBOT_UNITS_GENERATE_ALL(                 4, -2, -1,  2,  0,  0,  0, 1.L,  F,     farads,     capacitance             )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  4, -2, -1,  2,  0,  0,  0, 1.L,  C_V                                        )
//NINBOT_UNITS_GENERATE_ALL(              -3,  2,  1, -2,  0,  0,  0, 1.L,  Ω,     ohms,       electric_resistance     )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( -3,  2,  1, -2,  0,  0,  0, 1.L,  V_A                                        )
NINBOT_UNITS_GENERATE_ALL(                 3, -2, -1,  2,  0,  0,  0, 1.L,  S,     siemens,    electric_conductance    )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  3, -2, -1,  2,  0,  0,  0, 1.L,  A_V                                        )
NINBOT_UNITS_GENERATE_ALL(                -2,  2,  1, -1,  0,  0,  0, 1.L,  Wb,    webers,     magnetic_flux           )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( -2,  2,  1, -1,  0,  0,  0, 1.L,  V_s                                        )
NINBOT_UNITS_GENERATE_ALL(                -2,  0,  1, -1,  0,  0,  0, 1.L,  T,     teslas,     magnetic_flux_density   )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( -2,  0,  1, -1,  0,  0,  0, 1.L,  Wb_m2                                      )
NINBOT_UNITS_GENERATE_ALL(                -2,  2,  1, -2,  0,  0,  0, 1.L,  H,     henries,    inductance              )
NINBOT_UNITS_UNITFACTORY_RATIOPREFIX(     -2,  2,  1, -2,  0,  0,  0, 1.L,         henrys                              )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( -2,  2,  1, -2,  0,  0,  0, 1.L,  Wb_A                                       )
//NINBOT_UNITS_GENERATE_ALL(               0, 0,  0,  0,  0,  0,  0, 1.L,   ℃,     deg. Celsius,temperature            )  // See specialization below
NINBOT_UNITS_GENERATE_ALL(                 0,  0,  0,  0,  0,  0,  1, 1.L,  lm,    lumens,     luminous_flux           )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  0,  0,  0,  0,  0,  0,  1, 1.L,  cd_sr                                      )
NINBOT_UNITS_GENERATE_ALL(                 0, -2,  0,  0,  0,  0,  1, 1.L,  lux,   luxes,      illuminance             )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  0, -2,  0,  0,  0,  0,  1, 1.L,  lm_m2                                      )
NINBOT_UNITS_GENERATE_ALL(                -1,  0,  0,  0,  0,  0,  0, 1.L,  Bq,    becquerels, radionuclide_activity   )
NINBOT_UNITS_GENERATE_ALL(                -2,  2,  0,  0,  0,  0,  0, 1.L,  Gy,    grays,      absorbed_dose           )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        -2,  2,  0,  0,  0,  0,  0, 1.L,                     kerma                   )
NINBOT_UNITS_GENERATE_ALL(                -2,  2,  0,  0,  0,  0,  0, 1.L,  Sv,    sieverts,   dose_equivalent         )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX( -2,  2,  0,  0,  0,  0,  0, 1.L,  J_kg                                       )
NINBOT_UNITS_GENERATE_ALL(                -1,  0,  0,  0,  0,  1,  0, 1.L,  kat,   katals,     catalytic_activity      )
#endif

// Degrees Centigrade. Unicode character ℃ \u2103 is not XID_Continue.
// 0 degC is exactly 273.15_K.
// Note that this is different from the previous definition of temperature of
// water fusion, which is 273.16 +- 3.7e-7 _K as of SI 2019 9th edition
template <std::floating_point T = double>
consteval
auto
degC (long double magn)
{
    value<dimension{0, 0, 0, 0, 1, 0, 0}, T> retval;
    return retval * (magn + 273.15L);
}

/*** Derived units ***/

// Section 2.3.4 Table 5: Derived units expressed in terms of base SI units

NINBOT_UNITS_LITERALOPS_UNITALIAS(        0,  2,  0,  0,  0,  0,  0,  1.L,      m2,         area                              ) // A
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0,  3,  0,  0,  0,  0,  0,  1.L,      m3,         volume                            ) // C
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -1,  1,  0,  0,  0,  0,  0,  1.L,     m_s,         speed                             ) // v
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -1,  1,  0,  0,  0,  0,  0,  1.L,                  velocity                          )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2,  1,  0,  0,  0,  0,  0,  1.L,    m_s2,         acceleration                      ) // a
#if 0
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0, -1,  0,  0,  0,  0,  0,  1.L,     1_m,         wavenumber                        ) // σ
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0, -3,  1,  0,  0,  0,  0,  1.L,   kg_m3,         density                           ) // ρ
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        0, -3,  1,  0,  0,  0,  0,  1.L,                  mass_density                      )
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0, -2,  1,  0,  0,  0,  0,  1.L,   kg_m2,         surface_density                   ) // ρ_A
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0,  2, -1,  0,  0,  0,  0,  1.L,   m2_kg,         specific_volume                   ) // υ
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0, -2,  0,  1,  0,  0,  0,  1.L,    A_m2,         current_density                   ) // j
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0, -1,  0,  1,  0,  0,  0,  1.L,     A_m,         magnetic_field_strength           ) // H
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0, -3,  0,  0,  0,  1,  0,  1.L,  mol_m3,         amount_of_substance_concentration ) // c
NINBOT_UNITS_LITERALOPS_UNITALIAS(        0, -2,  0,  0,  0,  0,  1,  1.L,   cd_m2,         luminance                         ) // L_v
#endif


// Section 2.3.4 Table 6: Derived units expressed in terms of other derived SI units
#if 0
NINBOT_UNITS_GENERATE_ALL(               -1, -1,  1,  0,  0,  0,  0,  1.L,     Pas, pascal, dynamic_viscosity                 )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -2,  2,  1,  0,  0,  0,  0,  1.L,                  moment_of_force                   ) //(Same as Joule)
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2,  0,  1,  0,  0,  0,  0,  1.L,     N_m,         surface_tension                   )
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX( -1,  0,  0,  0,  0,  0,  0,  1.L,   rad_s                                            )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -1,  0,  0,  0,  0,  0,  0,  1.L,                  angular_velocity                  )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -1,  0,  0,  0,  0,  0,  0,  1.L,                  angular_frequency                 )
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX( -2,  0,  0,  0,  0,  0,  0,  1.L,  rad_s2                                            )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -2,  0,  0,  0,  0,  0,  0,  1.L,                  angular_acceleration              )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -3,  0,  1,  0,  0,  0,  0,  1.L,    W_m2,         heat_flux_density                 )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -3,  0,  1,  0,  0,  0,  0,  1.L,                  irradiance                        )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2,  2,  1,  0, -1,  0,  0,  1.L,     J_K,         heat_capacity                     )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -2,  2,  1,  0, -1,  0,  0,  1.L,                  entropy                           )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2,  2,  0,  0, -1,  0,  0,  1.L,   J_KgK,         specific_heat_capacity            )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -2,  2,  0,  0, -1,  0,  0,  1.L,                  specific_entropy                  )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2,  2,  0,  0,  0,  0,  0,  1.L,    J_Kg,         specific_energy                   )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -3,  1,  1,  0, -1,  0,  0,  1.L,    W_mK,         thermal_conductivity              )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2, -1,  1,  0,  0,  0,  0,  1.L,    J_m3,         energy_density                    )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -3,  1,  1, -1,  0,  0,  0,  1.L,     V_m,         electric_field_strength           )
NINBOT_UNITS_LITERALOPS_UNITALIAS(        1, -3,  0,  1,  0,  0,  0,  1.L,    C_m3,         electric_charge_density           )
NINBOT_UNITS_LITERALOPS_UNITALIAS(        1, -2,  0,  1,  0,  0,  0,  1.L,    C_m2,         surface_charge_density            )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        1, -2,  0,  1,  0,  0,  0,  1.L,                  electric_flux_density             )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(        1, -2,  0,  1,  0,  0,  0,  1.L,                  electric_displacement             )
NINBOT_UNITS_LITERALOPS_UNITALIAS(        4, -3, -1,  2,  0,  0,  0,  1.L,     F_m,         permitivity                       )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2,  1,  1, -2,  0,  0,  0,  1.L,     H_m,         permeability                      )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2,  2,  1,  0,  0, -1,  0,  1.L,   J_mol,         molar_energy                      )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -2,  2,  1,  0, -1, -1,  0,  1.L,  J_Kmol,         molar_entropy                     )
NINBOT_UNITS_UNITALIAS_TYPESUFFIX(       -2,  2,  1,  0, -1, -1,  0,  1.L,                  molar_heat_capacity               )
NINBOT_UNITS_LITERALOPS_UNITALIAS(        1,  0, -1,  1,  0,  0,  0,  1.L,    C_kg,         exposure                          ) // (x- and γ-rays)
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -3,  2,  0,  0,  0,  0,  0,  1.L,    Gy_s,         absorbed_dose_rate                )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -3,  2,  1,  0,  0,  0,  0,  1.L,    W_sr,         radiant_intensity                 )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -3,  0,  1,  0,  0,  0,  0,  1.L,  W_srm2,         radiance                          )
NINBOT_UNITS_LITERALOPS_UNITALIAS(       -1, -3,  0,  0,  0,  1,  0,  1.L,  kat_m3,         catalytic_activity_concentration  )
#endif

// Section 4: Non-SI units that are accepted for use with the SI
//                               T  L  M  I  Θ  N  J        ratio        symb  factory
NINBOT_UNITS_LITRL_FACTR_NORATIO( 1, 0, 0, 0, 0, 0, 0,               60.L, min, minutes            )
NINBOT_UNITS_LITRL_FACTR_NORATIO( 1, 0, 0, 0, 0, 0, 0,            3'600.L,   h, hours              )
NINBOT_UNITS_LITRL_FACTR_NORATIO( 1, 0, 0, 0, 0, 0, 0,           86'400.L,   d, days               )
NINBOT_UNITS_LITRL_FACTR_NORATIO( 0, 1, 0, 0, 0, 0, 0,  149'597'870'700.L,  au, astronomical_units )
// TODO parsec

NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  0,  0,  0,  0,  0,  0,  0,                    π/180.L,        deg )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  0,  0,  0,  0,  0,  0,  0,                    π/180.L,     arcdeg )
NINBOT_UNITS_UNITFACTORY_RATIOPREFIX(      0,  0,  0,  0,  0,  0,  0,                    π/180.L,    degrees )
NINBOT_UNITS_UNITFACTORY_RATIOPREFIX(      0,  0,  0,  0,  0,  0,  0,                    π/180.L, arcdegrees )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  0,  0,  0,  0,  0,  0,  0,             π/(180.L*60.L),     arcmin )
NINBOT_UNITS_UNITFACTORY_RATIOPREFIX(      0,  0,  0,  0,  0,  0,  0,             π/(180.L*60.L), arcminutes )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  0,  0,  0,  0,  0,  0,  0,           π/(180.L*3600.L),     arcsec )
//NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(   0,  0,  0,  0,  0,  0,  0, 1.e-12L * π/(180.L*3600.L),    pas             )
//NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(   0,  0,  0,  0,  0,  0,  0,  1.e-9L * π/(180.L*3600.L),    nas             )
/*NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(   0,  0,  0,  0,  0,  0,  0,  1.e-6L * π/(180.L*3600.L),    µas )*/ /* (µ is unicode \u00b5. Do not confuse with greek small letter μ \u03bc, which is used for vacuum magnetic permeability.) */ \
//NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(   0,  0,  0,  0,  0,  0,  0,  1.e-3L * π/(180.L*3600.L),    mas             )
//NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(   0,  0,  0,  0,  0,  0,  0,           π/(180.L*3600.L),     as             ) // Clashes with attoseconds
NINBOT_UNITS_UNITFACTORY_RATIOPREFIX(      0,  0,  0,  0,  0,  0,  0,           π/(180.L*3600.L), arcseconds )
/*
operator""_deg (char *);
*/

/*
NINBOT_UNITS_LITERAL(  0,  2,  0,  0,  0,  0,  0,  ha,     1.e4L) // hectare
NINBOT_UNITS_LITERAL(  0,  3,  0,  0,  0,  0,  0,  l,      1.e-3L) // litre
NINBOT_UNITS_LITERAL(  0,  3,  0,  0,  0,  0,  0,  L,      1.e-3L) // litre
NINBOT_UNITS_LITERAL(  0,  0,  1,  0,  0,  0,  0,  t,      1.e3L) // tonne
NINBOT_UNITS_LITERAL(  0,  0,  1,  0,  0,  0,  0,  Da,     1.660'539'066'60e-27L) // dalton (1/12 of the mass of Carbon 12)
NINBOT_UNITS_LITERAL_FULL_PREFIXES_FACTORY( -2,  2,  1,  0,  0,  0,  0,  eV, electronvolt,  1.602'176'634e-19L)

// Logarithmic units are included for consistency.
// They are treated as normal adimensional units.
NINBOT_UNITS_LITERAL(  0,  0,  0,  0,  0,  0,  0,  Np,     1.L) // Neper
NINBOT_UNITS_LITERAL(  0,  0,  0,  0,  0,  0,  0,  B,      1.L) // Bel
NINBOT_UNITS_LITERAL(  0,  0,  0,  0,  0,  0,  0,  dB,     0.1L) // deciBel

// Section 5.4.7: Pure numbers for quantity values

NINBOT_UNITS_LITERAL(  0,  0,  0,  0,  0,  0,  0,  ppm,    1.e-6L) // Parts per million
*/


// Section 4 (last paragraph): Deprecated units and other units (LIBRARY EXTENSION)

NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX( -1,  1,  0,  0,  0,  0,  0, 1000.L/3600.L, kmph ) // kilometres per hour
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX( -1,  1,  0,  0,  0,  0,  0, 1000.L/3600.L,  kph ) // kilometres per hour
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX( -1,  1,  0,  0,  0,  0,  0, 1000.L/3600.L, km_h ) // kilometres per hour
NINBOT_UNITS_LITERALOPERATOR_TYPESUFFIX(  0,  1,  0,  0,  0,  0,  0,       1.e-10L,    Å ) // Angstrom (Å is unicode \u00c5)

/*** Non-SI extensions - Imperial ***/
NINBOT_UNITS_LITRL_FACTR_NORATIO(  0,  1,  0,  0,  0,  0,  0,   0.0254L,        in, inches                 )
NINBOT_UNITS_LITRL_FACTR_NORATIO(  0,  1,  0,  0,  0,  0,  0,   0.3048L,        ft, feet                   )
NINBOT_UNITS_LITRL_FACTR_NORATIO(  0,  1,  0,  0,  0,  0,  0,   0.9144L,        yd, yards                  )
NINBOT_UNITS_LITRL_FACTR_NORATIO(  0,  1,  0,  0,  0,  0,  0, 1609.344L,        mi, miles                  )
NINBOT_UNITS_LITRL_FACTR_NORATIO(  0,  1,  0,  0,  0,  0,  0, 1852.000L,       nmi, nautical_miles         )

/*** Non-SI extensions ***/

NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  0,  0,  0,  0,  0,  0,  0,          π/200.L,   gon              )
NINBOT_UNITS_LITERALOPERATOR_RATIOPREFIX(  0,  0,  0,  0,  0,  0,  0,          π/200.L,  grad              )
NINBOT_UNITS_UNITFACTORY_RATIOPREFIX(      0,  0,  0,  0,  0,  0,  0,          π/200.L,         gradians   )

/*** Ninbot extensions ***/

NINBOT_UNITS_GENERATE_ALL(                -2,  2,  1,  0,  0,  0,  0, 1.L, Nm, newtonmetres,   torque                  ) // Same as energy

/*
NINBOT_UNITS_FACTORY(  0,  1,  0,  0,  0,  0,  0,  micron, 1.e-10L)       // Angstrom (Å is unicode \u00c5)
// TODO Factories for micron, bit, byte, etc.

// Degrees Fahrenheit. Unicode character ℉ \u2109 is not XID_Continue.
template <std::floating_point T = double>
consteval
auto
degF (long double magn)
{
    value<dimension{0, 0, 0, 0, 1, 0, 0}, T> retval;
    return retval * (magn + 459.67L) * (5.L/9.L);
}

// Degrees Rankine.
NINBOT_UNITS_LITERAL(  0,  0,  0,  0,  1,  0,  0,  degR, 5.L/9.L)
// Include factory for consistency
template <std::floating_point T = double>
consteval
auto
degR (long double magn)
{
    value<dimension{0, 0, 0, 0, 1, 0, 0}, T> retval;
    return retval * magn * (5.L/9.L);
}
*/



} // namespace units

//__________________ PHYSICAL CONSTANTS ____________________

inline namespace constants {

// The seven defining constants of the SI
export constexpr auto Δv_cs = 9'192'631'770_Hz_L;           // hyperfine transition frequency of Cs (Δ is unicode \u0394)
export constexpr auto c     = 299'792'458_m_s_L;            // speed of light in vacuum
#if 0
constexpr auto ℎ     = 6.626'070'150e-34_J_L*1_s_L;  // Planck constant (ℎ is unicode \u210e)
constexpr auto e     = 1.602'176'634e-19_C_L;        // elementary charge
constexpr auto k     = 1.380'649e-23_J_L/1_K_L;      // Boltzmann constant
constexpr auto N_A   = 6.022'140'760e23L/1._mol_L;   // Avogadro constant
constexpr auto K_cd  = 683_lm_L/1_W_L;               // luminous efficacy

// NOTE: Plain h collides with the unit symbol for hours. Thus, disabled.
//constexpr auto h   = ℎ;                            // Planck constant (using plain h)
constexpr auto ℏ     = ℎ / (2.L * π);                // reduced Planck constant (ℏ is unicode \u210f)
#endif


// Other constants. Source:
// Tiesinga, E., Mohr, P. J., Newell, D. B., & Taylor, B. N. (2021).
// CODATA recommended values of the fundamental physical constants: 2018.
// Reviews of Modern Physics, 93(2), 025010. https://doi.org/10.1103/RevModPhys.93.025010

// TODO: Confirm constants deduced from formulae, add every other constant.
// Table ⅩⅩⅩ ( = 30 in roman system)
export constexpr auto G      = 6.674'30e-11L * 1._m2_L * 1._m_s2_L / 1._kg_L; // Newtonian constant of gravitation [ T^-2 * L^3 * M^-1 ]
#if 0
constexpr auto μ_0    = 1.256'637'062'12e-6L * 1._N_L / ( 1._A_L * 1._A_L ); // Vacuum magnetic permeability  /* μ is unicode \u03bc. This letter is greek mu. */ [ T^-2 * L * M * I^-2 ]
constexpr auto µ_0    = 1.256'637'062'12e-6L * 1._N_L / ( 1._A_L * 1._A_L ); // Vacuum magnetic permeability  /* Alternative for previous μ. This µ \u00b5 is the micro symbol. */ [ T^-2 * L * M * I^-2 ]
constexpr auto ε_0    = 8.854'187'8128e-12L * 1._F_L / 1._m_L; // Vacuum electric permittivity (ε is unicode \u03b5) [ T^4 * K^-3 * M^-1 * I^2 ]
constexpr auto K_J    = (2.L * e / ℎ); // Josephson constant
constexpr auto R_K    = ℎ / (e * e); // von Klitzing constant
constexpr auto Φ_0    = ℎ / (2.L * e); // magnetic flux quantum (Φ is unicode \u03a6)
constexpr auto G_0    = (2.L * e * e) / ℎ; // conductance quantum
constexpr auto m_e    = 9.109'383'7015e-31_kg_L; // electron mass
constexpr auto m_p    = 1.672'621'923'69e-27_kg_L; // proton mass
constexpr auto α      = 7.297'352'5693e-3; // fine-structure constant
constexpr auto cR_inf = 3.289'841'960'2508e15_Hz_L; // Rydberg frequency (Infinite character ∞ \u221e is not in Unicode XID_Continue)
constexpr auto R      = N_A * k; // molar gas constant
constexpr auto F      = N_A * e; // Faraday constant
constexpr auto σ      = (π * π / 60.L) * k * k * k * k / (ℏ * ℏ * ℏ * c * c); // Stefan-Boltzmann constant (σ is unicode \u03c3)
//constexpr auto u      = 1._Da_L; // atomic mass unit: 1/12 of the mass of Carbon 12
#endif


} // namespace constants


//________________________________ STL OVERLOADS ________________________________
// TODO: Use blacklist or whitelist?

/*** Basic math functions ***/

// TODO: Except temperatures
export
template <dimension D, std::floating_point T>
value<D,T>
abs (value<D,T> v)
{
    return (v.SI() >= 0 ? v : -v);
}


/*** Trigonometric functions ***/

export
template <dimension D1, dimension D2, std::floating_point T>
angle_T<T>
atan (value<{},T> value)
{
    return radians(std::atan(value.SI()));
}

export
template <dimension D1, dimension D2, std::floating_point T>
angle_T<T>
atan2 (value<D1,T> num, value<D2,T> denom)
{
    static_assert(D1 == D2, "Inconsistent units");
    return radians(std::atan2(num.SI(), denom.SI()));
}

//________________________________ ___footer___ ________________________________


} // namespace nin

