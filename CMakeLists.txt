CMAKE_MINIMUM_REQUIRED(VERSION 3.28)
project(POS
        VERSION 0.11.0
        LANGUAGES CXX
        DESCRIPTION "POS library"
	)

if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Release CACHE STRING "" FORCE)
endif()

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


set(CPACK_PACKAGE_NAME "pos")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "POS library")
set(CPACK_PACKAGE_CONTACT "")
set(CPACK_PACKAGE_VENDOR "")
set(CPACK_STRIP_FILES YES)

set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Proprietary")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "gcc (>=11.2)")

include(CPack)
include(CMakePackageConfigHelpers)


enable_testing()

add_subdirectory(src)
add_subdirectory(doc)




message(STATUS "")
message(STATUS "####### POS OPTIONS #######")
message(STATUS "Configuration:  " ${CMAKE_BUILD_TYPE})
message(STATUS "Mujoco tests:   " ${MUJOCO_TESTS})
message(STATUS "Documentation:  " ${BUILD_DOCS})
message(STATUS "##### end POS options #####")
message(STATUS "")



configure_package_config_file(
	${CMAKE_CURRENT_SOURCE_DIR}/Config.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/POSConfig.cmake
	INSTALL_DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
)

write_basic_package_version_file(
	"POSConfigVersion.cmake"
	COMPATIBILITY SameMajorVersion
	#COMPATIBILITY AnyNewerVersion
	)


# Install package files
install(FILES
		${CMAKE_CURRENT_BINARY_DIR}/POSConfig.cmake
		${CMAKE_CURRENT_BINARY_DIR}/POSConfigVersion.cmake
	DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
	CONFIGURATIONS Release
	)


