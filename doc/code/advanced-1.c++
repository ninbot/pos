#include <memory>
import ninbot.pos;

using namespace nin;


class position_coordsys_screw_treenode_backend_SPEED
        : public coordsys_tree_backend<position_coordsys_class>
{
    using coordsys = coordsys<position_coordsys_class>;

    units::length const screw_step;

public:
    position_coordsys_screw_treenode_backend_SPEED( coordsys const& ParentCS_arg,
                                                    length          screw_step,
                                                    units::length   initial_value )
      : coordsys_tree_backend<position_coordsys_class>( ParentCS_arg, {} )
      , screw_step{screw_step}
    {
        set_value(initial_value);
    }

    position_coordsys_screw_treenode_backend_SPEED( coordsys const& ParentCS_arg,
                                                    length          screw_step,
                                                    units::angle    initial_value )
      : coordsys_tree_backend<position_coordsys_class>( ParentCS_arg, {} )
      , screw_step{screw_step}
    {
        set_value(initial_value);
    }

    void set_value(length value)
    {
        using namespace units::literals;
        tf_to_parent_data = {{0_m, 0_m, value}, {{0, 0, 1}, value / screw_step}};
    }
    void set_value(angle value)
    {
        using namespace units::literals;
        tf_to_parent_data = {{0_m, 0_m, value * screw_step}, {{0, 0, 1}, value}};
    }

    [[nodiscard]] std::shared_ptr<coordsys_backend<position_coordsys_class>>
    clone() const override
    {
        return std::make_shared<position_coordsys_screw_treenode_backend_SPEED>(
                            parent(), screw_step, tf_to_parent_data.translation.Δz() );
    }
};


class position_coordsys_screw_treenode_backend_MEMORY
        : public coordsys_backend<position_coordsys_class>
{
    using coordsys = coordsys<position_coordsys_class>;

    position_coordsys const parent;
    units::length const screw_step;
    units::length value;

public:
    position_coordsys_screw_treenode_backend_MEMORY( coordsys const& ParentCS_arg,
                                              length          screw_step,
                                              units::length   initial_value )
      : parent{ParentCS_arg}
      , screw_step{screw_step}
      , value{initial_value}
    { }

    position_coordsys_screw_treenode_backend_MEMORY( coordsys const& ParentCS_arg,
                                              length          screw_step,
                                              units::angle    initial_value )
      : parent{ParentCS_arg}
      , screw_step{screw_step}
      , value{initial_value * screw_step}
    { }

    void set_value(length new_value)
    {
        value = new_value;
    }
    void set_value(angle new_value)
    {
        value = new_value * screw_step;
    }

    [[nodiscard]] rigid_body_tf offset() const override
    {
        rigid_body_tf tf {{0_m, 0_m, value}, {{0, 0, 1}, value / screw_step}};
        return position_coordsys_class::compose_tf( parent.tf_to_WCS(), tf );
    }

    [[nodiscard]] std::shared_ptr<coordsys_backend<position_coordsys_class>>
    clone() const override
    {
        return std::make_shared<position_coordsys_screw_treenode_backend_MEMORY>(
                            parent, screw_step, value );
    }
};


int main() {}
