#include <iostream>
import ninbot.pos;

int main(int argc, char const* argv[])
{
    using namespace nin;

    position_coordsys_treenode CS1 (WCS, {{1_m, 0_m, 0_m}, {}});
    position_coordsys_treenode CS2 (CS1, {{}, {0_rad, 0_rad, 90_deg, euler_order::XYZ}});

    point p_CS2 (CS2, {0_m, 10_cm, 0_m});
    point p_WCS = p_CS2.map_to(WCS);

    std::cout << "x = " << p_WCS.x().SI() << " metres\n"; // Output is "x = 0.9 metres"
    return 0;
}
