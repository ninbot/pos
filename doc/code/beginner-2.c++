#include <iostream>
import ninbot.pos;

nin::area A4_area()
{
    nin::length width = nin::centimetres(21); // Factory initialization

    using namespace nin::units::literals;
    nin::length height = 297_mm; // Literal initialization

    return width * height;
}

nin::acceleration
car_0_100_kph(nin::length distance)
{
    // x'=x''*t + x'0;
    // x=x''*t2/2 + x'0*t + x0;

    //x = x''*t2/2;
    //x''2=x'2/t2; t2=x'2/x''2;
    //x = x''/2*x'2/x''2 = x'2/(2*x'');
    //x''=x'2/(2*x);

    using namespace nin::units::literals;
    return 100_kph*100_kph/(2*distance);
}

int main(int argc, char const* argv[])
{
    std::cout << A4_area().SI() << " m2" << std::endl;


}
