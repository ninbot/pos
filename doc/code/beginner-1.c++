#include <iostream>
import ninbot.pos;

/* Custom overload of stream output operator */
std::ostream & operator << (std::ostream & sout, nin::point p)
{
    sout << "("  << p.x().SI()
         << ", " << p.y().SI()
         << ", " << p.z().SI() << ")";

    return sout;
}

int main(int argc, char const* argv[])
{
    using namespace nin;

    /* Definitions */
    position_coordsys_treenode my_coordsys {WCS, {1_m, 0_m, 0_m}};

    /* Operations */
    point world_origin;
    point my_origin_WCS = my_coordsys.origin().map_to(WCS);

    /* Output */
    std::cout << "Origin of my_coordsys in world coordinate system is  "
              << my_origin_WCS << " metres." << std::endl;

    std::cout << "Origin of world coordinate system in my_coordsys is "
              << world_origin.map_to(my_coordsys) << " metres." << std::endl;
}
