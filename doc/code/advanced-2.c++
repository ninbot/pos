#include <iostream>
#include <iomanip>
#include <cmath>

#include "pinocchio/parsers/urdf.hpp"

#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/rnea.hpp"


import ninbot.pos;
using namespace nin::literals;

//constexpr nin::acceleration g = 9.80665_m_s2;
constexpr nin::acceleration g = 9.81_m_s2;

std::ostream & operator << (std::ostream & sout, nin::position_quantity const& value);
//________________________________ centroid ________________________________

struct centroid
{
    using dual_t = decltype(1_m * 1_kg);

    nin::mass            direct;
    std::array<dual_t,3> dual;

    centroid() = default;

    centroid(nin::mass m, nin::position_quantity p)
    {
        direct = m;
        for (size_t i = 0; i < 3; i++)
            dual[i] = p[i] * m;
    }

    centroid & operator += (centroid const& rhs)
    {
        direct += rhs.direct;
        dual[0] += rhs.dual[0];
        dual[1] += rhs.dual[1];
        dual[2] += rhs.dual[2];
        return *this;
    }

    centroid operator + (centroid const& rhs) const
    {
        centroid retval = *this;
        return retval += rhs;
    }

    nin::position_quantity point() const
    {
        nin::position_quantity retval;
        for (size_t i = 0; i < 3; i++)
            retval[i] = dual[i] / direct;
        return retval;
    }

    centroid & transform(nin::rigid_body_tf tf)
    {
        nin::position_quantity c = { nin::metres(dual[0].SI()),
                                     nin::metres(dual[1].SI()),
                                     nin::metres(dual[2].SI()) };
        for (size_t i = 0; i < 3; i++) tf.translation[i] *= direct.SI();
        c = tf(c);
        for (size_t i = 0; i < 3; i++) dual[i] = c[i] * 1_kg;
        return *this;
    }

    nin::torque eq_44 (nin::vector<double,3> const& g_i) const
    {
        return g * ( dual[1]*g_i[0] - dual[0]*g_i[1] );
    };
};




double v(nin::length l) { return std::abs(l.SI()) < 1e-10 ? 0 : l.SI(); }
double v(double l) { return std::abs(l) < 1e-5 ? 0 : l; }

template <typename T, size_t N>
std::ostream &
operator << (std::ostream & sout, nin::vector<T,N> const& value)
{
    sout << "(" << v(value[0]);
    for (size_t i = 1; i < N; i++)
        sout << ", " << v(value[i]);
    return sout << ")";
}

std::ostream & operator << (std::ostream & sout, nin::position_quantity const& value)
{
    sout << "point[ " << v(value.x())
         << "  " << v(value.y())
         << "  " << v(value.z())
         << " ]";
    return sout;
}


std::ostream & operator << (std::ostream & sout, nin::translation const& value)
{
    sout << "translation[ " << v(value.Δx())
         << "  " << v(value.Δy())
         << "  " << v(value.Δz())
         << " ]";
    return sout;
}


std::ostream &
operator << (std::ostream & sout, centroid const& value)
{
    nin::mass mass = value.direct;
    nin::position_quantity loc = (mass != 0._kg ? value.point() : nin::position_quantity{});
    sout << "Mass " << mass.SI() << " kg at (" << std::setprecision(5)
         << v(loc.x().SI()) << ", "
         << v(loc.y().SI()) << ", "
         << v(loc.z().SI()) << ") (m)";
    return sout;
}


template <size_t N>
std::array<nin::torque, N>
gravity_terms( std::array<nin::position_coordsys, N> const& coord_systems,
               std::array<centroid, N>               const& centroids )
{
    /* Obtain transformations from coordinate systems. */
    /* Assume that coordsys_0 is WCS. */

    std::array< nin::vector<double, 3>,  N > gravities;
    std::array< nin::rigid_body_tf,      N > rbtf; // From i to i-1

    std::array<nin::rigid_body_tf, N> offsets;
    for (size_t i = 0; i < N; i++)
        offsets[i] = coord_systems[i].offset();

    nin::vector<double, 3> g = {0, 0, -1};
    for (size_t i = 0; i < N; i++)
    {
        if (i < 5) rbtf[i] = compose(inv(offsets[i]), offsets[i+1]);

        gravities[i] = inv(offsets[i].rotation)(g);
    }


    centroid C;
    std::array<nin::torque, N> ret;

    for (size_t i_ = 0; i_ < N; i_++)
    {
        size_t i = N-1 - i_;
        C.transform(rbtf[i]) += centroids[i];
        ret[i] = C.eq_44(gravities[i]);
    }

    return ret;
}





/* Coordinate system solver for centroids.
 * Written for the gravity compensation paper.
 * Copyright 2024 Francisco Jesus Arjonilla Garcia
 */
// Requirements: POS Library version 0.11.0

using nin::WCS;
using poseq = nin::pose_quantity;
using coordsys = nin::position_coordsys_treenode;
using nin::units::length;
using nin::quaternion;
using nin::point;

constexpr length l1 = 0.089159_m;
constexpr length l2 = 0.425_m;
constexpr length l3 = 0.39225_m;
constexpr length l4 = 0.10915_m;
constexpr length l5 = 0.09465_m;
constexpr length l6 = 0.0823_m;

poseq stated(double θ) { return {{}, nin::quaternion{std::cos(θ/2), 0, 0, std::sin(θ/2)}}; }
poseq neutral(double θ) { return {}; }

poseq (*j)(double θ) = neutral;







/*** Universal Robots ***/

coordsys L_zero { WCS, poseq{{}, {{0,0,1}, 180_deg}} };

coordsys L_0 { L_zero, poseq{{0_m, 0_m, l1}, {{1,0,0}, 90_deg}} };
coordsys L_1 { L_0,    poseq{{0_m, l2, 0_m}, {{0,0,1}, -90_deg}} };
coordsys L_2 { L_1,    poseq{{-l3, 0_m, 0_m}, {}} };
coordsys L_3 { L_2,    poseq{{0_m, 0_m, l4}, (quaternion{1,0,0,-1} * quaternion{1,1,0,0})} };
coordsys L_4 { L_3,    poseq{{0_m, 0_m, l5}, {{1,0,0}, -90_deg}} };
coordsys L_5 { L_4,    poseq{{0_m, 0_m, l6}, {}} };

point c[6] = { {L_0, nin::position_quantity{      0_m, -0.02561_m,   0.00193_m } },
               {L_1, nin::position_quantity{ 0.2125_m,        0_m,   0.11336_m } },
               {L_2, nin::position_quantity{   0.15_m,      0.0_m,    0.0265_m } },
               {L_3, nin::position_quantity{      0_m,  -0.0018_m,   0.01634_m } },
               {L_4, nin::position_quantity{      0_m,   0.0018_m,   0.01634_m } },
               {L_5, nin::position_quantity{      0_m,        0_m, -0.001159_m } } };

/*** Matlab ***/


coordsys M_1 { WCS, poseq{{0_m, 0_m, l1}} };
coordsys M_2 { M_1, poseq{{0_m, 0.13585_m, 0_m}} };
coordsys M_3 { M_2, poseq{{0_m, -0.1197_m, l2}} };
coordsys M_4 { M_3, poseq{{0_m, 0_m, l3}} };
coordsys M_5 { M_4, poseq{{0_m, 0.093_m, 0_m}} };
coordsys M_6 { M_5, poseq{{0_m, 0_m, l5}} };
coordsys M_e { M_6, poseq{{0_m, l6, 0_m}} };


/*** Propuesta ***/

// D-H parameters of robot UR5 by Universal Robots as per the split convention.
constexpr nin::DH_table<6> UR5 = { nin::DH_index_convention::split,
    0_m,   0_deg, 0_m,   0_deg, nin::revolute_joint,
     l1, 180_deg, 0_m,  90_deg, nin::revolute_joint,
    0_m,  90_deg,  l2, 180_deg, nin::revolute_joint,
    0_m,   0_deg,  l3, 180_deg, nin::revolute_joint,
     l4,  90_deg, 0_m,  90_deg, nin::revolute_joint,
     l5, 180_deg, 0_m,  90_deg, nin::revolute_joint };


//________________________________ main() ________________________________


int main(int argc, char * argv[])
{

    nin::position_coordsys_DH<UR5.structural()> DH_CS;
    std::array<nin::position_coordsys, 6> links;
    for (size_t i = 0; i < 6; i++)
        links[i] = DH_CS.link(i);

    std::array<centroid, 6> UR5_c = { centroid
         {    3.7_kg, c[0].map_to(links[0]) },
         {  8.393_kg, c[1].map_to(links[1]) },
         {   2.33_kg, c[2].map_to(links[2]) },
         {  1.219_kg, c[3].map_to(links[3]) },
         {  1.219_kg, c[4].map_to(links[4]) },
         { 0.1879_kg, c[5].map_to(links[5]) } };

    DH_CS.joints({ 0_deg,
                   90_deg,
                   90_deg,
                   0_deg,
                   0_deg,
                   0_deg });

    DH_CS.joints({ 0.1_rad, 0.2_rad, 0.3_rad, 0.4_rad, 0.5_rad, 0.6_rad });

    std::array<nin::torque, 6> tfs = gravity_terms(links, UR5_c);

    std::cout << "Torque 5: " << v(tfs[5].SI()) << " Nm\n";
    std::cout << "Torque 4: " << v(tfs[4].SI()) << " Nm\n";
    std::cout << "Torque 3: " << v(tfs[3].SI()) << " Nm\n";
    std::cout << "Torque 2: " << v(tfs[2].SI()) << " Nm\n";
    std::cout << "Torque 1: " << v(tfs[1].SI()) << " Nm\n";
    std::cout << "Torque 0: " << v(tfs[0].SI()) << " Nm\n";



//________________________________ Pinocchio ________________________________

    using namespace pinocchio;

    std::cerr << "----------------------------- PINOCCHIO LIBRARY -----------------------------\n";

    // You should change here to set up your own URDF file or just pass it as an argument of this
    // example.
    const std::string urdf_filename = std::string(MODEL_URDF);


    // Load the urdf model
    Model model;
    pinocchio::urdf::buildModel(urdf_filename, model);
    std::cout << "model name: " << model.name << std::endl;

    // Create data required by the algorithms
    Data data(model);

    // Sample a random configuration
    //Eigen::VectorXd q = randomConfiguration(model);
    Eigen::VectorXd q(6);
    //q << 0.1, 0.2, 0.3, 0.4, 0.5, 0.6;
    std::cout << "q: " << q.transpose() << std::endl;

    // Perform the forward kinematics over the kinematic tree
    forwardKinematics(model, data, q);

    // Print out the placement of each joint of the kinematic tree
    for (JointIndex joint_id = 0; joint_id < (JointIndex)model.njoints; ++joint_id)
        std::cout << std::setw(24) << std::left << model.names[joint_id] << ": " << std::fixed
            << std::setprecision(2) << data.oMi[joint_id].translation().transpose() << std::endl;



    auto r = pinocchio::rnea::rnea(model, data, q, 0, 0);









    return 0;
}



