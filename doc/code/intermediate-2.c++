#include <mujoco/mujoco.h>
#include <iostream>

import ninbot.pos;

int main()
{
    mjModel * mmodel;
    mjData  * mdata;
    const char * file = MODEL_XML;

    // Load MuJoCo model
    char error[256] = "Could not load XML model";
    mmodel = mj_loadXML(file, nullptr, error, 255);
    if (!mmodel)
        throw std::runtime_error(error);

    mdata = mj_makeData(mmodel);
    mj_forward(mmodel, mdata);

    nin::position_coordsys_mujoco frame  (mmodel, mdata, mjOBJ_XBODY, "frame");
    std::array<nin::position_coordsys, 4> feet;
    feet[0] = nin::position_coordsys_mujoco(mmodel, mdata, mjOBJ_SITE, "foot_0");
    feet[1] = nin::position_coordsys_mujoco(mmodel, mdata, mjOBJ_SITE, "foot_1");
    feet[2] = nin::position_coordsys_mujoco(mmodel, mdata, mjOBJ_SITE, "foot_2");
    feet[3] = nin::position_coordsys_mujoco(mmodel, mdata, mjOBJ_SITE, "foot_3");

    unsigned air_counter = 0;
    using namespace nin::units;
    for (size_t i = 0; i < 15'000; i++) // 50 seconds
    {
        nin::length min_height = feet[0].origin().map_to(nin::WCS).z();
        for (int i = 1; i < 4; i++)
            min_height = std::min(min_height, feet[i].origin().map_to(nin::WCS).z());

        if (min_height > nin::centimetres(2))
            air_counter++;

        mj_step(mmodel, mdata);
    }
    std::cerr << "DEBUG air time: " << 0.002 * air_counter << '\n';
}

