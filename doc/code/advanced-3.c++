#include <iostream>
#include <memory>

import ninbot.pos;

using namespace nin;


template <coordsys_class CC>
class coordsys_cutoff_backend : public coordsys_backend<CC>
{
    coordsys<CC> const parent;

public:
    bool enabled;

    using coordsys_class = CC;

    coordsys_cutoff_backend(coordsys<CC> const& ParentCS_arg, bool initial_state = false)
        : parent{ParentCS_arg}, enabled{initial_state} { }

    [[nodiscard]] CC::tf_data_type offset() const override
    {
        return enabled ? typename CC::tf_data_type {} : parent.offset();
    }

    [[nodiscard]] std::shared_ptr<coordsys_backend<CC>> clone() const override
    {
        return std::make_shared<coordsys_cutoff_backend<CC>>(parent, enabled);
    }
};


template <coordsys_class CC> using coordsys_cutoff = coordsys_generic<coordsys_cutoff_backend<CC>>;



std::ostream & operator << (std::ostream & sout, position_coordsys pcs)
{
    point pq = pcs.origin().map_to(WCS);
    return sout << "("  << pq.x().SI()
                << ", " << pq.y().SI()
                << ", " << pq.z().SI() << ")";
}

int main()
{
    position_coordsys_treenode x(WCS, {1_m});
    position_coordsys_treenode xy(x, {0_m, 1_m});
    coordsys_cutoff<position_coordsys_class> cutoff(xy);
    position_coordsys_treenode xyz(cutoff, {0_m, 0_m, 1_m});

    std::cout << "Cutoff disabled: " << xyz << '\n';
    cutoff->enabled = true;
    std::cout << "Cutoff enabled: " << xyz << '\n';
}

