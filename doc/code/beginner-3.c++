#include <iostream>
#include <cmath>
import ninbot.pos;

/* Custom overload of stream output operator */
std::ostream & operator << (std::ostream & sout, nin::planar_point p)
{
    nin::planar_point p_WCS = p.map_to(nin::WCS);

    return sout << "("  << p_WCS.x().SI()
                << ", " << p_WCS.y().SI() << ")";
}

int main(int argc, char const* argv[])
{
    using namespace nin;
    using treenode = planar_position_coordsys_treenode;

    /* Definition of coordinate systems */
    treenode step1 (WCS,   {0.5_km});        // Walk East 0.5 kilometres
    treenode step2 {step1, {{}, -0.6_rad}};  // Turn right 0.6 radians
    treenode step3 {step2, {0_m, 8888_cm}};  // Walk sideways to the left 8888 centimetres
    treenode step4 (step3, {{}, 180_deg});   // Turn around
    treenode step5 {step4, {200_m, 0_m}};    // Walk straight for 200 metres
    treenode step6 {step5, {WCS, {{step5.origin().map_to(WCS)}, -135_deg}}}; // Face South-West
    treenode step7 {step6, {1_km/3, 0_m}};   // Walk a third of a kilometre

    /* Definition of points */
    planar_point treasure1 = step7.origin().map_to(WCS);
    step2.set_origin_in_parent({{}, 0.6_rad}); // Turn left 0.6 radians
    step6.set_origin_in_parent({WCS, {{step5.origin().map_to(WCS)}, -135_deg}}); // Face South-West
    planar_point treasure2 = step7;

    /* Find closest location */
    length dist1  = distance({}, treasure1);
    length dist2  = distance({}, treasure2);
    if (dist1 > dist2)
    {
        std::swap(treasure1, treasure2);
        std::swap(dist1, dist2);
    }

    /* Calculate turning angles */
    angle turn1 = radians( atan2(treasure1.y(), treasure1.x()) );

    treenode face1 {WCS, { {}, turn1 }};
    treenode location1 {face1, {dist1}};
    planar_point treasure2_loc1 {location1, treasure2};
    angle turn2 = radians( atan2(treasure2_loc1.y(), treasure2_loc1.x()) );

    /* Output */
    std::cout << "Candidate location 1 is at " << treasure1 << std::endl;
    std::cout << "Candidate location 2 is at " << treasure2 << std::endl;

    std::cout << "First candidate treasure location: turn " << turn1.in(1_deg)
              << " degrees and walk " << dist1.SI() << " metres.\n";
    std::cout << "If treasure is not there, turn " << turn2.in(1_deg) << " degrees and walk "
              << distance(treasure1, treasure2).SI() << " metres.\n";
}
